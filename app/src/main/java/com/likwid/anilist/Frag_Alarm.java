package com.likwid.anilist;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Frag_Alarm extends Fragment implements View.OnClickListener, TextView.OnEditorActionListener, TimePickerDialog.OnTimeSetListener {

    public final static String FRAG_NAME = "frag_alarm";

    private final int REQUEST_CODE_EMAIL = 50000;
    private boolean sun, mon, tue, wed, thu, fri, sat, monthly, yearly;
    private Frag_Calendar_View.FragmentCalendar fc;
    Calendar startCal = Calendar.getInstance();
    Calendar endCal = Calendar.getInstance();

    private MainActivity toSlide;

    private int id;
    private Button bSetColor;
    private EditText etName;
    private ImageView ivRepeatArrow;
    private Switch sEndDate;

    private LinearLayout llColors;
    private ImageView ivBlue, ivRed, ivPink, ivPurple, ivOrange, ivGreen, ivLime, ivYellow, ivSky,
        ivSlate;
    private ImageView ivRepeatSun, ivRepeatMon, ivRepeatTue, ivRepeatWed, ivRepeatThu, ivRepeatFri,
        ivRepeatSat, ivRepeatMonthly, ivRepeatYearly, ivStartCal, ivEndCal;

    private String color = "blue";
    private Adapter_SQL database;
    private TextView tvStartsTitle, tvEndsTitle, tvRepeatTitle;
    private LinearLayout llRepeat1, llRepeat2, llRepeat3;
    private boolean repeatOpened;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fc = (Frag_Calendar_View.FragmentCalendar) getActivity();
        database = new Adapter_SQL(getActivity());
        startCal.setTimeInMillis(fc.getDate().getTimeInMillis());
        toSlide = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup container, Bundle savedInstanceState) {
        View calAdd = li.inflate(com.likwid.anilist.R.layout.frag_alarm, container, false);

        Button bSetAlarm = (Button) calAdd.findViewById(com.likwid.anilist.R.id.bSetAlarm);
        bSetColor = (Button) calAdd.findViewById(com.likwid.anilist.R.id.bSetColor);
        etName = (EditText) calAdd.findViewById(com.likwid.anilist.R.id.etName);
        sEndDate = (Switch) calAdd.findViewById(com.likwid.anilist.R.id.sEndDate);

        ivRepeatArrow = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatArrow);
        llRepeat1 = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llRepeat1);
        llRepeat2 = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llRepeat2);
        llRepeat3 = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llRepeat3);
        repeatOpened = false;

        llColors = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llColors);
        LinearLayout llBlue = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llBlue);
        LinearLayout llRed = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llRed);
        LinearLayout llPink = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llPink);
        LinearLayout llPurple = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llPurple);
        LinearLayout llOrange = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llOrange);
        LinearLayout llGreen = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llGreen);
        LinearLayout llLime = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llLime);
        LinearLayout llYellow = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llYellow);
        LinearLayout llSky = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llSky);
        LinearLayout llSlate = (LinearLayout) calAdd.findViewById(com.likwid.anilist.R.id.llSlate);

        ivBlue = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckBlue);
        ivRed = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckRed);
        ivPink = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckPink);
        ivPurple = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckPurple);
        ivOrange = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckOrange);
        ivGreen = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckGreen);
        ivLime = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckLime);
        ivYellow = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckYellow);
        ivSky = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckSky);
        ivSlate = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivCheckSlate);

        ivRepeatSun = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatSun);
        ivRepeatMon = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatMon);
        ivRepeatTue = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatTue);
        ivRepeatWed = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatWed);
        ivRepeatThu = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatThu);
        ivRepeatFri = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatFri);
        ivRepeatSat = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatSat);
        ivRepeatMonthly = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatMonthly);
        ivRepeatYearly = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivRepeatYearly);

        tvStartsTitle = (TextView) calAdd.findViewById(com.likwid.anilist.R.id.tvStartsTitle);
        ivStartCal = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivStartCal);
        ivEndCal = (ImageView) calAdd.findViewById(com.likwid.anilist.R.id.ivEndCal);
        tvEndsTitle = (TextView) calAdd.findViewById(com.likwid.anilist.R.id.tvEndsTitle);
        tvRepeatTitle = (TextView) calAdd.findViewById(com.likwid.anilist.R.id.tvRepeatTitle);

        RelativeLayout rlBottomNew = (RelativeLayout) calAdd.findViewById(com.likwid.anilist.R.id.rlBottomNew);
        RelativeLayout rlBottomExists = (RelativeLayout) calAdd.findViewById(com.likwid.anilist.R.id.rlBottomExists);

        ivRed.setVisibility(View.INVISIBLE);
        ivPink.setVisibility(View.INVISIBLE);
        ivPurple.setVisibility(View.INVISIBLE);
        ivOrange.setVisibility(View.INVISIBLE);
        ivGreen.setVisibility(View.INVISIBLE);
        ivLime.setVisibility(View.INVISIBLE);
        ivYellow.setVisibility(View.INVISIBLE);
        ivSky.setVisibility(View.INVISIBLE);
        ivSlate.setVisibility(View.INVISIBLE);

        tvStartsTitle.setOnClickListener(this);
        tvEndsTitle.setOnClickListener(this);
        tvRepeatTitle.setOnClickListener(this);

        ivStartCal.setOnClickListener(this);
        ivEndCal.setOnClickListener(this);

        bSetAlarm.setOnClickListener(this);

        llBlue.setOnClickListener(this);
        llRed.setOnClickListener(this);
        llPink.setOnClickListener(this);
        llPurple.setOnClickListener(this);
        llOrange.setOnClickListener(this);
        llGreen.setOnClickListener(this);
        llLime.setOnClickListener(this);
        llYellow.setOnClickListener(this);
        llSky.setOnClickListener(this);
        llSlate.setOnClickListener(this);

        ivRepeatSun.setOnClickListener(this);
        ivRepeatMon.setOnClickListener(this);
        ivRepeatTue.setOnClickListener(this);
        ivRepeatWed.setOnClickListener(this);
        ivRepeatThu.setOnClickListener(this);
        ivRepeatFri.setOnClickListener(this);
        ivRepeatSat.setOnClickListener(this);
        ivRepeatMonthly.setOnClickListener(this);
        ivRepeatYearly.setOnClickListener(this);

        bSetColor.setOnClickListener(this);

        etName.setOnEditorActionListener(this);

        sEndDate.setOnClickListener(this);

        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) sun = true;
        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) mon = true;
        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) tue = true;
        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) wed = true;
        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) thu = true;
        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) fri = true;
        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) sat = true;

        startCal.setTimeInMillis(fc.getDate().getTimeInMillis());
        startCal.set(Calendar.HOUR_OF_DAY, startCal.get(Calendar.HOUR_OF_DAY) + 1);

        endCal.setTimeInMillis(fc.getDate().getTimeInMillis());
        endCal.set(Calendar.HOUR_OF_DAY, endCal.get(Calendar.HOUR_OF_DAY) + 2);

        id = getArguments().getInt(MainActivity.ALARM_ARGS);
        if (id > 0){
            //if item exists
            rlBottomNew.setVisibility(View.GONE);
            rlBottomExists.setVisibility(View.VISIBLE);
            database.open();
            bSetAlarm.setText("Set alarm for " + database.getName(id));
            database.close();
        }else{
            //if new created
            rlBottomNew.setVisibility(View.VISIBLE);
            rlBottomExists.setVisibility(View.GONE);
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int colorInt = prefs.getInt(getActivity().getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);

        RelativeLayout rlTop = (RelativeLayout) calAdd.findViewById(com.likwid.anilist.R.id.rlTop);
        rlTop.setBackgroundResource(colorInt);
        updateTexts();

        return calAdd;
    }

    public Integer colorStringToInt(String color) {
        int colorInt = com.likwid.anilist.R.color.cBlue;
        if (color.equals("blue")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cBlue);
        if (color.equals("red")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cRed);
        if (color.equals("pink")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cPink);
        if (color.equals("purple")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cPurple);
        if (color.equals("orange")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cOrange);
        if (color.equals("green")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cGreen);
        if (color.equals("lime")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cLime);
        if (color.equals("yellow")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cYellow);
        if (color.equals("sky")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cSky);
        if (color.equals("slate")) colorInt = getResources().getColor(com.likwid.anilist.R.color.cSlate);
        return colorInt;
    }

    public void resetColors(){
        ivBlue.setVisibility(View.INVISIBLE);
        ivRed.setVisibility(View.INVISIBLE);
        ivPink.setVisibility(View.INVISIBLE);
        ivPurple.setVisibility(View.INVISIBLE);
        ivOrange.setVisibility(View.INVISIBLE);
        ivGreen.setVisibility(View.INVISIBLE);
        ivLime.setVisibility(View.INVISIBLE);
        ivYellow.setVisibility(View.INVISIBLE);
        ivSky.setVisibility(View.INVISIBLE);
        ivSlate.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_EMAIL && resultCode == Activity.RESULT_OK){
            String email = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(getString(com.likwid.anilist.R.string.prefEmail), email);
            editor.commit();
            setAlarm();
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    public void setAlarm(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String email = prefs.getString(getString(com.likwid.anilist.R.string.prefEmail), null);
        String name;
        Element single;

        if (email == null || !email.contains("@") || !email.contains(".") || email.length() < 6) {
            try {
                Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                        new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, null, null, null, null);
                startActivityForResult(intent, REQUEST_CODE_EMAIL);

            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
            return;
        }

        if (endCal.getTimeInMillis() < startCal.getTimeInMillis()){
            Toast.makeText(getActivity(), "The ending time must be after the starting time", Toast.LENGTH_SHORT).show();
            return;
        }

        database.open();
        if (id <= 0) {
            //if task is NOT created, create new item
            name = etName.getText().toString();
            if (database.createElement(name, color, 0, database.getPath(0))) {
                single = database.getLast();
            }else{
                return;
            }
        }else{
            //if task is already created
            single = database.getSingle(id);
            color = single.color;
            name = single.title;
        }

        //Add Alarm to calendar
        SetNotification notif = new SetNotification(
                getActivity(),
                startCal,
                endCal,
                name,
                colorStringToInt(color),
                email);
        notif.SetRepeatDay(sun, mon, tue, wed, thu, fri, sat);
        notif.SetMonthly(monthly);
        notif.SetYearly(yearly);

        notif.EventIsForever(!sEndDate.isChecked());

        //Delete event if it exists
        if (single.alarm > 0) notif.deleteReminder(single.alarm);

        notif.MakeReminder();
        long eventID = notif.insertReminder();

        Element addAlarmTo;
        if (id <= 0){
            //if task did NOT exist before this
            addAlarmTo = database.getLast();
        }else{
            //if task did exist before this
            addAlarmTo = database.getSingle(id);
        }

        database.addAlarm(addAlarmTo.id, eventID);
        database.close();
        toSlide.refreshList();
        fc.refreshCalendar();
    }

    @Override
    public void onClick(View view) {
        resetColors();
        switch (view.getId()){
            case com.likwid.anilist.R.id.sEndDate:
                updateTexts();
                break;
            case com.likwid.anilist.R.id.tvRepeatTitle:
                if (repeatOpened){
                    llRepeat1.setVisibility(View.GONE);
                    llRepeat2.setVisibility(View.GONE);
                    llRepeat3.setVisibility(View.GONE);
                    repeatOpened = false;
                    ivRepeatArrow.setImageResource(com.likwid.anilist.R.drawable.droparrowdown);
                }else{
                    llRepeat1.setVisibility(View.VISIBLE);
                    llRepeat2.setVisibility(View.VISIBLE);
                    llRepeat3.setVisibility(View.VISIBLE);
                    repeatOpened = true;
                    ivRepeatArrow.setImageResource(com.likwid.anilist.R.drawable.droparrowup);
                }
                break;
            case com.likwid.anilist.R.id.tvStartsTitle:
                DialogFragment StartTp = new StartTimePicker();
                StartTp.show(getFragmentManager(), "starttimepicker");
                break;
            case com.likwid.anilist.R.id.tvEndsTitle:
                DialogFragment EndTp = new EndTimePicker();
                EndTp.show(getFragmentManager(), "endtimepicker");
                break;
            case com.likwid.anilist.R.id.ivStartCal:
                DialogFragment startCp = new StartCalPicker();
                startCp.show(getFragmentManager(), "startcalpicker");
                break;
            case com.likwid.anilist.R.id.ivEndCal:
                DialogFragment endCp = new EndCalPicker();
                endCp.show(getFragmentManager(), "endcalpicker");
                break;
            case com.likwid.anilist.R.id.bSetAlarm:
                setAlarm();
                break;
            case com.likwid.anilist.R.id.bSetColor:
                llColors.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llBlue:
                ivBlue.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cBlue));
                llColors.setVisibility(View.GONE);
                color = "blue";
                break;
            case com.likwid.anilist.R.id.llRed:
                ivRed.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cRed));
                llColors.setVisibility(View.GONE);
                color = "red";
                break;
            case com.likwid.anilist.R.id.llPink:
                ivPink.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cPink));
                llColors.setVisibility(View.GONE);
                color = "pink";
                break;
            case com.likwid.anilist.R.id.llPurple:
                ivPurple.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cPurple));
                llColors.setVisibility(View.GONE);
                color = "purple";
                break;
            case com.likwid.anilist.R.id.llOrange:
                ivOrange.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cOrange));
                llColors.setVisibility(View.GONE);
                color = "orange";
                break;
            case com.likwid.anilist.R.id.llGreen:
                ivGreen.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cGreen));
                llColors.setVisibility(View.GONE);
                color = "green";
                break;
            case com.likwid.anilist.R.id.llLime:
                ivLime.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cLime));
                llColors.setVisibility(View.GONE);
                color = "lime";
                break;
            case com.likwid.anilist.R.id.llYellow:
                ivYellow.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cYellow));
                llColors.setVisibility(View.GONE);
                color = "yellow";
                break;
            case com.likwid.anilist.R.id.llSky:
                ivSky.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cSky));
                llColors.setVisibility(View.GONE);
                color = "sky";
                break;
            case com.likwid.anilist.R.id.llSlate:
                ivSlate.setVisibility(View.VISIBLE);
                bSetColor.setBackgroundColor(getResources().getColor(com.likwid.anilist.R.color.cSlate));
                llColors.setVisibility(View.GONE);
                color = "slate";
                break;
            case com.likwid.anilist.R.id.ivRepeatSun:
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY || yearly || monthly) {
                    yearly = monthly = false;
                    sun = !sun;
                }
                setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatMon:
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY || yearly || monthly) {
                    yearly = monthly = false;
                    mon = !mon;
                }
                setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatTue:
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.TUESDAY || yearly || monthly) {
                    yearly = monthly = false;
                    tue = !tue;
                }
                setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatWed:
                setRestrictionDay();
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.WEDNESDAY || yearly || monthly) {
                    yearly = monthly = false;
                    wed = !wed;
                }
                setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatThu:
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY || yearly || monthly) {
                    yearly = monthly = false;
                    thu = !thu;
                }
                setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatFri:
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY || yearly || monthly) {
                    yearly = monthly = false;
                    fri = !fri;
                }
                setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatSat:
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY || yearly || monthly) {
                    yearly = monthly = false;
                    sat = !sat;
                }
                setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatMonthly:
                sun=mon=tue=wed=thu=fri=sat=yearly=false;
                monthly = !monthly;
                if (!monthly)
                    setRestrictionDay();
                break;
            case com.likwid.anilist.R.id.ivRepeatYearly:
                sun=mon=tue=wed=thu=fri=sat=monthly=false;
                yearly = !yearly;
                if (!yearly)
                    setRestrictionDay();
                break;
        }
        setRepeatDisplay();
        updateTexts();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }

    public void setRestrictionDay(){
        if (!yearly && !monthly) {
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) sun = true;
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) mon = true;
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) tue = true;
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) wed = true;
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) thu = true;
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) fri = true;
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) sat = true;
        }
        setRepeatDisplay();
    }

    public void setRepeatDisplay(){
        if (!sun){ ivRepeatSun.setAlpha(.25f); }else{ ivRepeatSun.setAlpha(1f); }
        if (!mon){ ivRepeatMon.setAlpha(.25f); }else{ ivRepeatMon.setAlpha(1f); }
        if (!tue){ ivRepeatTue.setAlpha(.25f); }else{ ivRepeatTue.setAlpha(1f); }
        if (!wed){ ivRepeatWed.setAlpha(.25f); }else{ ivRepeatWed.setAlpha(1f); }
        if (!thu){ ivRepeatThu.setAlpha(.25f); }else{ ivRepeatThu.setAlpha(1f); }
        if (!fri){ ivRepeatFri.setAlpha(.25f); }else{ ivRepeatFri.setAlpha(1f); }
        if (!sat){ ivRepeatSat.setAlpha(.25f); }else{ ivRepeatSat.setAlpha(1f); }
        if (!monthly){ ivRepeatMonthly.setAlpha(.25f); }else{ ivRepeatMonthly.setAlpha(1f); }
        if (!yearly){ ivRepeatYearly.setAlpha(.25f); }else{ ivRepeatYearly.setAlpha(1f); }
        updateTexts();
    }

    private void updateTexts() {
        tvStartsTitle.setText("Start " + new SimpleDateFormat("MM/dd/yyyy @ hh:mm a").format(startCal.getTime()));
        if (!sEndDate.isChecked()){
            tvEndsTitle.setText("Repeat forever, ending " + new SimpleDateFormat("hh:mm a").format(endCal.getTime()));
        }else {
            tvEndsTitle.setText("Until " + new SimpleDateFormat("MM/dd/yyyy @ hh:mm a").format(endCal.getTime()));
        }

        String repeatText = "Days: ";
        if (sun && mon && tue && wed && thu & fri && sat){
            repeatText += "Everyday";
        }else {
            if (sun) repeatText += "Sun ";
            if (mon) repeatText += "Mon ";
            if (tue) repeatText += "Tue ";
            if (wed) repeatText += "Wed ";
            if (thu) repeatText += "Thu ";
            if (fri) repeatText += "Fri ";
            if (sat) repeatText += "Sat ";
        }
        if (yearly) repeatText += "Yearly ";
        if (monthly) repeatText += "Monthly ";

        tvRepeatTitle.setText(repeatText);

    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        setAlarm();
        return true;
    }

    public void setStartCal(Calendar cal){
        startCal.setTimeInMillis(cal.getTimeInMillis());
        updateTexts();
    }

    public Calendar getStartCal(){
        return startCal;
    }

    public void setEndCal(Calendar cal){
        endCal.setTimeInMillis(cal.getTimeInMillis());
        updateTexts();
    }

    public Calendar getEndCal(){
        return endCal;
    }

    public static class StartTimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            MainActivity slide = (MainActivity) getActivity();
            FragmentManager fm = slide.getFragmentManager();
            Fragment alarmFrag = fm.findFragmentById(R.id.container);
            Frag_Alarm toAlarm = (Frag_Alarm) alarmFrag;

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(toAlarm.getStartCal().getTimeInMillis());
            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            cal.set(Calendar.MINUTE, minute);

            toAlarm.setStartCal(cal);
        }
    }

    public static class EndTimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            MainActivity slide = (MainActivity) getActivity();
            FragmentManager fm = slide.getFragmentManager();
            Fragment alarmFrag = fm.findFragmentById(R.id.container);
            Frag_Alarm toAlarm = (Frag_Alarm) alarmFrag;

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(toAlarm.getEndCal().getTimeInMillis());
            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            cal.set(Calendar.MINUTE, minute);

            toAlarm.setEndCal(cal);
        }
    }

    public static class StartCalPicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of TimePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, dayOfMonth);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            MainActivity slide = (MainActivity) getActivity();
            FragmentManager fm = slide.getFragmentManager();
            Fragment alarmFrag = fm.findFragmentById(R.id.container);
            Frag_Alarm toAlarm = (Frag_Alarm) alarmFrag;

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(toAlarm.getEndCal().getTimeInMillis());
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, monthOfYear);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            toAlarm.setStartCal(cal);
        }
    }

    public static class EndCalPicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of TimePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, dayOfMonth);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            MainActivity slide = (MainActivity) getActivity();
            FragmentManager fm = slide.getFragmentManager();
            Fragment alarmFrag = fm.findFragmentById(R.id.container);
            Frag_Alarm toAlarm = (Frag_Alarm) alarmFrag;

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(toAlarm.getEndCal().getTimeInMillis());
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, monthOfYear);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            toAlarm.setEndCal(cal);
        }
    }
}