package com.likwid.anilist;

import android.graphics.Canvas;
import android.graphics.Point;
import android.view.View;
import android.view.View.DragShadowBuilder;

public class ShadowBuilder extends DragShadowBuilder{

	View v;
	
	public ShadowBuilder(View view) {
		super(view);
		v = view;
	}

	@Override
	public void onDrawShadow(Canvas canvas) {
		
		super.onDrawShadow(canvas);
	}

	@Override
	public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint) {
		int height = v.getHeight()/2;
		int width =  v.getWidth()/2;
		
		v.setMinimumHeight(height);
		v.setMinimumWidth(width);
		
		shadowSize.set(width, height);
		shadowTouchPoint.set(height/2, height/2);
		super.onProvideShadowMetrics(shadowSize, shadowTouchPoint);
	}

}
