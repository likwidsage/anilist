package com.likwid.anilist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by likwid on 5/17/14.
 */
public class ListText extends TextView {

    public ListText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        rotate();
    }

    public ListText(Context context, AttributeSet attrs){
        super(context, attrs);
        init();
        rotate();
    }

    public ListText(Context context){
        super(context);
        init();
        rotate();
    }

    private void rotate() {
        setSelected(true);
    }

    private void init() {
        if (!isInEditMode()) {

        }
    }
}
