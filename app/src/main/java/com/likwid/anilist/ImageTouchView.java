package com.likwid.anilist;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by likwid on 9/21/14.
 */
public class ImageTouchView extends TouchView {

    public ImageTouchView(Context context) {
        super(context);
    }

    public ImageTouchView(Context c, AttributeSet as) {
        super(c, as);
    }

    public ImageTouchView(Context c, AttributeSet as, int style) {
        super(c, as, style);
    }
}
