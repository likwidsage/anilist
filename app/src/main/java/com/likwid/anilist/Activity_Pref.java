package com.likwid.anilist;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Activity_Pref extends Activity implements OnClickListener, AdapterView.OnItemSelectedListener, IabHelper.OnIabPurchaseFinishedListener, IabHelper.QueryInventoryFinishedListener, IabHelper.OnIabSetupFinishedListener {

    Button bChangeBG;
	LinearLayout llBlue, llRed, llPink, llOrange, llGreen, llPurple, llLime, llYellow, llSky, llSlate,
            llPrefsThemes;
	int heightWidth;
	String prefTheme, prefEmail;
    Boolean colorBGcurrent, colorMenuCurrent, BgIsImage;
	int colorPrefInt;
	SharedPreferences prefs;
	Switch swColorBG, swColorMenu, swShowLabel;
    Spinner sTheme;
    ArrayList<String> themeList = new ArrayList<String>();
    private Switch swBgIsImage;
    TextView tvVersion;
    private TextView tvEmail;
    Boolean hasTheme = false;
    Boolean prefShowLabel;

    IabHelper mIabHelper;
    private static final String bepbk1="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQE";
    private static final String bepbk2="AnPfXoAIkGcoUuxbj06OKH0ZWVaQ+0a9LK+rmgrY0RJJW";
    private static final String bepbk3="Qsv4MpMm5tAAh17OPy7vuLctPb5EOLJ+fgwM5d";
    private static final String bepbk4="IB15sMZWhDsEGjjih8ZV0LqG5aqtlR3LOTPtfu0/6Zq9LNp";
    private static final String bepbk5="qgtNQgGJfnSCayPvg/3oHVAjl7gP7+iz3KqwG";
    private static final String bepbk6="xqxh9R20EhCb+DtL8XYsH7HkNw9hcVpvpQ/56SmAgjk/KuV6k++o";
    private static final String bepbk7="DCvWVdGCSlpvJfnK96gpgeWrbPY0VLPRik";
    private static final String bepbk8="f56D7r1/1i6j7vUPlSl0/nBgwGO9L+mK4gmxjvFvVljoZ";
    private static final String bepbk9="zHIrnswV9+15hjh+69Ah+i25JRXKMIe5NntGy";
    private static final String bepbk0="GopA5ruwIDAQAB";

    static final String LISTTHEMES = MainActivity.LISTTHEMES;
    static final File themeFolder = new File(Environment.getExternalStorageDirectory().toString() + "/AniList/themes/");
    static final File folder = new File(Environment.getExternalStorageDirectory().toString() + "/AniList/exported/");
    private static final int REQUEST_CODE_EMAIL = 600;
    private static final String TAG = "Preference";
    private SeekBar sbMenuBgTrans;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        getActionBar().hide();

		setContentView(com.likwid.anilist.R.layout.activity_pref);

		llBlue = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefBlue);
		llRed = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefRed);
		llPink = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefPink);
		llOrange = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefOrange);
		llGreen = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefGreen);
		llPurple = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefPurple);
		llLime = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefLime);
		llYellow = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefYellow);
		llSky = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefSky);
		llSlate = (LinearLayout) findViewById(com.likwid.anilist.R.id.prefSlate);
		swColorBG = (Switch) findViewById(com.likwid.anilist.R.id.swPrefChangeBG);
		swColorMenu = (Switch) findViewById(com.likwid.anilist.R.id.swPrefsChangeMenu);
		swShowLabel = (Switch) findViewById(com.likwid.anilist.R.id.swPrefsShowLabel);
        swBgIsImage = (Switch) findViewById(com.likwid.anilist.R.id.swPrefImageOrColor);
        sbMenuBgTrans = (SeekBar) findViewById(com.likwid.anilist.R.id.sbMenuBgTrans);
        bChangeBG = (Button) findViewById(com.likwid.anilist.R.id.bChangeBG);
        sTheme = (Spinner) findViewById(com.likwid.anilist.R.id.sTheme);
        tvVersion = (TextView) findViewById(com.likwid.anilist.R.id.tvVersion);
        Button bEmail = (Button) findViewById(com.likwid.anilist.R.id.bEmail);
        Button bClear = (Button) findViewById(com.likwid.anilist.R.id.bClear);
        tvEmail = (TextView) findViewById(com.likwid.anilist.R.id.tvEmail);
        llPrefsThemes = (LinearLayout) findViewById(com.likwid.anilist.R.id.llPrefThemes);

        heightWidth = llRed.getLayoutParams().height;

        tvVersion.setText("");
        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            tvVersion.setText("v"+versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
		
		llBlue.setOnClickListener(this);
		llRed.setOnClickListener(this);
		llPink.setOnClickListener(this);
		llOrange.setOnClickListener(this);
		llGreen.setOnClickListener(this);
		llPurple.setOnClickListener(this);
		llLime.setOnClickListener(this);
		llYellow.setOnClickListener(this);
		llSky.setOnClickListener(this);
		llSlate.setOnClickListener(this);
        swColorBG.setOnClickListener(this);
        swColorMenu.setOnClickListener(this);
        swBgIsImage.setOnClickListener(this);
        bChangeBG.setOnClickListener(this);
        bEmail.setOnClickListener(this);
        bClear.setOnClickListener(this);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplication());
		colorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
		prefTheme = prefs.getString(getString(com.likwid.anilist.R.string.prefTheme), "default");
        BgIsImage = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefBgIsImage), false);
        prefEmail = prefs.getString(getString(com.likwid.anilist.R.string.prefEmail), "");

		colorBGcurrent = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);
		colorMenuCurrent = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorMenu), true);

        if (!themeFolder.exists()){
            themeFolder.mkdirs();
        }

        themeList.add("default");
        getThemes();

        sTheme.setAdapter(new Adapter_Themes(this, themeList));
        sTheme.setOnItemSelectedListener(this);

        prefEmail = prefs.getString(getString(com.likwid.anilist.R.string.prefEmail), "");
        setCurrentSettings();

        mIabHelper = new IabHelper(this, bepbk1 + bepbk2 + bepbk3 + bepbk4 + bepbk5 + bepbk6 +
                bepbk7 + bepbk8 + bepbk9 + bepbk0);

        mIabHelper.startSetup(this);
	}

    //sets the list of themes and returns the number of themes found
    private int getThemes(){
        PackageManager pm = getPackageManager();
        List<ResolveInfo> themes = pm.queryIntentActivities(new Intent("com.likwid.anilist.THEME"), PackageManager.GET_SHARED_LIBRARY_FILES);

        if (themes.size() > 0) {
            themeList.clear();
            themeList.add(getPackageName());
            for (ResolveInfo theme: themes){
                String packageName = theme.activityInfo.packageName;
                themeList.add(packageName);
            }
        }
        return themes.size();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mIabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.i(TAG, "onActivityResult handled by IABUtil.");
        }


        if (requestCode == REQUEST_CODE_EMAIL && resultCode == RESULT_OK) {
            prefEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            tvEmail.setText("Currently selected email: \n" + prefEmail);
            saveSettings();
        }
    }

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()){
        case com.likwid.anilist.R.id.bClear:
            prefEmail = "";
            tvEmail.setText(getString(com.likwid.anilist.R.string.choose_email));
            saveSettings();
            break;
        case com.likwid.anilist.R.id.bEmail:
            try {
                Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                        new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, null, null, null, null);
                startActivityForResult(intent, REQUEST_CODE_EMAIL);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
		case com.likwid.anilist.R.id.prefBlue:
			colorPrefInt = com.likwid.anilist.R.color.cBlue;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefRed:
			colorPrefInt = com.likwid.anilist.R.color.cRed;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefPink:
			colorPrefInt = com.likwid.anilist.R.color.cPink;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefOrange:
			colorPrefInt = com.likwid.anilist.R.color.cOrange;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefGreen:
			colorPrefInt = com.likwid.anilist.R.color.cGreen;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefPurple:
			colorPrefInt = com.likwid.anilist.R.color.cPurple;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefLime:
			colorPrefInt = com.likwid.anilist.R.color.cLime;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefYellow:
			colorPrefInt = com.likwid.anilist.R.color.cYellow;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefSky:
			colorPrefInt = com.likwid.anilist.R.color.cSky;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.prefSlate:
			colorPrefInt = com.likwid.anilist.R.color.cSlate;
			setCurrentSettings();
			break;
		case com.likwid.anilist.R.id.swPrefChangeBG:
			saveSettings();
			break;
		case com.likwid.anilist.R.id.swPrefsChangeMenu:
			saveSettings();
			break;
        case com.likwid.anilist.R.id.swPrefsShowLabel:
            saveSettings();
            break;
        case com.likwid.anilist.R.id.swPrefImageOrColor:
            if (!hasTheme) {
                mIabHelper.launchPurchaseFlow(this, MainActivity.LISTTHEMES, 10001, this, new Random(20).toString());
                swBgIsImage.setChecked(false);
            }else {
                saveSettings();
            }
            break;
        case com.likwid.anilist.R.id.sTheme:
            if (hasTheme)
                mIabHelper.launchPurchaseFlow(this, MainActivity.LISTTHEMES, 10001, this, new Random(20).toString());
            break;
        case com.likwid.anilist.R.id.bChangeBG:
            if (!hasTheme) {
                mIabHelper.launchPurchaseFlow(this, MainActivity.LISTTHEMES, 10001, this, new Random(20).toString());
            }else {
                Intent cropAct = new Intent("com.likwid.orgalist.CROP");
                startActivity(cropAct);
            }
        break;
		}
	}

	private void setCurrentSettings(){
        prefEmail = prefs.getString(getString(com.likwid.anilist.R.string.prefEmail), "");
        prefTheme = prefs.getString(getString(com.likwid.anilist.R.string.prefTheme), "default");
        BgIsImage = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefBgIsImage), false);
        prefShowLabel = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefShowLabel), true);

        colorBGcurrent = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);
        colorMenuCurrent = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorMenu), true);
        sbMenuBgTrans.setProgress(prefs.getInt(getString(com.likwid.anilist.R.string.pref_menu_alpha), 255));

		int p;
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) llBlue.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llBlue.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llRed.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llRed.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llPurple.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llPurple.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llPink.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llPink.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llOrange.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llOrange.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llGreen.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llGreen.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llLime.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llLime.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llYellow.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llYellow.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llSky.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llSky.setLayoutParams(lp);
			lp = (LinearLayout.LayoutParams) llSlate.getLayoutParams();
			lp.height = heightWidth;
			lp.width = heightWidth;
			llSlate.setLayoutParams(lp);
		
		p = 20;
		
		if (colorPrefInt == com.likwid.anilist.R.color.cBlue){
			lp = (LinearLayout.LayoutParams) llBlue.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llBlue.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cRed){
			lp = (LinearLayout.LayoutParams) llRed.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llRed.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cPurple){
			lp = (LinearLayout.LayoutParams) llPurple.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llPurple.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cPink){
			lp = (LinearLayout.LayoutParams) llPink.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llPink.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cOrange){
			lp = (LinearLayout.LayoutParams) llOrange.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llOrange.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cGreen){
			lp = (LinearLayout.LayoutParams) llGreen.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llGreen.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cLime){
			lp = (LinearLayout.LayoutParams) llLime.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llLime.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cYellow){
			lp = (LinearLayout.LayoutParams) llYellow.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llYellow.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cSky){
			lp = (LinearLayout.LayoutParams) llSky.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llSky.setLayoutParams(lp);
		}else if (colorPrefInt == com.likwid.anilist.R.color.cSlate){
			lp = (LinearLayout.LayoutParams) llSlate.getLayoutParams();
			lp.height = heightWidth + p;
			lp.width = heightWidth + p;
			llSlate.setLayoutParams(lp);
		}
		
		if(colorBGcurrent){
            swColorBG.setChecked(true);
		}else{
            swColorBG.setChecked(false);
		}
		
		if(colorMenuCurrent){
            swColorMenu.setChecked(true);
		}else{
            swColorMenu.setChecked(false);
		}

        if(prefShowLabel){
            swShowLabel.setChecked(true);
        }else{
            swShowLabel.setChecked(false);
        }

        if (BgIsImage){
            swBgIsImage.setChecked(true);
            bChangeBG.setVisibility(View.VISIBLE);
        }else{
            swBgIsImage.setChecked(false);
            bChangeBG.setVisibility(View.GONE);
        }

        if (!prefEmail.equals("")){
            tvEmail.setText("Currently selected email: \n" + prefEmail);
        }else{
            tvEmail.setText(getString(com.likwid.anilist.R.string.choose_email));
        }
	}

    public void saveSettings(){
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(getString(com.likwid.anilist.R.string.prefColorInt), colorPrefInt);
        editor.putBoolean(getString(com.likwid.anilist.R.string.prefColorBG), swColorBG.isChecked());
        editor.putBoolean(getString(com.likwid.anilist.R.string.prefColorMenu), swColorMenu.isChecked());
        editor.putBoolean(getString(com.likwid.anilist.R.string.prefShowLabel), swShowLabel.isChecked());
        editor.putBoolean(getString(com.likwid.anilist.R.string.prefBgIsImage), swBgIsImage.isChecked());
        editor.putString(getString(com.likwid.anilist.R.string.prefEmail), prefEmail);
        editor.putInt(getString(com.likwid.anilist.R.string.pref_menu_alpha), sbMenuBgTrans.getProgress());

        if (swBgIsImage.isChecked()){
            if (bChangeBG.getVisibility() == View.GONE) {
                bChangeBG.startAnimation(AnimationUtils.loadAnimation(this, com.likwid.anilist.R.anim.fade_in));
                bChangeBG.setVisibility(View.VISIBLE);
            }
        }else{
            if (bChangeBG.getVisibility() == View.VISIBLE) {
                bChangeBG.startAnimation(AnimationUtils.loadAnimation(this, com.likwid.anilist.R.anim.fade_out));
                bChangeBG.setVisibility(View.GONE);
            }
        }

		editor.commit();
	}

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (hasTheme) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(getString(com.likwid.anilist.R.string.prefTheme), themeList.get(i));
            editor.commit();
        }else if(i > 0){
            sTheme.setSelection(0);
            mIabHelper.launchPurchaseFlow(this, MainActivity.LISTTHEMES, 10001, this, new Random(20).toString());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveSettings();
    }

    @Override
    public void onIabSetupFinished(com.likwid.anilist.IabResult result) {
        if (result.isSuccess()) {
            final ArrayList<String> skuList = new ArrayList<String>();
            skuList.add(LISTTHEMES);
            mIabHelper.queryInventoryAsync(true, skuList, this);
        }
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        if (result.isFailure()) {
            return;
        }

        if (info.getSku().equals(LISTTHEMES)) {
            Toast.makeText(getApplication(), "Thank you for your purchase! Customize!", Toast.LENGTH_SHORT).show();
            hasTheme = true;
        }

        final ArrayList<String> skuList = new ArrayList<String>();
        skuList.add(MainActivity.LISTTHEMES);
        mIabHelper.queryInventoryAsync(true, skuList, this);
    }

    @Override
    public void onQueryInventoryFinished(IabResult result, Inventory inv) {
        if (result.isFailure()) {
            return;
        }
        hasTheme = inv.hasPurchase(LISTTHEMES);

        if (hasTheme) {
            for (int i = 0; i < themeList.size(); i++) {
                if (prefTheme.equals(themeList.get(i))) {
                    sTheme.setSelection(i);
                }
            }
        }
    }
}
