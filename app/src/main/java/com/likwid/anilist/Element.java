package com.likwid.anilist;

class Element{
	//Status: 0 = incomplete ; 1 = complete
	//Alarm is ID in google calendar
	//Color is the text of the color IE "blue"
	//Marked: 0 = Not Marked ; 1 = Marked for deletion

	int id, priority, alarm;
	String title, folder, color;
    Boolean marked, completed;

	Element(int id, String title, boolean completed, String color, int priority, boolean marked, int alarm, String folder){
		this.id=id;
		this.title=title;
		this.completed=completed;
		this.color = color;
		this.priority = priority;
        this.marked = marked;
		this.alarm = alarm;
		this.folder = folder;
	}
}