package com.likwid.anilist;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by likwid on 9/27/14.
 */
public class SetNotification {
    Context mContext;
    Boolean Sun, Mon, Tue, Wed, Thu, Fri, Sat, Monthly, Yearly = false;
    Calendar mCal, endCal;
    String itemName, email;
    int setColor;
    String rRule;
    ContentResolver cr;
    ContentValues values = new ContentValues();
    private Uri uri;
    private long CalendarID;
    private boolean forever;

    public SetNotification(Context context, Calendar startcal, Calendar endCal, String Title, int Color, String email) {
        this.email = email;
        mContext = context;
        mCal = startcal;
        this.endCal = endCal;
        itemName = Title;
        setColor = Color;
        cr = context.getContentResolver();
    }

    public void SetRepeatDay(Boolean u, Boolean m,Boolean t,Boolean w,Boolean r,Boolean f,Boolean s){
        if (u||m||t||w||r||f||s) Monthly = Yearly = false;
        Sun = u;
        Mon = m;
        Tue = t;
        Wed = w;
        Thu = r;
        Fri = f;
        Sat = s;
    }

    public void EventIsForever(Boolean everlast){
        forever = everlast;
    }

    public void SetMonthly(Boolean monthly){
        if (monthly) Sun=Mon=Tue=Wed=Thu=Fri=Sat=Yearly= false;
        Monthly = monthly;
    }

    public void SetYearly(Boolean yearly){
        if (yearly) Sun=Mon=Tue=Wed=Thu=Fri=Sat=Yearly= false;
        Yearly = yearly;
    }

    public void MakeReminder(){
        final String[] EVENT_PROJECTION = new String[]{
                CalendarContract.Calendars._ID,                           // 0
                CalendarContract.Calendars.ACCOUNT_NAME,                  // 1
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,         // 2
                CalendarContract.Calendars.OWNER_ACCOUNT                  // 3
        };

        // The indices for the projection array above.
        final int PROJECTION_ID_INDEX = 0;

        // Run query
        Cursor cur;
        ContentResolver cr = mContext.getContentResolver();
        uri = CalendarContract.Calendars.CONTENT_URI;
        String selection = "((" + CalendarContract.Calendars.ACCOUNT_NAME + " = ?) AND ("
                + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?) AND ("
                + CalendarContract.Calendars.OWNER_ACCOUNT + " = ?))";
        String[] selectionArgs = new String[]{email, "com.google", email};
        // Submit the query and get a Cursor object back.
        cur=cr.query(uri,EVENT_PROJECTION,selection,selectionArgs,null);

        // Use the cursor to step through the returned records
        for(cur.moveToFirst();!cur.isAfterLast();cur.moveToNext())
        {
            // Get the field values
            CalendarID = cur.getLong(PROJECTION_ID_INDEX);
        }

        cur.close();

        if(Monthly){
            rRule = "FREQ=MONTHLY;";
        }else if(Yearly){
            rRule = "FREQ=YEARLY;";
        }

        if(Sun||Mon||Tue||Wed||Thu||Fri||Sat){
            rRule = "FREQ=DAILY;BYDAY=";
            if (Sun) rRule += "SU,";
            if (Mon) rRule += "MO,";
            if (Tue) rRule += "TU,";
            if (Wed) rRule += "WE,";
            if (Thu) rRule += "TH,";
            if (Fri) rRule += "FR,";
            if (Sat) rRule += "SA,";

            rRule = rRule.substring(0, rRule.length() - 1) + ";";
        }

        if (rRule == null)
            rRule = "FREQ=DAILY;INTERVAL=1;";

        if (!forever)
            rRule += "UNTIL=" + new SimpleDateFormat("yyyyMMdd").format(endCal.getTime())+"T235959";
    }

    public long insertReminder(){
        values.clear();

        //Insert Values
        values.put(CalendarContract.Events.DTSTART, mCal.getTimeInMillis());
        values.put(CalendarContract.Events.RRULE, rRule);
        values.put(CalendarContract.Events.DURATION, getDuration());
        values.put(CalendarContract.Events.EVENT_COLOR, setColor);
        values.put(CalendarContract.Events.TITLE, itemName);
        values.put(CalendarContract.Events.DESCRIPTION,"Added from "+ mContext.getString(com.likwid.anilist.R.string.app_name));
        values.put(CalendarContract.Events.CALENDAR_ID, CalendarID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE,TimeZone.getDefault().getID());
        values.put(CalendarContract.Events.HAS_ALARM,1);

        //set URI and get eventID
        uri=cr.insert(CalendarContract.Events.CONTENT_URI, values);
        long eventID = Long.parseLong(uri.getLastPathSegment());

        //Insert reminder
        values.clear();
        values.put(CalendarContract.Reminders.EVENT_ID, eventID);
        values.put(CalendarContract.Reminders.METHOD,CalendarContract.Reminders.METHOD_ALERT);
        values.put(CalendarContract.Reminders.MINUTES,0);
        cr.insert(CalendarContract.Reminders.CONTENT_URI,values);

        Toast.makeText(mContext, itemName + " has been scheduled.", Toast.LENGTH_SHORT).show();
        return eventID;
    }

    public long deleteReminder(int eventId){
        uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventId);
        if (uri != null) {
            mContext.getContentResolver().delete(uri, null, null);
        }

        return insertReminder();
    }

    private String getDuration(){
        Calendar startTime = Calendar.getInstance();
        Calendar endTime = Calendar.getInstance();
        startTime.setTimeInMillis(mCal.getTimeInMillis());
        endTime.setTimeInMillis(endCal.getTimeInMillis());
        endTime.set(startTime.get(Calendar.YEAR), startTime.get(Calendar.MONTH), startTime.get(Calendar.DAY_OF_MONTH));
        long duration = endTime.getTimeInMillis() - startTime.getTimeInMillis();

        duration = duration/1000L;
        int h = 0;
        while (duration >= 3600L){
            h++;
            duration -= 3600L;
        }

        int m = 0;
        while (duration >= 60L) {
            m++;
            duration -= 60L;
        }

        int s = 0;
        while (duration >= 1L){
            s++;
            duration -= 1L;
        }
        return "PT"+h+"H"+m+"M"+s+"S";
    }
}
