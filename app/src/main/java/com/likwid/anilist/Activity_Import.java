package com.likwid.anilist;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;

public class Activity_Import extends Activity{

	static final File folder = MainActivity.EXPORT_FOLDER;
	static final String header = MainActivity.HEADER;
	Adapter_SQL database;
    LinearLayout llMain;
	int selected;
	String importText;
	String nameOfParent;

	@Override
	public void onCreate (Bundle savedInstanceState) {
		database = new Adapter_SQL(this);
		selected = getIntent().getIntExtra("selected", 0);
		nameOfParent = "the list";
		if (selected > 0){
			database.open();
			nameOfParent = database.getName(selected);
			database.close();
		}

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int colorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
		
		Boolean ColorBG = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);
		
		if (ColorBG){
			llMain.setBackgroundColor(getResources().getColor(colorPrefInt));
		}else{
			llMain.setBackgroundColor(Color.BLACK);
		}
		
		//Detect clipboard
		ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		if (clipboard.hasPrimaryClip()){
			ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
			this.importText = item.getText().toString();
			if (importText.startsWith(header)){
				selected = getIntent().getIntExtra("selected", 0);
				importText = importText.substring(header.length());
				new ImportText(getApplication(), selected).importText(importText);
			}else{
				Toast.makeText(getApplication(), "Make sure you copy the entire text.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}else{
			Toast.makeText(getApplication(), "Couldn't find any thing to copy in.\nPlease copy the text first.", Toast.LENGTH_SHORT).show();
			finish();
		}
	}
}
