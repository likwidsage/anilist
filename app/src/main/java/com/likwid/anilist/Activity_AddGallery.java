package com.likwid.anilist;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Activity_AddGallery extends FragmentActivity {

	private int RESULT_GALLERY = 0;
	final File folder = MainActivity.PHOTOS_FOLDER;
	Bitmap pic;
	Intent mIntent;
	int selected;
	Adapter_SQL database;
	Element parentInfo;
	Button bAddPic, bAddPicCancel;
	ImageView ivAddPic;
	Uri photoUri;
	Intent pickGal;
	File fileName;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		database = new Adapter_SQL(this);
		mIntent = getIntent();
		selected = mIntent.getExtras().getInt("selected", 0);

		if (selected > 0) {
			createDir();
			database.open();
			parentInfo = database.getSingle(selected);
			database.close();

//			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO){
//				pickGal = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//				pickGal.addCategory(Intent.CATEGORY_OPENABLE);
//				pickGal.setType("image/*");
//			}else{
				pickGal = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				pickGal.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//			}
			
			startActivityForResult(pickGal, RESULT_GALLERY);

		} else {
			Toast.makeText(getApplication(), "Select item to main_add picture to.",
					Toast.LENGTH_LONG).show();
			finish();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == RESULT_GALLERY && resultCode == RESULT_OK) {
//			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO){
//				
//				
//			}else{
				photoUri = intent.getData();
				Bitmap pic = uriToBitmap(photoUri);
				OutputStream outStream = null;
				fileName = SetFile();
				
				try {					
					outStream = new FileOutputStream(fileName);
					pic.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
					outStream.flush();
					outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				String filename = fileName.getName();
				database.open();
				database.addPhoto(filename, selected);
				database.close();
				Toast.makeText(getApplicationContext(), filename, Toast.LENGTH_SHORT).show();
				

//			}
		}
		finish();
	}

	public Bitmap uriToBitmap(Uri uri) {
		String[] Column = { MediaColumns.DATA };

		Cursor cursor = getContentResolver().query(uri, Column, null, null,
				null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(Column[0]);
		String filePath = cursor.getString(columnIndex);
		cursor.close();

		Bitmap bitmapImage = BitmapFactory.decodeFile(filePath);
		return bitmapImage;
	}

	private void createDir() {
		if (!folder.exists()) {
			folder.mkdir();
		}
		// Create . nomedia
		File nomedia = new File(folder, ".nomedia");
		if (!nomedia.exists()) {
			try {
				FileOutputStream nomediaOut = new FileOutputStream(nomedia);
				nomediaOut.flush();
				nomediaOut.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private File SetFile(){
		database.open();
		int picNum = database.getCount() + 1;
		database.close();

		String StringNum = String.format("%05d", picNum);
		File image = new File(folder, "ElementPic" + StringNum + ".jpg");

		return image;
	}

}
