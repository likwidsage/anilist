package com.likwid.anilist;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;


public class WidgetConfigure extends Activity implements View.OnClickListener {

    Spinner sWidgetConfigure;
    Button bWidgetConfigure;
    Adapter_SQL database;
    ArrayList<Element> list;
    Sort_Elements sort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);
        getActionBar().hide();
        setContentView(com.likwid.anilist.R.layout.activity_widget_configure);

        sWidgetConfigure = (Spinner) findViewById(com.likwid.anilist.R.id.sWidgetConfigure);
        bWidgetConfigure = (Button) findViewById(com.likwid.anilist.R.id.bWidgetConfigure);

        database = new Adapter_SQL(getApplicationContext());
        sort = new Sort_Elements();

        database.open();
        list = database.getAll();
        Element mainList = new Element(0, "Main List", false, "blue", 0, false, 0, "0/");

        list = sort.sortFolder(list);
        list.add(0, mainList);

        ArrayList<String> titles = new ArrayList<>();
        for (Element item: list){
            String prefix = "";
            int level = database.getLevel(item.id);
            for (int i = 0; i < level; i++){
                prefix = prefix + "\t\t";
            }
            titles.add(prefix + item.title);
        }
        database.close();
        sWidgetConfigure.setAdapter(new Adapter_spinner(getApplicationContext(), titles, "#000000", "#00FFFFFF"));

        bWidgetConfigure.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case com.likwid.anilist.R.id.bWidgetConfigure:
                Bundle extras = getIntent().getExtras();
                int posPicked = sWidgetConfigure.getSelectedItemPosition();
                int id = list.get(posPicked).id;
                int wID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(wID+"IdForWidget", id);
                editor.commit();

                new WidgetProvider().onUpdate(this,
                        AppWidgetManager.getInstance(this),
                        AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, WidgetProvider.class)
                        )
                );

                Intent resultValue = new Intent();
                resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, wID);
                setResult(RESULT_OK, resultValue);
                finish();
            break;
        }
    }
}
