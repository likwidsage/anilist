package com.likwid.anilist;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ImportText {

    static final File folder = MainActivity.EXPORT_FOLDER;
    static final String header = MainActivity.HEADER;
    static final String starter = MainActivity.STARTER;
    static final String starterDone = MainActivity.STARTER_DONE;
    static final String seperator = MainActivity.SEPERATOR;
    static final String footer = MainActivity.FOOTER;

    Adapter_SQL database;
    Context mContext;
    private boolean importError;
    int selected;

    public ImportText(Context c, int parent){
        mContext = c;
        selected = parent;
        database = new Adapter_SQL(mContext);
    }

    public Boolean TextIsGood(String importText){
        if (!importText.startsWith(header)
                || !importText.endsWith(footer)
                || !importText.contains(seperator)
                || !(importText.contains(starter) || importText.contains(starterDone)) )
        {
            return false;
        }else{
            return true;
        }
    }

    public Boolean importText(String text) {
        String importText = text.trim();
        Log.d("importText", importText);
        if (!TextIsGood(importText)){
            Toast.makeText(mContext, "Error Importing. Format Incorrect", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            importText = importText.substring(MainActivity.HEADER.length(), (importText.length() - MainActivity.FOOTER.length()));
        }

        String[] lines = importText.split("\n");
        database.open();
        int pid;
        String[] components;

        // Get MaxLevel
        int maxLevel = 0;
        for (int i = 1; i < lines.length; i++) {
            components = lines[i].substring(3).split(seperator); // 0 = level // 1 = type // 2 = title
            if (Integer.parseInt(components[0]) > maxLevel) {
                maxLevel = Integer.parseInt(components[0]);
            }
        }

        // the level will be the place ( IE: l2p[0] will be top level, l2p[1]
        // will be the child of l2p[0] )
        // the value will be the id of that level ( IE: If l2p[0] = 20 then pid
        // of l2p[1] = 20 )
        int[] l2p = new int[maxLevel + 1];

        for (String line : lines) {

            components = line.substring(3).split(seperator); // 0 = level // 1 = type // 2 = title
            if ((components[1].equals("E") && components.length == 4)
                    || (components[1].equals("N") && components.length == 3)) {
                int level = Integer.parseInt(components[0]);

                // Set the parent of the item to be created
                if (level == 0) {
                    // set parent to the selected element if top item
                    pid = selected;
                } else {
                    // If not the top item, get the the id from the level above
                    pid = l2p[level - 1];
                }

                // Write the item to the list depending by what type it is.
                if (components[1].equals("E")) {
                    database.createElement(components[2].trim(), components[3].trim(), 0, database.getPath(pid));
                } else if (components[1].equals("N")) {
                    database.addNote(components[2].trim(), pid);
                }

                // If the line designates it's done (starts with STARTER_DONE) set the last created
                // Task to complete.
                if (line.startsWith(starterDone)) {
                    database.switchComplete(database.getLast().id);
                }

                // Set the correct level in l2p to the last created id
                l2p[level] = database.getLast().id;

            } else {
                importError = true;
                break;
            }
        }

        String nameOfSelected = "main list";
        if (selected > 0){
            nameOfSelected = "'"+ database.getName(selected) +"'";
        }
        database.close();

        if (!importError){
            String toastString = "Task(s) imported to " + nameOfSelected + ".";
            Toast.makeText(mContext, toastString, Toast.LENGTH_LONG).show();
            return true;
        }else{
            Toast.makeText(mContext, "Error importing file", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    //Reading ani files from filename
    public String ReadAni(File filename){
        return ReadAni(Uri.fromFile(filename));
    }

    //Decompress file from Uri and reaReturns the list as a string or nullsuccess.
    public String ReadAni(Uri uri){
        try {
            byte[] BUFFER = new byte[1024];

            InputStream aniFile = mContext.getContentResolver().openInputStream(uri);
            ZipInputStream zis = new ZipInputStream(aniFile);
            ZipEntry entry = zis.getNextEntry();

            //Create directory if it doesn't exist
            if(!MainActivity.TMP_DIR.exists())
                if (MainActivity.TMP_DIR.mkdir())
                    Log.d("Read Ani - MkDir", "Success");

            //Extract "list" and write it to tmp folder
            File listFile = new File(MainActivity.TMP_DIR, entry.getName());
            FileOutputStream fos = new FileOutputStream(listFile);
            int read = 0;
            while ((read = zis.read(BUFFER)) > 0){
                fos.write(BUFFER, 0, read);
            }
            Log.d ("Read Ani - List", listFile.getPath());

            //Read the list into the textbox
            InputStream in = new FileInputStream(listFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line, list = "";
            while ((line = reader.readLine()) != null) list += line + "\n";

            //Delete file and folder after finishing importing
            if (listFile.delete())
                if (MainActivity.TMP_DIR.delete())
                    Log.d("Read Ani - Delete", "Success");

            zis.closeEntry();
            zis.close();
            fos.close();

            Log.d("Read Ani - List", list);
            return list.trim();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
