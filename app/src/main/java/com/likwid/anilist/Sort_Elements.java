package com.likwid.anilist;

import android.content.Context;
import android.database.Cursor;
import android.provider.CalendarContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Sort_Elements{

	ArrayList<Element> resultList;

    public Sort_Elements() {
	}

    public ArrayList<Element> sortMarked(ArrayList<Element> list){
        ArrayList<Element> newList = new ArrayList<>();
        Boolean found = false;
        //Add all the NON marked items first
        for (int i = 0; i < list.size(); i++){
            //Set back counter to start from last found place after removing
            if (found){
                i--;
                found =false;
            }

            if (!list.get(i).marked){
                newList.add(list.get(i));
                list.remove(i);
                found = true;
            }
        }

        //Add the remaining
        for (int i = 0; i < list.size(); i++){
            if (list.get(i).marked){
                newList.add(list.get(i));
            }
        }

        return newList;
    }
	
	public ArrayList<Element> sortPriorities(ArrayList<Element> list){
		Collections.sort(list, new Comparator<Element>() {
			@Override
			public int compare(Element lhs, Element rhs) {
				if (lhs.priority > rhs.priority){
					return 1;
				}else if (lhs.priority < rhs.priority){
					return -1;
				}else{
					return lhs.title.compareTo(rhs.title);
				}
			}
		});
		return list;
	}
	
	public ArrayList<Element> sortColor(ArrayList<Element> list){
		Collections.sort(list, new Comparator<Element>() {
			@Override
			public int compare(Element lhs, Element rhs) {
				int i = lhs.color.compareToIgnoreCase(rhs.color);
				if (i == 0){
					i = lhs.title.compareToIgnoreCase(rhs.title);
					if (i == 0){
						if (lhs.id > rhs.id){
							i = 1;
						}
					}else if (lhs.id < rhs.id){
						i = -1;
					}else if (lhs.id == rhs.id){
						i = 0;
					}
				}
				return i;
			}
		});
		return list;
	}
	
	public ArrayList<Element> sortAlphabetically(ArrayList<Element> list){
		Collections.sort(list, new Comparator<Element>() {
			@Override
			public int compare(Element lhs, Element rhs) {
				return lhs.title.compareToIgnoreCase(rhs.title);
			}
		});
		return list;
	}

    public ArrayList<Element> sortAlarm(ArrayList<Element> list, final Context context){
        Collections.sort(list, new Comparator<Element>() {
            @Override
            public int compare(Element lhs, Element rhs) {
                final String[] CALCOL = {CalendarContract.Events.DTSTART};
                Cursor c = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI,
                        CALCOL,
                        CalendarContract.Events._ID + "=" + lhs.alarm,
                        null,
                        null
                );
                c.moveToFirst();
                Long lhstime = Long.valueOf(c.getString(c.getColumnIndex(CalendarContract.Events.DTSTART)));


                c = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI,
                        CALCOL,
                        CalendarContract.Events._ID + "=" + rhs.alarm,
                        null,
                        null
                );
                c.moveToFirst();
                Long rhstime = Long.valueOf(c.getString(c.getColumnIndex(CalendarContract.Events.DTSTART)));

                return lhstime.compareTo(rhstime);
            }
        });
        return list;
    }

	public ArrayList<Element> sortFolder(ArrayList<Element> list){
		ArrayList<Element> endList = new ArrayList<Element>();
        if (list.size() == 0){ return endList; }
        if (list.size() == 1){ return list; }

        endList.add(list.get(0));
		list.remove(0);
        Boolean matchFound = false;

        int resultIndex = 0;
        while (list.size() > 0){
            matchFound = false;
            int listIndex = 0;

            //Check List for EXPORT_FOLDER containing another EXPORT_FOLDER
            while (listIndex < list.size()){
                //If EXPORT_FOLDER is found, main_add it to the endList and remove it from list
                if (list.get(listIndex).folder.equals(endList.get(resultIndex).folder + endList.get(resultIndex).id + "/")){
                    endList.add(list.get(listIndex));
                    list.remove(listIndex);
                    matchFound = true;
                    break;
                }

                //If no match is found search for the next EXPORT_FOLDER in list
                if (!matchFound){
                    listIndex++;
                }
            }

            //If no matching folders are left (or found) in the whole list,
            // main_add the item "endList" and search for that
            if (!matchFound){
                endList.add(list.get(0));
                list.remove(0);
                resultIndex++;
            }
        }

		return endList;
	}
}
