package com.likwid.anilist;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Frag_Calendar_Main extends Fragment implements AdapterView.OnItemSelectedListener,
        View.OnClickListener{

    public final static String FRAG_NAME = "frag_cal";

    private int REQUEST_CODE_EMAIL = 600;
    File folder = MainActivity.EXPORT_FOLDER;
    Adapter_SQL database;
    static Boolean refreshPressed = false;
    FragmentManager fm;
    Frag_Calendar_View.FragmentCalendar fc;

    FrameLayout flCalView, flCalList;

    SharedPreferences prefs;

    static private ImageView ivCalRefresh;
    private RelativeLayout rlCalMainBG;
    Spinner sMonth, sYear;
    Handler handler = new Handler();
    static Runnable resetRefresh = new Runnable() {
        @Override
        public void run() {
            refreshPressed = false;
            ivCalRefresh.setVisibility(View.VISIBLE);
        }
    };

    static String ARG_SECTION_NUMBER = "section_args";

    public Frag_Calendar_Main() {
    }

    public static Fragment newInstance(int sectionNumber) {
        Fragment fragment = new Frag_Calendar_Main();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(com.likwid.anilist.R.layout.frag_calendar_main, container, false);
        ivCalRefresh = (ImageView) view.findViewById(com.likwid.anilist.R.id.ivCalRefresh);
        rlCalMainBG = (RelativeLayout) view.findViewById(com.likwid.anilist.R.id.rlCalMainBG);
        sMonth = (Spinner) view.findViewById(com.likwid.anilist.R.id.sMonths);
        sYear = (Spinner) view.findViewById(com.likwid.anilist.R.id.sYear);
        flCalList = (FrameLayout) view.findViewById(com.likwid.anilist.R.id.flCalList);
        flCalView = (FrameLayout) view.findViewById(com.likwid.anilist.R.id.flCalView);

        sMonth.setOnItemSelectedListener(this);
        sYear.setOnItemSelectedListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fc = (Frag_Calendar_View.FragmentCalendar) getActivity();
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        database = new Adapter_SQL(getActivity());

        ArrayList<String> monthsList = new ArrayList<String>();
        monthsList.clear();
        Calendar monthsCal = Calendar.getInstance();
        for (int z = 0; z <= monthsCal.getActualMaximum(Calendar.MONTH); z++){
            monthsCal.set(Calendar.MONTH, z);
            monthsList.add(new SimpleDateFormat("MMMM").format(monthsCal.getTime())+"");
        }
        sMonth.setAdapter(new Adapter_spinner(getActivity(), monthsList, "#000000", "#00000000"));
        sMonth.setSelection(Calendar.getInstance().get(Calendar.MONTH));
        sMonth.setOnItemSelectedListener(this);

        ArrayList<String> yearsList = new ArrayList<String>();
        yearsList.clear();
        for (int z = 0; z < 10; z++){
            yearsList.add(Calendar.getInstance().get(Calendar.YEAR) - (4-z)+"");
        }
        sYear.setAdapter(new Adapter_spinner(getActivity(), yearsList, "#000000", "#00000000"));
        sYear.setSelection(4);
        sYear.setOnItemSelectedListener(this);
        ivCalRefresh.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fm = getActivity().getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(com.likwid.anilist.R.id.flCalList, new Frag_Calendar_List(), "CalList");
        ft.replace(com.likwid.anilist.R.id.flCalView, new Frag_Calendar_View(), "CalView");
        ft.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_EMAIL && resultCode == Activity.RESULT_OK) {
            String email = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            prefs.edit().putString(getString(com.likwid.anilist.R.string.prefEmail), email).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int ColorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);

        rlCalMainBG.setBackgroundColor(getResources().getColor(ColorPrefInt));
    }

    @Override
    public void onClick(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(getActivity(), com.likwid.anilist.R.anim.refresh_cal_button));
        switch (view.getId()){
            case com.likwid.anilist.R.id.ivCalRefresh:
                if (fc.getDate().get(Calendar.YEAR) != Calendar.getInstance().get(Calendar.YEAR)
                        || fc.getDate().get(Calendar.MONTH) != Calendar.getInstance().get(Calendar.MONTH)){
                    if (!refreshPressed) {
                        ivCalRefresh.setVisibility(View.INVISIBLE);
                        refreshPressed = true;
                        handler.postDelayed(resetRefresh, 4000);

                        fc.resetDate();
                        sMonth.setSelection(fc.getDate().get(Calendar.MONTH));
                        sYear.setSelection(4);
                    }
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Frag_Calendar_View.FragmentCalendar fc = (Frag_Calendar_View.FragmentCalendar) getActivity();
        switch (adapterView.getId()){
            case com.likwid.anilist.R.id.sMonths:
                fc.setMonth(i);
                fc.refreshCalendar();
                break;
            case com.likwid.anilist.R.id.sYear:
                int selectedYear = Calendar.getInstance().get(Calendar.YEAR) - (4-i);
                fc.setYear(selectedYear);
                fc.refreshCalendar();
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException i){
            throw new RuntimeException(i);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

