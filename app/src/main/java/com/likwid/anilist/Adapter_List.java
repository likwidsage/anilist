package com.likwid.anilist;

import android.animation.Animator;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewAnimationUtils;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.TimerTask;

public class Adapter_List extends BaseAdapter implements OnClickListener, OnLongClickListener, View.OnTouchListener{

    private Context mContext;
    public final static String TINYTAG = "tiny_icon";
    public final static String ICONTAG = "ivIconBG";
    public final static String SELECT_TAG = "rlMainRow";
    public final static String SELECT_TAG2 = "rlMainRow2";

    ArrayList<Element> values;
    int selected;
    Adapter_SQL database;
    View row, touchView;
    MainActivity toSlide;
    Animation selectRow;
    int vid;
    float sX = 0, sY = 0, dX = 0, oX = 0, oY = 0;
    Boolean touchMoved;
    Handler handler = new Handler();
    long clickDelay = ViewConfiguration.getDoubleTapTimeout();
    ArrayList<Integer> selections;

    Runnable TapDetectTask;
    int ColorPrefInt;

    DisplayMetrics dm = new DisplayMetrics();

    int slctColor;
    private long downTimer = 0;
    private boolean touchUped = false;
    private boolean doubleClicked;
    private boolean ChangeInX;
    private boolean scrolling;
    int minLevel;
    private boolean slideOpened;
    private boolean rightSideTouch;
    private Element single;

    public Adapter_List(Context c, ArrayList<Element> list){
        dm = c.getResources().getDisplayMetrics();
        mContext = c;
        values = list;
        database = new Adapter_SQL(mContext);
        toSlide = (MainActivity) mContext;
        selections = toSlide.getSelection();

        TapDetectTask = new TimerTask() {
            @Override
            public void run() {
                if (downTimer >= clickDelay && !touchMoved && !doubleClicked && !scrolling) {
                    if (touchUped) {
                        singleClick(touchView, vid);
                    }else{
                        longPress();
                    }
                }else if (!touchMoved && doubleClicked){
                    doubleClick(sX, sY);
                }
                touchUped =false;
                downTimer = 0;
            }
        };

        minLevel = 0;
        if (list.size() > 0) {
            database.open();
            minLevel = database.getLevel(list.get(0).id);
            for (Element element : list) {
                if (database.getLevel(element.id) < minLevel) {
                    minLevel = database.getLevel(element.id);
                }
            }
            database.close();
        }

        //read preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        ColorPrefInt = prefs.getInt(mContext.getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);

        selectRow = AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.list_select);
    }

    static class RowHolder{
        LinearLayout llInfo, llTinyIcons;
        RelativeLayout rlMainRow, rlSelect, rlSelect2;
        TextView rowText, iconLetter, tvSub;
        ImageView ivTinyAlarm, ivTinyNote, ivTinyPic, ivTinyUrl;
    }

    @Override
    public View getView(int pos, View convert, ViewGroup parent) {
        row = convert;
        if (row == null) {
            LayoutInflater inflate = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflate.inflate(com.likwid.anilist.R.layout.adapter_main_row, parent, false);

            RowHolder rowholder = new RowHolder();
            rowholder.llInfo = (LinearLayout) row.findViewById(com.likwid.anilist.R.id.llInfo);
            rowholder.llTinyIcons = (LinearLayout) row.findViewById(com.likwid.anilist.R.id.llTinyIcons);
            rowholder.rlMainRow = (RelativeLayout) row.findViewById(com.likwid.anilist.R.id.rlMainRow);
            rowholder.rlSelect = (RelativeLayout) row.findViewById(com.likwid.anilist.R.id.rlMainSelect);
            rowholder.rlSelect2 = (RelativeLayout) row.findViewById(com.likwid.anilist.R.id.rlMainSelect2);
            rowholder.rowText = (TextView) row.findViewById(com.likwid.anilist.R.id.tvItemText);
            rowholder.iconLetter = (TextView) row.findViewById(com.likwid.anilist.R.id.tvRow_IconLetter);
            rowholder.ivTinyAlarm = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyAlarm);
            rowholder.ivTinyNote = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyNote);
            rowholder.ivTinyPic = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyPic);
            rowholder.ivTinyUrl = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyURL);
            rowholder.tvSub = (TextView) row.findViewById(com.likwid.anilist.R.id.tvMainRowSub);
            row.setTag(rowholder);
        }

        Boolean selected = false;
        for (Integer selection : selections) {
            if (selection == values.get(pos).id) {
                selected = true;
            }
        }

        RowHolder holder = (RowHolder) row.getTag();
        database.open();

        if (!values.get(pos).marked) {
            if (values.get(pos).alarm > 0) {
                holder.ivTinyAlarm.setVisibility(View.VISIBLE);
            } else {
                holder.ivTinyAlarm.setVisibility(View.GONE);
            }

            if (database.hasPhotos((values.get(pos).id))) {
                holder.ivTinyPic.setVisibility(View.VISIBLE);
            } else {
                holder.ivTinyPic.setVisibility(View.GONE);
            }

            if (database.hasNotes(values.get(pos).id)) {
                holder.ivTinyNote.setVisibility(View.VISIBLE);
            } else {
                holder.ivTinyNote.setVisibility(View.GONE);
            }
        } else {
            holder.ivTinyAlarm.setVisibility(View.GONE);
            holder.ivTinyPic.setVisibility(View.GONE);
            holder.ivTinyNote.setVisibility(View.GONE);
        }

        database.close();

        if (values.get(pos).title.contains("http://") && !values.get(pos).marked) {
            holder.ivTinyUrl.setVisibility(View.VISIBLE);
            String title = values.get(pos).title;
            int startPos = title.indexOf("http://");
            int endPos = title.substring(startPos).indexOf(" ");

            if (endPos == -1) {
                holder.ivTinyUrl.setTag(title.substring(startPos));
            } else {
                holder.ivTinyUrl.setTag(title.substring(startPos, endPos));
            }

        } else {
            holder.ivTinyUrl.setVisibility(View.GONE);
        }

        if (values.get(pos).title.length() > 0) {
            String iconText = values.get(pos).title.substring(0, 1).toUpperCase();
            holder.iconLetter.setText(iconText);
        }else{
            holder.iconLetter.setText("");
        }

        holder.rowText.setText(values.get(pos).title);

        //If Completed
        if (values.get(pos).completed) {
            holder.rowText.setAlpha(.25f);
            holder.tvSub.setAlpha(.25f);
            holder.rowText.setPaintFlags(holder.rowText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.iconLetter.setVisibility(View.INVISIBLE);
        } else {
            holder.rowText.setPaintFlags(holder.rowText.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.iconLetter.setVisibility(View.VISIBLE);
            holder.rowText.setAlpha(1f);
            holder.tvSub.setAlpha(1f);
        }


        slctColor = ElementColor(values.get(pos).color);

        //Item marked for deletion
        if (!values.get(pos).marked) {
            holder.iconLetter.setVisibility(View.VISIBLE);
            ShapeDrawable iconBG = new ShapeDrawable(new OvalShape());
            iconBG.setIntrinsicWidth(200);
            iconBG.setIntrinsicHeight(200);
            iconBG.getPaint().setColor(slctColor);
            holder.iconLetter.setBackground(iconBG);
        }else{
            holder.iconLetter.setVisibility(View.INVISIBLE);
            holder.iconLetter.setBackgroundResource(com.likwid.anilist.R.drawable.delete);
        }

        //Set row background color
        if (selected) {
            holder.rlSelect.setBackgroundColor(slctColor);
            holder.rlSelect2.setBackgroundColor(slctColor);
        }else{
            holder.rlSelect.setBackgroundColor(Color.TRANSPARENT);
            holder.rlSelect2.setBackgroundColor(Color.TRANSPARENT);
        }

        database.open();
        int subTotal = database.getChildCount(values.get(pos).id);
        int subComplete = database.getCompletedChildCount(values.get(pos).id);
        database.close();

        if (!values.get(pos).marked){
            holder.tvSub.setVisibility(View.VISIBLE);
            if (subTotal > 0) {
                holder.tvSub.setText("Sub tasks: " + subComplete + " of " + subTotal + " completed.");
            } else {
                holder.tvSub.setText("No Sub Tasks");
            }
        }else{
            holder.tvSub.setVisibility(View.GONE);
        }

        //if the item is marked for deletion, edit it to show the difference
        if (values.get(pos).marked){
            holder.rlMainRow.setAlpha(0.25f);
        }else{
            holder.rlMainRow.setAlpha(1f);
        }

        holder.rlSelect.setTag(values.get(pos).id+SELECT_TAG);
        holder.rlSelect2.setTag(values.get(pos).id + SELECT_TAG2);

        holder.llTinyIcons.setTag(values.get(pos).id + TINYTAG);

        holder.iconLetter.setOnClickListener(this);
        holder.llTinyIcons.setOnClickListener(this);
        holder.iconLetter.setOnLongClickListener(this);
        holder.iconLetter.setTag(values.get(pos).id + ICONTAG);

        holder.rlMainRow.setTag(values.get(pos).id);
        holder.rlMainRow.setOnDragListener(new DragListener(mContext));

        holder.rlMainRow.setOnTouchListener(this);

        return row;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Element getItem(int pos) {
        return new Element(values.get(pos).id,
                values.get(pos).title,
                values.get(pos).completed,
                values.get(pos).color,
                values.get(pos).priority,
                values.get(pos).marked,
                values.get(pos).alarm,
                values.get(pos).folder
                );
    }

    @Override
    public long getItemId(int pos) {
        return values.get(pos).id;
    }

    //Toggle selection of id. X and Y are coordinates for circular reveal
    public void setSelected(int id, float x, float y) {
        if (toSlide.getSelection().contains(id)){
            toSlide.removeSelected(id, x, y);
        }else{
            toSlide.addSelection(id, x, y);
        }
    }

    @Override
    public void onClick(View v) {
        int id;
        switch (v.getId()) {
            case com.likwid.anilist.R.id.tvRow_IconLetter:
                String tag = (String) v.getTag();
                id = Integer.parseInt(tag.substring(0, tag.length() - ICONTAG.length()));

                Rect view = new Rect();
                v.getLocalVisibleRect(view);
                float x = view.left + view.width()/2;
                float y = view.top + view.height()/2;

                setSelected(id, x, y);
                break;
            case com.likwid.anilist.R.id.llTinyIcons:
                String tag2 = (String) v.getTag();
                id = Integer.parseInt(tag2.substring(0, tag2.length() - TINYTAG.length()));
                toSlide.showTinyOptions(id);
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        String tag = (String) v.getTag();
        int id = Integer.parseInt(tag.substring(0, tag.length() - ICONTAG.length()));
        switch (v.getId()) {
            case com.likwid.anilist.R.id.ivIconBG:
                database.open();
                toolTip("What does this do?", "Selects '" + database.getName(id) + "'.");
                database.close();
                break;
        }
        return true;
    }

    public void toolTip(String title, String text) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View helpView = li.inflate(com.likwid.anilist.R.layout.diag_help, null);
        TextView helpText = (TextView) helpView.findViewById(com.likwid.anilist.R.id.tvHelpText);
        Button bDismiss = (Button) helpView.findViewById(com.likwid.anilist.R.id.bDiagHelpDismis);
        helpText.setText(text);

        final Dialog diag = new Dialog(mContext);
        diag.setTitle(title);
        diag.setContentView(helpView);
        diag.setCanceledOnTouchOutside(true);
        bDismiss.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                diag.dismiss();
            }
        });

        diag.show();
    }

    public int ElementColor(String color){
        int oldSetColor = 0;
        if (color.equals("blue")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cBlue);
        } else if (color.equals("red")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cRed);
        } else if (color.equals("purple")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cPurple);
        } else if (color.equals("pink")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cPink);
        } else if (color.equals("orange")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cOrange);
        } else if (color.equals("green")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cGreen);
        } else if (color.equals("lime")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cLime);
        } else if (color.equals("yellow")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cYellow);
        } else if (color.equals("sky")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cSky);
        } else if (color.equals("slate")) {
            oldSetColor = mContext.getResources().getColor(com.likwid.anilist.R.color.cSlate);
        }
        return oldSetColor;
    }

    public void singleClick(View touchView, final int viewId){
        if (!slideOpened) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                Animation zoom = AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.zoomin);
                zoom.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        toSlide.goInto(vid);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                touchView.startAnimation(zoom);
            }else{
                toSlide.goInto(viewId);
            }
        }else{
            slideOpened = false;
        }

    }



    public void doubleClick(float x, float y){
        if (!single.marked) {
            setSelected(vid, x, y);
        }else{
            Toast.makeText(mContext, "Can't select a deleted item", Toast.LENGTH_SHORT).show();
        }
    }

    public void longPress(){
        if (!touchMoved && !scrolling){
            ListView lv = (ListView) touchView.getParent().getParent();
            if (Frag_MainList.sortMethod.equals(Frag_MainList.USER)) {
                View.DragShadowBuilder myShadow = new ShadowBuilder(touchView);
                String[] clipDescription = {ClipDescription.MIMETYPE_TEXT_PLAIN};
                ClipData.Item item = new ClipData.Item(lv.getPositionForView(touchView) + "");
                ClipData dragData = new ClipData(touchView.getTag().toString(), clipDescription, item);
                Frag_MainList.dragStart = lv.getPositionForView(touchView);
                touchView.startDrag(dragData, myShadow, touchView, 0);
            } else {
                toolTip("What does this do?", "-Tap to view sub-items.\n-Double tap to select.\n-Swipe left to delete\n-Swipe right to complete\n\n[In manual sorting mode only]\n-Hold & drag to re-arrange the items manually.");
            }
            database.open();

            final View select = touchView.findViewWithTag(vid + SELECT_TAG);
            if (toSlide.getSelection().contains(single.id)) {
                select.setBackgroundColor(ElementColor(single.color));
            }else{
                select.setBackgroundColor(Color.TRANSPARENT);
            }
        }
    }

    /////////////////ALL TOUCH EVENTS ////////////////////////////////////////
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        View lv = (View) view.getParent().getParent();
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            sX = motionEvent.getX();
            sY = motionEvent.getY();

            Boolean sameViewTwice = false;
            if (((Integer) view.getTag()) == vid){
                sameViewTwice = true;
            }

            vid = (Integer) view.getTag();
            touchView = view;

            database.open();
            single = database.getSingle(vid);
            database.close();

            touchMoved=false;
            scrolling = false;

            //circular reveal
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && single.marked) {
                if (downTimer <= 0) {
                    //First Click
                    final View select = touchView.findViewWithTag(vid + SELECT_TAG);
                    final View select2 = touchView.findViewWithTag(vid + SELECT_TAG2);

                    select.setBackgroundColor(ElementColor(single.color));

                    Animator tapAnim;
                    if (toSlide.getSelection().contains(single.id)) {
                        tapAnim = ViewAnimationUtils.createCircularReveal(select2,
                                (int) sX, (int) sY, select2.getWidth(), 0);
                    } else {
                        tapAnim = ViewAnimationUtils.createCircularReveal(select,
                                (int) sX, (int) sY, 0, select.getWidth());
                    }

                    tapAnim.setDuration(clickDelay);
                    tapAnim.start();
                } else if (downTimer > 0 && sameViewTwice) {
                    //Second Click
                    final View select = touchView.findViewById(com.likwid.anilist.R.id.rlMainSelect);
                    final View select2 = touchView.findViewById(com.likwid.anilist.R.id.rlMainSelect2);

                    select.setBackgroundColor(ElementColor(single.color));

                    Animator tapAnim;
                    if (toSlide.getSelection().contains(single.id)) {
                        tapAnim = ViewAnimationUtils.createCircularReveal(select,
                                (int) sX, (int) sY, select.getWidth(), 0);
                    } else {
                        tapAnim = ViewAnimationUtils.createCircularReveal(select2,
                                (int) sX, (int) sY, 0, select2.getWidth());
                    }

                    tapAnim.setDuration(clickDelay);
                    tapAnim.start();
                }
            }

            if (downTimer > 0 && sameViewTwice){
                //Second Tap
                doubleClicked = true;
                handler.removeCallbacks(TapDetectTask);
                handler.postDelayed(TapDetectTask, 0);
            }else
            if (downTimer == 0){
                //First Tap
                touchUped = false;
                doubleClicked = false;
                downTimer = SystemClock.elapsedRealtime();
                handler.postDelayed(TapDetectTask, clickDelay);
            }

            return true;
        }

        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
            if (sX > dm.widthPixels - 100) {
                rightSideTouch = true;
                return true;
            } else {
                if (Math.abs(motionEvent.getRawX() - sX) > 50) {
                    float distance = motionEvent.getRawX() - sX;
                    float alpha = Math.abs(distance/255f);
                    touchView.setTranslationX(motionEvent.getRawX() - sX);
                    touchMoved = true;
                    ChangeInX = true;
                    handler.removeCallbacks(TapDetectTask);
                    downTimer = 0;
                }else{
                    touchView.setTranslationX(oX);
                }
                return true;
            }
        }

        if (motionEvent.getAction() == MotionEvent.ACTION_UP
                || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {

            //Scrolling
            if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL){
                scrolling = true;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    final View select = touchView.findViewWithTag(vid + SELECT_TAG);
                    select.setBackgroundColor(Color.TRANSPARENT);
                }

            }

            if (rightSideTouch){
                //Clear background
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    final View select = touchView.findViewWithTag(vid + SELECT_TAG);
                    select.setBackgroundColor(Color.TRANSPARENT);
                }

                touchMoved = false;
                rightSideTouch=false;
                float flingV = (sX - motionEvent.getX())/(motionEvent.getEventTime() - motionEvent.getDownTime());
                if (flingV > 1){
                    toSlide.spClosePanel(true);
                    slideOpened = true;
                }
            }

            touchUped = true;
            dX = motionEvent.getRawX() - sX;

            ///SWIPE GESTURES/////
            database.open();

            if (dX > 50 && touchMoved) { //Swipe Right - COMPLETE /////////////
                //Clear background
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    final View select = touchView.findViewWithTag(vid + SELECT_TAG);
                    if (toSlide.getSelection().contains(single.id)){
                        select.setBackgroundColor(ElementColor(single.color));
                    }else{
                        select.setBackgroundColor(Color.TRANSPARENT);
                    }
                }

                if (vid == selected) {
                    //Deselect
                    setSelected(selected, sX, sY);
                }

                if (database.isMarked(vid)){
                    touchView.setTranslationX(oX);
                    return true;
                }

                database.switchComplete(vid);
                Animation complete = AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.list_complete);
                complete.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        database.open();
                        single = database.getSingle(vid);
                        database.close();

                        Animation showTView = AnimationUtils.loadAnimation(mContext, R.anim.list_complete_2);
                        touchView.startAnimation(showTView);
                        touchView.setTranslationX(oX);
                        toSlide.refreshList();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                touchView.startAnimation(complete);
            } else if (dX < -50 && touchMoved) { //Swipe Left - MARK TO DELETE //////////////
                //Clear background
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    final View select = touchView.findViewWithTag(vid + SELECT_TAG);
                    if (toSlide.getSelection().contains(single.id)){
                        select.setBackgroundColor(ElementColor(single.color));
                    }else{
                        select.setBackgroundColor(Color.TRANSPARENT);
                    }
                }

                //Use the id of the view (vid) to toggle the deletion of the item
                Animation deleteItem = AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.list_delete);
                deleteItem.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        //change the deletion and update the single element
                        database.open();
                        database.ToggleDeletion(vid);
                        single = database.getSingle(vid);
                        database.close();

                        toSlide.clearSelection();
                        Animation showTView = AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.list_delete2);
                        touchView.startAnimation(showTView);
                        touchView.setTranslationX(oX);
                        toSlide.refreshList();
                    }
                    @Override public void onAnimationRepeat(Animation animation) {}
                });

                //Play the animation on the touched view
                touchView.startAnimation(deleteItem);
            }else{
                touchView.setTranslationX(oX);
            }
            database.close();
            return true;
        }
        return false;
    }
}