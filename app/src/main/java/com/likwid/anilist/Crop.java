package com.likwid.anilist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;


public class Crop extends Activity{
    private final int GET_PIC = 100000;
    private ImageTouchView ivMainPreview;
    private LinearLayout llActionBar;

    private Button bSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(com.likwid.anilist.R.layout.activity_crop);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int alpha = prefs.getInt(getString(com.likwid.anilist.R.string.pref_menu_alpha), 255);
        int color = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
        llActionBar = (LinearLayout) findViewById(com.likwid.anilist.R.id.llActionBar);
        llActionBar.setBackgroundColor(getResources().getColor(color));
        llActionBar.getBackground().setAlpha(alpha);

        ivMainPreview = (ImageTouchView) findViewById(com.likwid.anilist.R.id.ivCropPreview);
        ivMainPreview.setDrawingCacheEnabled(true);

        bSave = (Button) findViewById(com.likwid.anilist.R.id.bCrop);
        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileOutputStream fileOutputStream = openFileOutput("bg.jpg", Context.MODE_PRIVATE);
                    Bitmap.createScaledBitmap(ivMainPreview.getDrawingCache(true), 1080,
                            1920, false).compress(Bitmap.CompressFormat.JPEG, 100,
                            fileOutputStream);
                    fileOutputStream.close();
                    Toast.makeText(getApplicationContext(), "Background Set", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finish();
            }
        });


        Intent picIntent = new Intent(Intent.ACTION_PICK);
        picIntent.setType("image/*");
        startActivityForResult(picIntent, GET_PIC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case GET_PIC:
                if (resultCode == RESULT_OK) {
                    Uri picUri = data.getData();
                    ivMainPreview.setImageURI(picUri);
                }else{
                    finish();
                }
                break;
            default:
                finish();
                break;
        }
    }
}
