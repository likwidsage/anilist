package com.likwid.anilist;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.ExifInterface;
import android.net.Uri;
import android.opengl.GLES10;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


public class TouchView extends View {
    private final int NONE = 0;
    private final int PAN = 1;
    private final int ZOOM = 2;
    private int mode = 0;

    Bitmap bitmap;
    private float oldDistance = 1f, newDistance = 1f, minScale = 1f;
    PointF start = new PointF();
    Context context;
    float scale = 1f, setX = 0, setY = 0, rotate = 0;
    int height, width;
    Uri picUri;
    Boolean readExif = true;
    Matrix matrix = new Matrix();

    public TouchView(Context context) {
        super(context, null, 0);
        this.context = context;
    }
    public TouchView(Context c, AttributeSet as){ super(c, as, 0); }
    public TouchView(Context c, AttributeSet as, int style){ super(c, as, style); }

    public void setImageURI(Uri uri){
        picUri = uri;

        //Scale bitmap to fit in gl view
        try {
            ContentResolver cr = getContext().getContentResolver();
            InputStream in = cr.openInputStream(picUri);
            BitmapFactory.Options justSize = new BitmapFactory.Options();

            justSize.inJustDecodeBounds = true;
            bitmap = BitmapFactory.decodeStream(in, null, justSize);

            justSize.inSampleSize = getSampleSize(justSize);

            justSize.inJustDecodeBounds = false;
            in = cr.openInputStream(picUri);
            bitmap = BitmapFactory.decodeStream(in, null, justSize);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (readExif) {
            //Rotate based on Exif data
            ExifInterface exin;
            try {
                exin = new ExifInterface(picUri.getPath());
                int rotation = exin.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                switch (rotation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate += 90f;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate += 180f;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate += 270f;
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        matrix.postRotate(rotate);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        minScale = (float) height / (float) bitmap.getHeight();
        scale = minScale;
        setX = -(bitmap.getWidth() / 2) + (width / 2);
        setY = 0;
    }

    public void setRotate(float angle){ rotate = angle; }
    public void readExif(Boolean readExif){ this.readExif = readExif; }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        height = h;
        width = w;

        if (bitmap != null) {
            minScale = (float) height / (float) bitmap.getHeight();
            scale = minScale;
            setX = -(bitmap.getWidth() / 2) + (width / 2);
            setY = 0;
        }
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private int getSampleSize(BitmapFactory.Options options){
        int sampleSize = 1;
        int height = options.outHeight;
        int width = options.outWidth;
        //if height or width is large than the texture size limit
        while(height > GLES10.GL_MAX_TEXTURE_SIZE || width > GLES10.GL_MAX_TEXTURE_SIZE){
            height /= 2;
            width /= 2;
            sampleSize *= 2;
        }
        return sampleSize;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (picUri != null) {
//            //limit left
//            if (setX > 0) setX = 0;
//            //limit top
//            if (setY > 0) setY = 0;
//            //limit bottom
//            if (setY < -(bitmap.getHeight() * scale - (float) height) / scale) {
//                setY = -(bitmap.getHeight() * scale - (float) height) / scale;
//            }
//            //limit right
//            if (setX < -(bitmap.getWidth() * scale - (float) width) / scale) {
//                setX = -(bitmap.getWidth() * scale - (float) width) / scale;
//            }
            //limit scale
            if (scale > minScale * 4) scale = minScale * 4;

            canvas.save();
            canvas.scale(scale, scale);
            canvas.translate(setX, setY);
            canvas.drawBitmap(bitmap, 0, 0, null);

            canvas.restore();
        }
    }

    //Distance between 2 points in MotionEvent
    private float Distance(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    //Mid point betweeen 2 points in MotionEvent
    private float Angle(MotionEvent event){
        float angle = 0;
        PointF midPoint = new PointF((event.getX(0)+event.getX(1))/2, (event.getY(0)+event.getY(1))/2);

        return angle;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()){
            case MotionEvent.ACTION_DOWN:
                start.set(event.getX(), event.getY());
                mode = PAN;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDistance = Distance(event);
                if (oldDistance > 10f){
                    mode = ZOOM;
                }
            case MotionEvent.ACTION_MOVE:
                if (mode == PAN){
                    setX += (event.getX() - start.x)/scale;
                    setY += (event.getY() - start.y)/scale;
                    start.set(event.getX(), event.getY());
                    invalidate();
                    return true;
                }
                if (mode == ZOOM) {
                    newDistance = Distance(event);
                    if (Math.abs(newDistance - oldDistance) > 5f) {
                        scale *= (newDistance / oldDistance);
                        oldDistance = newDistance;
                        if (scale < minScale) scale = minScale;
                        invalidate();
                    }
                    return true;
                }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;
        }
        return true;
    }
}
