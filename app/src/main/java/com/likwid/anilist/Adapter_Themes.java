package com.likwid.anilist;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by likwid on 10/18/14.
 */
public class Adapter_Themes extends BaseAdapter {
    private final BitmapFactory.Options o1 = new BitmapFactory.Options();;
    private View row;
    private Context mContext;
    private TextView tvName;
    private ImageView ivAdd, ivManage, ivMove, ivAddNote, ivManageImport;
    private ArrayList<String> themePackageNames;

    public Adapter_Themes(Context c, ArrayList<String> list){
        mContext = c;
        themePackageNames = list;
        o1.inScaled = false;
    }

    @Override
    public int getCount() {
        return themePackageNames.size();
    }

    @Override
    public Object getItem(int i) {
        return themePackageNames.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        row = view;
        if (row == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(com.likwid.anilist.R.layout.adapter_theme_item, viewGroup, false);
        }

        String theme = themePackageNames.get(i);

        tvName = (TextView) row.findViewById(com.likwid.anilist.R.id.tvName);
        ivAdd = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainAdd);
        ivManage = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainManage);
        ivMove = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainMove);
        ivAddNote = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivAddNote);
        ivManageImport = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivManageImport);


        try {
            Resources res = mContext.getPackageManager().getResourcesForApplication(theme);

            int nameRes = res.getIdentifier("app_name", "string", theme);
            CharSequence themeName = res.getText(nameRes);
            tvName.setText(themeName);

            int resImg = res.getIdentifier("main_add", "drawable", theme);
            if (resImg != 0) ivAdd.setImageBitmap(BitmapFactory.decodeResource(res, resImg));

            resImg = res.getIdentifier("main_manage", "drawable", theme);
            if (resImg != 0) ivManage.setImageBitmap(BitmapFactory.decodeResource(res, resImg));

            resImg = res.getIdentifier("manage_move", "drawable", theme);
            if (resImg != 0) ivMove.setImageBitmap(BitmapFactory.decodeResource(res, resImg));

            resImg = res.getIdentifier("add_note", "drawable", theme);
            if (resImg != 0) ivAddNote.setImageBitmap(BitmapFactory.decodeResource(res, resImg));

            resImg = res.getIdentifier("manage_import", "drawable", theme);
            if (resImg != 0) ivManageImport.setImageBitmap(BitmapFactory.decodeResource(res, resImg));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return row;
    }

    private Boolean checkIconSize(Bitmap icon){
        return true;
    }
}
