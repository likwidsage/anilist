package com.likwid.anilist;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;

/**
 * Created by likwid on 9/7/14.
 */
public class WidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListRemoteViewFactory(this.getApplicationContext(), intent);
    }
}

class ListRemoteViewFactory implements RemoteViewsService.RemoteViewsFactory{

    private Context context;
    private int awID;
    private ArrayList<Element> list;
    Adapter_SQL database;
    SharedPreferences prefs;

    public ListRemoteViewFactory(Context c, Intent intent){
        context = c;
        awID = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @Override
    public void onCreate() {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int id = prefs.getInt(awID+WidgetProvider.ID_FOR_WIDGET, 0);

        database = new Adapter_SQL(context);
        database.open();
        list = database.getChildren(id);
        database.close();
    }

    @Override
    public void onDataSetChanged() {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int id = prefs.getInt(awID+WidgetProvider.ID_FOR_WIDGET, 0);

        RemoteViews v = new RemoteViews(context.getPackageName(), com.likwid.anilist.R.layout.widget_layout);
        database = new Adapter_SQL(context);
        database.open();
        list = database.getChildren(id);
        database.close();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        RemoteViews v = new RemoteViews(context.getPackageName(), com.likwid.anilist.R.layout.widget_item);
        v.setTextViewText(com.likwid.anilist.R.id.tvItemText, list.get(i).title);
        if (!list.get(i).completed) {
            v.setTextColor(com.likwid.anilist.R.id.tvItemText, Color.parseColor("#FFFFFF"));
            int slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cBlue);

            if (list.get(i).color.equals("blue")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cBlue);
            } else if (list.get(i).color.equals("red")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cRed);
            } else if (list.get(i).color.equals("purple")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cPurple);
            } else if (list.get(i).color.equals("pink")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cPink);
            } else if (list.get(i).color.equals("orange")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cOrange);
            } else if (list.get(i).color.equals("green")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cGreen);
            } else if (list.get(i).color.equals("lime")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cLime);
            } else if (list.get(i).color.equals("yellow")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cYellow);
            } else if (list.get(i).color.equals("sky")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cSky);
            } else if (list.get(i).color.equals("slate")) {
                slctColor = context.getResources().getColor(com.likwid.anilist.R.color.cSlate);
            }

             v.setInt(com.likwid.anilist.R.id.ivIconBG, "setBackgroundColor", slctColor);
        }else{
            v.setTextColor(com.likwid.anilist.R.id.tvItemText, Color.parseColor("#888888"));
            v.setInt(com.likwid.anilist.R.id.ivIconBG, "setBackgroundColor", Color.parseColor("#00000000"));
        }
        v.setTextViewText(com.likwid.anilist.R.id.tvRow_IconLetter, list.get(i).title.substring(0,1).toUpperCase());
        //set selected color and icon color



        Bundle extras = new Bundle();
        extras.putInt(WidgetProvider.ITEM_ID, list.get(i).id);
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);
        v.setOnClickFillInIntent(com.likwid.anilist.R.id.rlWMainRowBG, fillInIntent);

        return v;
    }

    @Override
    public void onDestroy() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(awID+WidgetProvider.ID_FOR_WIDGET);
        editor.commit();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).id;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}