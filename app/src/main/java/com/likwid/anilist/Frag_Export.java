package com.likwid.anilist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class Frag_Export extends Fragment implements OnClickListener {

	static final int WRITEFILE = 2;
	public TextView tvTitle, etExportList;
	Adapter_SQL database;
	MainActivity toSlide;
	Button bExport, bShare;
    String list = "";
	LinearLayout llMain;

    public Frag_Export() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(com.likwid.anilist.R.layout.frag_export, container, false);
		tvTitle = (TextView) view.findViewById(com.likwid.anilist.R.id.tvExportTitle);
		etExportList = (TextView) view.findViewById(com.likwid.anilist.R.id.etExportList);
		bExport = (Button) view.findViewById(com.likwid.anilist.R.id.bExportFile);
		bShare = (Button) view.findViewById(com.likwid.anilist.R.id.bExportShare);
		llMain = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.llExportMain);
		
		bExport.setOnClickListener(this);
		bShare.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		database = new Adapter_SQL(getActivity());
		toSlide = (MainActivity) getActivity();
        ArrayList<Integer> selection = toSlide.getSelection();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int colorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
		Boolean ColorBG = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);

		tvTitle.setBackgroundColor(getResources().getColor(colorPrefInt));

		if (ColorBG){
			llMain.setBackground(new ColorDrawable(getResources().getColor(colorPrefInt)));
		}

		int pad = 10;
		etExportList.setPadding(pad,pad,pad,pad);

        ExportList el = new ExportList(getActivity());
		etExportList.setText(el.ToText(selection));

        bExport.setBackgroundColor(getResources().getColor(colorPrefInt));
        bShare.setBackgroundColor(getResources().getColor(colorPrefInt));
        tvTitle.setText("Exporting " + selection.size() + (selection.size() == 1 ? "item.":"items."));
	}

    //Returns the path to the file it writes
	public String writeFile(String nameOfFile, Context mContext){
		if (!MainActivity.EXPORT_FOLDER.exists()){
			MainActivity.EXPORT_FOLDER.mkdirs();
		}

		try {
			File exportFile;
			exportFile = new File(MainActivity.EXPORT_FOLDER, nameOfFile);
			OutputStream out = new FileOutputStream(exportFile);

			String toFile = etExportList.getText().toString();
			out.write(toFile.getBytes());
			out.flush();
			out.close();

			String toastString = exportFile.toString();
			Toast.makeText(mContext, "File saved as "+ toastString, Toast.LENGTH_SHORT).show();
            return exportFile.getPath();
        } catch (IOException e) {
			e.printStackTrace();
            return null;
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == WRITEFILE && resultCode == Activity.RESULT_OK) {
            //Get Uri
			Uri uri = intent.getData();

            //Write the file to the path
            ArrayList<Integer> selection = toSlide.getSelection();
            try {
                OutputStream os = getActivity().getContentResolver().openOutputStream(uri);
                ExportList el = new ExportList(getActivity());
                if (el.CreateAni(selection, os)) {
                    Toast.makeText(getActivity(), "File saved.", Toast.LENGTH_SHORT).show();
                    toSlide.refreshList();
                }else{
                    Toast.makeText(getActivity(), "Error.", Toast.LENGTH_SHORT).show();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
		}
	}

	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()){
		case (com.likwid.anilist.R.id.bExportFile):
			if (Build.VERSION.SDK_INT >= 19) {
                //Show the new file explorer
				tvTitle.setVisibility(View.GONE);
				Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);

                Calendar cal = Calendar.getInstance();
                Date now = new Date();
                SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmSS");
                String time = dateformat.format(now);

                database.open();
                ArrayList<Integer> selection = toSlide.getSelection();
                String title = database.getName(selection.get(0)) + "_" + time + MainActivity.EXTENSION;
                database.close();
                intent.putExtra(Intent.EXTRA_TITLE, title);

				intent.setType("*/*");
				startActivityForResult(intent, WRITEFILE);
			}else{
                //Show the dialogue to save the list
				LayoutInflater li = LayoutInflater.from(getActivity());
				View dialogView = li.inflate(com.likwid.anilist.R.layout.dial_namefile, null);
				final EditText etName = (EditText) dialogView.findViewById(com.likwid.anilist.R.id.etFilename);
				final Context mContext = getActivity();
				
				AlertDialog.Builder db = new AlertDialog.Builder(getActivity());
				db.setTitle("Save as...");
				db.setView(dialogView);
				db.setPositiveButton("Save", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
							writeFile(etName.getText().toString().trim(), mContext);
					}
				}); 
				
				db.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});
				
				AlertDialog prompt = db.create();
				prompt.show();
			}
			break;
		case(com.likwid.anilist.R.id.bExportShare):
			Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
			shareIntent.setType("text/plain");
		    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, etExportList.getText().toString());
			startActivity(shareIntent);
			toSlide.refreshList();
		}
		
	}
}