package com.likwid.anilist;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;


public class WidgetProvider extends AppWidgetProvider {
    public static final String ITEM_ID = "WidgetItemID";
    public static final String ID_FOR_WIDGET = "IdForWidget";
    public static final String ADD_TO_LIST = "AddToList";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        final int N = appWidgetIds.length;
        for (int appWidgetId : appWidgetIds) {

            Intent intent = new Intent(context, WidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

            views.setRemoteAdapter(R.id.wList, intent);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            int id = prefs.getInt(appWidgetId + ID_FOR_WIDGET, 0);

            Adapter_SQL database = new Adapter_SQL(context);
            database.open();
            if (id > 0) {
                String name = database.getName(id);
                if (name != null) views.setTextViewText(R.id.textView, database.getName(id));
            } else {
                views.setTextViewText(R.id.textView, "Main List");
            }
            database.close();

            //Intent to open the selected item
            Intent openIntent = new Intent(context, MainActivity.class);
            openIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            PendingIntent openPendingIntent = PendingIntent.getActivity(context, Integer.parseInt(appWidgetId + "00"), openIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setPendingIntentTemplate(R.id.wList, openPendingIntent);

            //Intent to main_add task
            Intent addIntent = new Intent(context, Widget_Add.class);
            addIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            addIntent.putExtra(ADD_TO_LIST, false);
            PendingIntent addPendingIntent = PendingIntent.getActivity(context, Integer.parseInt(appWidgetId + "01"), addIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setOnClickPendingIntent(R.id.ivWidgetAdd, addPendingIntent);

            //Intent for "main_add to list"
            Intent addListIntent = new Intent(context, Widget_Add.class);
            addListIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            addListIntent.putExtra(ADD_TO_LIST, true);
            PendingIntent addListPendingIntent = PendingIntent.getActivity(context, Integer.parseInt(appWidgetId + "02"), addListIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setOnClickPendingIntent(R.id.ivWidgetAddList, addListPendingIntent);

            views.setEmptyView(R.id.wList, R.id.tvEmptyView);

            //Update widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}


