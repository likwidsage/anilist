package com.likwid.anilist;

import android.content.Context;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by likwid on 2/8/15.
 */
public class ExportList {
    static final String starter = MainActivity.STARTER;
    static final String starterDone = MainActivity.STARTER_DONE;
    static final String seperator = MainActivity.SEPERATOR;
    static final String header = MainActivity.HEADER;
    static final String footer = MainActivity.FOOTER;

    Context mContext;
    Adapter_SQL database;
    private ArrayList<Element> export = new ArrayList<>();
    private String list = "";

    public ExportList (Context c){
        mContext = c;
        database = new Adapter_SQL(mContext);
    }

    //Returns the list as string to import/export
    public String ToText(ArrayList<Integer> selection){
        if (selection.size() <= 0){
            return list;
        }

        database.open();
        int minLevel = database.getLevel(selection.get(0));
        for (Integer selected : selection) {
            Element single = database.getSingle(selected);
            String path = single.folder;
            ArrayList<Element> allChildren = database.getAllChildren(selected, path);
            export.add(single);
            Sort_Elements sort = new Sort_Elements();
            if (allChildren.size() > 1) {
                allChildren = sort.sortFolder(allChildren);
            }
            export.addAll(allChildren);
        }

        String prefix;
        String levelDis="";
        int level;
        for (Element anExport : export) {
            level = database.getLevel(anExport.id) - minLevel;
            String type = database.getType(anExport.id);

            for (int j = 0; j <= level; j++) { // adds indents for sub items
                levelDis = j + "";
            }
            String starter = (!anExport.completed ? ExportList.starter : starterDone);
            if (type.equals("note")) {
                prefix = "N";
                list = list + starter + levelDis + seperator + prefix + seperator + anExport.title + "\n";
            }
            if (type.equals("element")) {
                prefix = "E";
                list = list + starter + levelDis + seperator + prefix + seperator + anExport.title + seperator + anExport.color + "\n";
            }
            levelDis = "";
        }
        database.close();

        String returnString = header + list + footer;
        Log.d("ToText", returnString);
        return returnString;
    }

    public Boolean CreateAni(ArrayList<Integer> selection, OutputStream fileOut){
        String list;
        list = ToText(selection);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(bos);
        try {
            //Name the file in the zip "list"
            ZipEntry entry = new ZipEntry("list");
            zos.putNextEntry(entry);
            zos.write(list.getBytes());
            zos.closeEntry();
            zos.close();

            fileOut.write(bos.toByteArray());

            bos.flush();
            bos.close();
            fileOut.flush();
            fileOut.close();
            return true;

        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
}
