package com.likwid.anilist;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.vending.licensing.LicenseChecker;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class MainActivity extends Activity implements Frag_MainList.menuItems,
        DragListener.listManage, Frag_Calendar_View.FragmentCalendar,Adapter_Calendar.CalAdaptFinished,
        IabHelper.OnIabPurchaseFinishedListener, IabHelper.QueryInventoryFinishedListener,
        IabHelper.OnIabSetupFinishedListener, View.OnClickListener, NavigationDrawerFragment.NavigationDrawerCallbacks {
    private static final String FRAG_ADDMENU = "frag_addmenu";
    ArrayList<String> blipsShown = new ArrayList<String>();
    ArrayList<String> skuList = new ArrayList<String> ();

    IabHelper mIabHelper;

    private Boolean hasSubs = false;
    public boolean hasSubs(){
        return hasSubs;
    }

    private Boolean hasThemes = false;
    public  boolean hasThemes(){
        return hasThemes;
    }

    Boolean showAd = true;

    static final String ALARM_ARGS = "Alarm_Selection";

    final static String REMOVEADS = "remove_ads";
    final static String UNLSUBS = "unlimited_sub";
    final static String LISTTHEMES = "list_themes";
    final int FREESUBLIMIT = 2;
    public int getFreeSubLimit(){
        return FREESUBLIMIT;
    }

    static final File EXPORT_FOLDER = new File(Environment.getExternalStorageDirectory().toString() + "/AniList/exported/");
    static final File PHOTOS_FOLDER = new File(Environment.getExternalStorageDirectory().toString() + "/AniList/photos/");
    static final File TMP_DIR = new File(Environment.getExternalStorageDirectory().toString() + "/AniList/tmp/");
    static final String HEADER = "<AniList>\n";
    static final String STARTER = ":-:";
    static final String STARTER_DONE = ":x:";
    static final String SEPERATOR = ":/:";
    static final String FOOTER = "</AniList>";
    static final String EXTENSION = ".alst";

    private boolean panelUsed = false;

    Calendar mCal = Calendar.getInstance();

    ArrayList<Integer> selections = new ArrayList<Integer>();
    DrawerLayout drawer;
    Adapter_SQL database;
    private DrawerLayout mDL;
    int fragInEnter, fragInExit, popBackEnter, popBackExit, prefColorInt;
    SharedPreferences prefs;
    private int viewId=0;
    private boolean movingMode = false;
    RelativeLayout llAd;
    private AdView ad;
    private AdRequest adRequest;
    private final byte[] SALT = new byte[]{
            34, -41, -111, 26, -62, -25, 99, 63, 24, 41, 87, 81, 19, -37, 33, -45, 83, -62, 56, 48
    };
    private LicenseChecker mChecker;

    DisplayMetrics dm = new DisplayMetrics();
    Boolean rightSideTouched = false;
    PointF startingTouch = new PointF();
    private boolean calDataChanged = false;

    private static final String SELECTIONS = "selection";
    private static final String VIEWID = "viewid";


    public void SetMovingMode(Boolean On){
        movingMode = On;
    }

    public Boolean getMovingMode(){
        return movingMode;
    }

    public int getViewId() {
        return viewId;
    }

    public void setViewId(int id){
        viewId = id;
    }

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mDL = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, mDL);

        database = new Adapter_SQL(this);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment mainList;
        FragmentManager manager = getFragmentManager();
        Log.d("Drawer Selected", position+"");
        if (position == 0) {
            mainList = Frag_MainList.newInstance(0);
            manager.beginTransaction().replace(R.id.container, mainList, Frag_MainList.FRAG_NAME).commit();
        }else{
            mainList = Frag_Calendar_Main.newInstance(0);
            manager.beginTransaction().replace(R.id.container, mainList, Frag_Calendar_Main.FRAG_NAME).commit();
        }
    }


    public void clearSelection() {
        Frag_MainList Mainfrag = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
        for (Integer selected: selections){
            if (Mainfrag.lv.findViewWithTag(selected+Adapter_List.SELECT_TAG) != null)
                Mainfrag.lv.findViewWithTag(selected+Adapter_List.SELECT_TAG)
                        .setBackgroundColor(Color.TRANSPARENT);
        }
        selections.clear();
        Mainfrag.setSelectionEffects();
    }

    @Override
    public void addNote() {
        if (selections.get(0) > 0 && selections.size() == 1) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            database.open();
            alert.setTitle("Add note to "+database.getName(selections.get(0)));
            database.close();

            // Set an EditText view to get user input
            final EditText input = new EditText(this);
            alert.setView(input);

            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String value = input.getText().toString();
                    // Do something with value!
                    if (value.contains(MainActivity.HEADER)
                            || value.contains(MainActivity.STARTER)
                            || value.contains(MainActivity.SEPERATOR)
                            || value.contains(MainActivity.FOOTER)) {
                        Toast.makeText(getApplication(), "Note can not contain '"
                                + MainActivity.HEADER + "', '"
                                + MainActivity.STARTER + "', '"
                                + MainActivity.SEPERATOR + "' or '"
                                + MainActivity.FOOTER + "'", Toast.LENGTH_SHORT).show();

                        addNote();
                    } else {
                        if (value.trim().length() < 1) {
                            Toast.makeText(getApplication(), "Note was blank. Cancelled.", Toast.LENGTH_SHORT).show();
                        } else {
                            database.open();
                            database.addNote(value, selections.get(0));
                            database.close();
                            Frag_MainList main = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
                            main.refresh();
                        }
                    }
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });

            alert.show();
        }else {
            Toast toast = Toast.makeText(getApplication(), "Select a single item to add a note to.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void addCamera() {
        if (selections.size() == 1) {
            if (selections.size() == 1) {
                if (selections.get(0) > 0) {
                    Intent intent = new Intent("com.likwid.orgalist.ACTIVITY_ADDCAMERA");
                    intent.putExtra("selected", selections.get(0));
                    startActivity(intent);
                }
            }
        } else {
            Toast toast = Toast.makeText(getApplication(), "Select a single item to add a picture to.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void addGallery() {
        if (selections.size() == 1) {
            if (selections.get(0) > 0) {
                Intent intent = new Intent("com.likwid.orgalist.ACTIVITY_ADDGALLERY");
                intent.putExtra("selected", selections.get(0));
                startActivity(intent);
            }
        } else {
            Toast toast = Toast.makeText(getApplication(), "Select a single item to add a picture to.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    // Adds alarm. Selection isn't needed because when nothing is selected, it triggers
    // the function to create an item to with the alarm.
    @Override
    public void addAlarm(int id) {
        if (selections.size() <= 1){
            panelUsed = true;
            spClosePanel(true);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(fragInEnter, fragInExit, popBackEnter, popBackExit);

            //Create new fragment with selection as arguement
            Bundle args = new Bundle();
            args.putInt(ALARM_ARGS, id);
            Fragment alarmFrag = new Frag_Alarm();
            alarmFrag.setArguments(args);

            ft.replace(R.id.container, alarmFrag, FRAG_ADDMENU);
            ft.commit();
        } else {
            Toast toast = Toast.makeText(getApplication(), "Select a single item to add an alarm to.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void exportItem() {
        if (selections.size() > 0) {
            panelUsed = true;
            spClosePanel(true);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(fragInEnter, fragInExit, popBackEnter, popBackExit);
            ft.replace(com.likwid.anilist.R.id.container, new Frag_Export(), FRAG_ADDMENU);
            ft.commit();
        } else {
            Toast toast = Toast.makeText(getApplication(), "Select an item to export.", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void importItem() {
        panelUsed = true;
        spClosePanel(true);
        FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(fragInEnter, fragInExit, popBackEnter, popBackExit);
        ft.replace(com.likwid.anilist.R.id.container, new Frag_Import(), FRAG_ADDMENU);
        ft.commit();
    }

    public int colorToResource(String color){
        int oldSetColor = 0;
        if (color.equals("blue")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cBlue);
        } else if (color.equals("red")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cRed);
        } else if (color.equals("purple")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cPurple);
        } else if (color.equals("pink")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cPink);
        } else if (color.equals("orange")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cOrange);
        } else if (color.equals("green")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cGreen);
        } else if (color.equals("lime")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cLime);
        } else if (color.equals("yellow")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cYellow);
        } else if (color.equals("sky")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cSky);
        } else if (color.equals("slate")) {
            oldSetColor = getResources().getColor(com.likwid.anilist.R.color.cSlate);
        }
        return oldSetColor;
    }


    @Override
    public void renameItem() {
        if (selections.get(0) > 0 && selections.size() == 1) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            database.open();
            alert.setTitle("Renaming "+database.getName(selections.get(0)));


            // Set an EditText view to get user input
            final EditText input = new EditText(this);
            alert.setView(input);
            input.setText(database.getName(selections.get(0)));
            database.close();

            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String value = input.getText().toString();
                    // Do something with value!
                    if (value.contains(MainActivity.HEADER)
                            || value.contains(MainActivity.STARTER)
                            || value.contains(MainActivity.SEPERATOR)
                            || value.contains(MainActivity.FOOTER))
                    {
                        Toast.makeText(getApplication(), "Note can not contain '"
                                + MainActivity.HEADER + "', '"
                                + MainActivity.STARTER + "', '"
                                + MainActivity.SEPERATOR + "' or '"
                                + MainActivity.FOOTER + "'", Toast.LENGTH_SHORT).show();

                        addNote();
                    }else{
                        if (value.trim().length() < 1) {
                            Toast.makeText(getApplication(), "Note was blank. Cancelled.", Toast.LENGTH_SHORT).show();
                        }else{
                            database.open();
                            database.renameElement(value, selections.get(0));
                            database.close();
                            Frag_MainList main = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
                            main.refresh();
                        }
                    }

                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });

            alert.show();
        }else{
            Toast.makeText(getApplication(), "Select an item to rename.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void viewPhotos(int id) {
        if (id > 0) {
            panelUsed = true;
            spClosePanel(true);

            Bundle args = new Bundle();
            args.putInt(Frag_ElementPhotos.PHOTO_ID, id);
            Fragment fragPhoto = Frag_ElementPhotos.newInstance(0);
            fragPhoto.setArguments(args);

            FragmentManager manager = getFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(fragInEnter, fragInExit, popBackEnter, popBackExit);
            ft.replace(com.likwid.anilist.R.id.container, fragPhoto, FRAG_ADDMENU);
            ft.commit();
        } else {
            Toast toast = Toast.makeText(getApplication(), "Select an item to view photo(s) of.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.setGravity(Gravity.TOP, 0, 200);
            toast.show();
        }
    }

    @Override
    public void viewAll() {
        panelUsed = true;
        FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        Fragment list_all = Frag_List_All.newInstance(0);
        ft.setCustomAnimations(fragInEnter, fragInExit, popBackEnter, popBackExit);
        ft.replace(com.likwid.anilist.R.id.container, list_all, "listall");
        ft.commit();
    }

    @Override
    public void viewNotes(int id) {
        if (id > 0){
            panelUsed = true;
            spClosePanel(true);

            Bundle args = new Bundle();
            args.putInt(Frag_ElementNotes.NOTE_ID, id);
            Frag_ElementNotes fragNotes = new Frag_ElementNotes();
            fragNotes.setArguments(args);

            FragmentManager manager = getFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(fragInEnter, fragInExit, popBackEnter, popBackExit);
            ft.replace(com.likwid.anilist.R.id.container, fragNotes, FRAG_ADDMENU);
            ft.commit();
        }else{
            Toast toast = Toast.makeText(getApplication(), "Select an item to see notes of.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.setGravity(Gravity.TOP, 0, 200);
            toast.show();
        }
    }

    public void refreshList(){ //Called by menu fragments to refresh mainlist
        setIntent(new Intent());
        Frag_MainList main = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
        main.refresh();
        spClosePanel(true);
    }

    public void goInto(int id) {
        Frag_MainList main = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
        if (main != null) {
            main.goIntoItem(id);
            spClosePanel(true);
        }else{
            viewId = id;
            FragmentManager manager = getFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(fragInEnter, fragInExit, popBackEnter, popBackExit);
            ft.replace(R.id.container, new Frag_MainList(), Frag_MainList.FRAG_NAME);
            ft.commit();
        }
    }

    public void showTinyOptions(final int id){
        database.open();
        final Element single = database.getSingle(id);
        database.close();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        database.open();
        builder.setMessage("What do you want to do?")
                .setTitle(single.title);

        if (database.getPhotos(id).size() > 0) {
            builder.setNegativeButton("View Images",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            spClosePanel(false);
                            viewPhotos(id);
                        }
                    });
        }

        if (database.getChildNotes(id).size() > 0) {
            builder.setNeutralButton("View Notes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            spClosePanel(false);
                            viewNotes(id);
                        }
                    });
        }

        if (database.getName(id).startsWith("http://") || database.getName(id).startsWith("https://")) {
            builder.setPositiveButton("Visit",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(single.title));
                            startActivity(i);
                        }
                    });
        }

        database.close();
        AlertDialog tinyDial = builder.create();
        tinyDial.setCanceledOnTouchOutside(true);
        tinyDial.show();
    }

    public void addSelection(int id, float x, float y) {
        Frag_MainList Mainfrag = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
        selections.add(id);


        database.open();
        final Element single = database.getSingle(id);
        database.close();

        final View select = Mainfrag.lv.findViewWithTag(id + Adapter_List.SELECT_TAG2);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //Lower than Lollipop
            Animation selectAnim = AnimationUtils.loadAnimation(this, com.likwid.anilist.R.anim.list_select);
            selectAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    select.setBackgroundColor(colorToResource(single.color));

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            select.startAnimation(selectAnim);
        }else{
            //Lollipop and above
            if (x == 0 && y == 0){
                View icon = Mainfrag.lv.findViewWithTag(id+Adapter_List.ICONTAG);
                x = icon.getLeft() + (icon.getWidth()/2);
                y = icon.getTop() + (icon.getHeight()/2);
            }
            select.setBackgroundColor(colorToResource(single.color));
            Animator revSelect = ViewAnimationUtils.createCircularReveal(select,
                    (int) x,
                    (int) y,
                    0,
                    select.getWidth());
            revSelect.start();
        }
        Mainfrag.setSelectionEffects();
    }

    public ArrayList<Integer> getSelection() {
        return this.selections;
    }

    public void removeSelected(int i, float x, float y) {
        Frag_MainList Mainfrag = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
        selections.remove(selections.indexOf(i));

        final View select = Mainfrag.lv.findViewWithTag(i+Adapter_List.SELECT_TAG);
        final View select2 = Mainfrag.lv.findViewWithTag(i + Adapter_List.SELECT_TAG2);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Animation selectAnim = AnimationUtils.loadAnimation(this, com.likwid.anilist.R.anim.list_deselect);
            selectAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    select.setBackgroundColor(Color.TRANSPARENT);
                    select2.setBackgroundColor(Color.TRANSPARENT);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            select.startAnimation(selectAnim);
            select2.startAnimation(selectAnim);
        }else{
            //Lollipop and above
            if (x == 0 && y == 0){
                View icon = Mainfrag.lv.findViewWithTag(i+Adapter_List.ICONTAG);
                x = icon.getLeft() + (icon.getWidth()/2);
                y = icon.getTop() + (icon.getHeight()/2);
            }
            Animator revSelect = ViewAnimationUtils.createCircularReveal(select,
                    (int) x,
                    (int) y,
                    select.getWidth(),
                    0);
            Animator revSelect2 = ViewAnimationUtils.createCircularReveal(select2,
                    (int) x,
                    (int) y,
                    select.getWidth(),
                    0);
            revSelect.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    select.setBackgroundColor(Color.parseColor("#00000000"));
                    select2.setBackgroundColor(Color.parseColor("#00000000"));
                }
            });
            revSelect.start();
            revSelect2.start();
        }
        Mainfrag.setSelectionEffects();
    }

    public void spClosePanel(Boolean close) {
        if (close){
            mDL.closeDrawer(Gravity.LEFT);
        }else{
            mDL.openDrawer(Gravity.LEFT);
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public void CalAdaptDone() {
        FragmentManager manager = getFragmentManager();
        Fragment calendar = manager.findFragmentByTag(Frag_Calendar_Main.FRAG_NAME);
        FragmentManager cfm = calendar.getFragmentManager();
        Frag_Calendar_View fcv = (Frag_Calendar_View) cfm.findFragmentByTag(Frag_Calendar_View.FRAG_NAME);
        if (fcv != null){
            fcv.CalAdaptDone();
        }
    }

    @Override
    public void resetDate() {
        mCal.setTimeInMillis(Calendar.getInstance().getTimeInMillis());
    }

    @Override
    public void setMonth(int M) {
        //Check to see if selected day is larger than then the new months latest date
        Calendar ncal = Calendar.getInstance();
        ncal.set(Calendar.DAY_OF_MONTH, 1);
        ncal.set(Calendar.MONTH, M);
        ncal.set(Calendar.YEAR, mCal.get(Calendar.YEAR));
        if (mCal.get(Calendar.DAY_OF_MONTH) > ncal.getActualMaximum(Calendar.DAY_OF_MONTH)){
            //If it is, set it so the last day is selected instead
            mCal.set(Calendar.DAY_OF_MONTH, ncal.getActualMaximum(Calendar.DAY_OF_MONTH));
        }

        //Change the month of the selected day
        mCal.set(Calendar.MONTH, M);
        FragmentManager manager = getFragmentManager();
        Fragment calendar = manager.findFragmentByTag(Frag_Calendar_Main.FRAG_NAME);
        FragmentManager cfm = calendar.getChildFragmentManager();
        FragmentTransaction cft = cfm.beginTransaction();
        cft.replace(com.likwid.anilist.R.id.flCalList, new Frag_Calendar_List(), Frag_Calendar_List.FRAG_NAME);
        cft.commitAllowingStateLoss();
    }

    @Override
    public void setYear(int Y) {
        mCal.set(Calendar.YEAR, Y);
        FragmentManager manager = getFragmentManager();
        Fragment calendar = manager.findFragmentByTag(Frag_Calendar_Main.FRAG_NAME);
        FragmentManager cfm = calendar.getChildFragmentManager();
        FragmentTransaction cft = cfm.beginTransaction();
        cft.replace(com.likwid.anilist.R.id.flCalList, new Frag_Calendar_List(), Frag_Calendar_List.FRAG_NAME);
        cft.commitAllowingStateLoss();
    }

    @Override
    public void setDay(int d) {
        mCal.set(Calendar.DAY_OF_MONTH, d);
        FragmentManager manager = getFragmentManager();
        Fragment calendar = manager.findFragmentByTag(Frag_Calendar_Main.FRAG_NAME);

        FragmentManager cfm = calendar.getChildFragmentManager();
        Frag_Calendar_List fcl = (Frag_Calendar_List) cfm.findFragmentByTag(Frag_Calendar_List.FRAG_NAME);
        fcl.refreshCalList();
    }

    @Override
    public void setBlipsShown(ArrayList<String> blipsShown) {
        this.blipsShown.clear();
        for(String blip: blipsShown){
            this.blipsShown.add(blip);
        }
    }

    @Override
    public Calendar getDate() {
        return mCal;
    }

    @Override
    public void refreshCalendar() {
        FragmentManager manager = getFragmentManager();
        Fragment calendar = manager.findFragmentByTag(Frag_Calendar_Main.FRAG_NAME);
        if (calendar!=null) {
            FragmentManager cfm = calendar.getFragmentManager();
            Fragment CalListFragment = cfm.findFragmentByTag(Frag_Calendar_View.FRAG_NAME);
            Frag_Calendar_View fcv = (Frag_Calendar_View) CalListFragment;
            if (fcv != null) {
                fcv.refreshCalView();
            }
        }

        if (calendar!=null) {
            FragmentManager cfm2 = calendar.getChildFragmentManager();
            Fragment CalListFragment = cfm2.findFragmentByTag(Frag_Calendar_List.FRAG_NAME);
            Frag_Calendar_List fcl = (Frag_Calendar_List) CalListFragment;
            if (fcl != null) {
                fcl.refreshCalList();
            }
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        if (result.isFailure()) {
            return;
        }else{
            if (info.getSku().equals(MainActivity.REMOVEADS)) {
                Toast.makeText(getApplication(), "Thank you for your purchase! Removed Ads.", Toast.LENGTH_SHORT).show();
                showAd = false;
            }else if (info.getSku().equals(MainActivity.UNLSUBS)) {
                Toast.makeText(getApplication(), "Thank you for your purchase! Unlocked Sub Tasks.", Toast.LENGTH_SHORT).show();
                hasSubs = true;
            }else if (info.getSku().equals(MainActivity.LISTTHEMES)) {
                Toast.makeText(getApplication(), "Thank you for your purchase! Customize!", Toast.LENGTH_SHORT).show();
                hasThemes = true;
            }

            final ArrayList<String> skuList = new ArrayList<String>();
            skuList.add(MainActivity.REMOVEADS);
            skuList.add(MainActivity.UNLSUBS);
            skuList.add(MainActivity.LISTTHEMES);
            mIabHelper.queryInventoryAsync(true, skuList, this);
        }
    }

    @Override
    public void onIabSetupFinished(IabResult result) {
        if (result.isSuccess()) {
            final ArrayList<String> skuList = new ArrayList<String>();
            skuList.add(REMOVEADS);
            skuList.add(UNLSUBS);
            skuList.add(LISTTHEMES);
            mIabHelper.queryInventoryAsync(true, skuList, this);
        }
    }

   @Override
    public void onQueryInventoryFinished(IabResult result, Inventory inv) {
        if (result.isFailure()) {
            return;
        }

        hasThemes = inv.hasPurchase(LISTTHEMES);
        showAd = !inv.hasPurchase(REMOVEADS);
        hasSubs = inv.hasPurchase(UNLSUBS);

        if (showAd) {
            llAd.setVisibility(View.VISIBLE);
        } else {
            llAd.setVisibility(View.GONE);
        }

        if (hasThemes){
            Frag_MainList Mainfrag = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_MainList.FRAG_NAME);
            if (Mainfrag != null)
                Mainfrag.setBG();
        }

        if (inv.hasPurchase("android.test.purchased")) {
            mIabHelper.consumeAsync(inv.getPurchase("android.test.purchased"), null);
        }
    }

    @Override
    public void dropProcess() {
        Frag_MainList main = (Frag_MainList) getFragmentManager().findFragmentByTag(Frag_Calendar_List.FRAG_NAME);
        main.dropProcess();
    }

    public void showItemToBuy(String tag) {
        mIabHelper.launchPurchaseFlow(this, tag, 10001, this, new Random(20).toString());
    }
}
