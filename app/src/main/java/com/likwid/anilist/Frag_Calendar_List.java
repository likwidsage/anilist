package com.likwid.anilist;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Frag_Calendar_List extends Fragment{

    public final static String FRAG_NAME = "CalList";

    final String[] CALCOL = {CalendarContract.Events._ID,
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.RRULE,
    };

    File folder = MainActivity.EXPORT_FOLDER;
    ListView lvMainCalDue;
    Adapter_SQL database;
    MainActivity toSlide;

    int selectedDay, selectedYear, selectedMonth;
    public ArrayList<Element> alarms = new ArrayList<>();
    Frag_Calendar_View.FragmentCalendar fc;
    private RelativeLayout rlMainCalBG;
    LinearLayout llNothingDueBG;
    TextView tvNothingDue;
    RelativeLayout rlMainCalDueNone;


    public Frag_Calendar_List() {
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(com.likwid.anilist.R.layout.frag_calendar_list, container, false);
        lvMainCalDue = (ListView) view.findViewById(com.likwid.anilist.R.id.lvCalMainDue);
        rlMainCalBG = (RelativeLayout) view.findViewById(com.likwid.anilist.R.id.rlCalMainBG);

        rlMainCalDueNone = (RelativeLayout) view.findViewById(com.likwid.anilist.R.id.rlMainCalDueNone);
        llNothingDueBG = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.llNothingDueBG);
        tvNothingDue = (TextView) view.findViewById(com.likwid.anilist.R.id.tvNothingDue);

        fc = (Frag_Calendar_View.FragmentCalendar) getActivity();

        Typeface TitleFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Allura.otf");
        tvNothingDue.setTypeface(TitleFont);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        database = new Adapter_SQL(getActivity());
        toSlide = (MainActivity) getActivity();

        selectedMonth = fc.getDate().get(Calendar.MONTH);
        selectedDay = fc.getDate().get(Calendar.DAY_OF_MONTH);
        selectedYear = fc.getDate().get(Calendar.YEAR);

        refreshCalList();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs;
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int ColorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
        Boolean ColorBG = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);

        if (ColorBG){
            rlMainCalBG.setBackgroundColor(getResources().getColor(ColorPrefInt));
        }else{
            rlMainCalBG.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        llNothingDueBG.setBackgroundColor(getResources().getColor(ColorPrefInt));
    }

    public void refreshCalList(){
        alarms.clear();
        database.open();
        alarms = database.getAllAlarms();
        database.close();
        alarms = SortAlarms(alarms);
        Adapter_CalList calListA = new Adapter_CalList(getActivity(), alarms);
        lvMainCalDue.setAdapter(calListA);

        tvNothingDue.setText("Nothing due on " + SimpleDateFormat.getInstance().format(fc.getDate().getTime()));
        if (alarms.size() > 0){
            rlMainCalDueNone.setVisibility(View.GONE);
            lvMainCalDue.setVisibility(View.VISIBLE);
        }else{
            rlMainCalDueNone.setVisibility(View.VISIBLE);
            lvMainCalDue.setVisibility(View.GONE);
        }
    }

    public ArrayList<Element> SortAlarms(ArrayList<Element> alarm){
        ArrayList<Element> alarmsForList = new ArrayList<>();
        alarmsForList.clear();

        //check if the selected date matches any alarm
        for (Element anAlarm : alarm) {
            Cursor c = getActivity().getContentResolver().query(CalendarContract.Events.CONTENT_URI,
                    CALCOL,
                    CalendarContract.Events._ID + "=" + anAlarm.alarm,
                    null,
                    null
            );
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                String time = c.getString(c.getColumnIndex(CalendarContract.Events.DTSTART));
                String rule = c.getString(c.getColumnIndex(CalendarContract.Events.RRULE));
                Log.d("rule", rule);

                if (rule.contains("UNTIL=")){
                    //main_add alarms that have an end date
                    int index = rule.lastIndexOf("UNTIL=");
                    Log.d("rule index", index+"");
                    String dateString = rule.substring(index+6, index+14);           //yyyyMMdd
                    Log.d("rule date", dateString);
                    int endYear = Integer.parseInt(dateString.substring(0,4));
                    int endMonth = Integer.parseInt(dateString.substring(4,6));
                    int endDay = Integer.parseInt(dateString.substring(6,8));
                    Calendar eventStart = Calendar.getInstance();
                    eventStart.setTimeInMillis(Long.parseLong(time));
                    eventStart.set(Calendar.HOUR_OF_DAY, 0);
                    eventStart.set(Calendar.MINUTE, 0);
                    eventStart.set(Calendar.SECOND, 0);

                    Calendar eventEnd = Calendar.getInstance();
                    eventEnd.set(endYear, endMonth-1, endDay);
                    eventEnd.set(Calendar.HOUR_OF_DAY, 23);
                    eventEnd.set(Calendar.MINUTE, 59);
                    eventEnd.set(Calendar.SECOND, 59);

                    //if the selected time falls between the start and end time of the event
                    if (fc.getDate().getTimeInMillis() > eventStart.getTimeInMillis()
                            && fc.getDate().getTimeInMillis() < eventEnd.getTimeInMillis()){

                        if (rule.contains("FREQ=DAILY;BYDAY=")) {
                            //if days are specified
                            String subRule = rule.substring(17);
                            if (subRule.contains("SU") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                                    || subRule.contains("MO") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
                                    || subRule.contains("TU") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
                                    || subRule.contains("WE") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
                                    || subRule.contains("TH") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
                                    || subRule.contains("FR") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
                                    || subRule.contains("SA") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                                    ) {
                                //main_add alarms where the day match the current day
                                alarmsForList.add(anAlarm);
                            }
                        }else if(rule.contains("FREQ=MONTHLY")) {
                            //monthly with ending time
                            if (eventStart.get(Calendar.DAY_OF_MONTH) == fc.getDate().get(Calendar.DAY_OF_MONTH)) {
                                alarmsForList.add(anAlarm);
                            }
                        }else if(rule.contains("FREQ=YEARLY")) {
                            //yearly with ending time
                            if (eventStart.get(Calendar.DAY_OF_YEAR) == fc.getDate().get(Calendar.DAY_OF_YEAR)) {
                                alarmsForList.add(anAlarm);
                            }
                        }else{
                            //if days are NOT specified
                            alarmsForList.add(anAlarm);
                        }
                    }
                }else{
                    //main_add alarms that run forever
                    Calendar eventStart = Calendar.getInstance();
                    eventStart.setTimeInMillis(Long.parseLong(time));
                    eventStart.set(Calendar.HOUR_OF_DAY, 0);
                    eventStart.set(Calendar.MINUTE, 0);
                    eventStart.set(Calendar.SECOND, 0);

                    //if the selected time is after the event time
                    if (fc.getDate().getTimeInMillis() >= eventStart.getTimeInMillis()) {
                        //if days are specified
                        if (rule.contains("FREQ=DAILY;BYDAY=")) {
                            String subRule = rule.substring(17);
                            if (subRule.contains("SU") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                                    || subRule.contains("MO") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
                                    || subRule.contains("TU") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
                                    || subRule.contains("WE") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
                                    || subRule.contains("TH") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
                                    || subRule.contains("FR") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
                                    || subRule.contains("SA") && (fc.getDate().get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                                    ) {
                                //main_add alarms where the day match the current day
                                if (!alarmsForList.contains(anAlarm))
                                    alarmsForList.add(anAlarm);
                            }
                        }else if(rule.contains("FREQ=MONTHLY")) {
                            //monthly with ending time
                            if (eventStart.get(Calendar.DAY_OF_MONTH) == fc.getDate().get(Calendar.DAY_OF_MONTH)) {
                                alarmsForList.add(anAlarm);
                            }
                        }else if(rule.contains("FREQ=YEARLY")) {
                            //yearly with ending time
                            if (eventStart.get(Calendar.DAY_OF_YEAR) == fc.getDate().get(Calendar.DAY_OF_YEAR)) {
                                alarmsForList.add(anAlarm);
                            }
                        }else{
                            //if days are NOT specified
                            if (!alarmsForList.contains(anAlarm)) {
                                alarmsForList.add(anAlarm);
                            }
                        }
                    }
                }
            }
            c.close();
        }

        Sort_Elements sort = new Sort_Elements();
        return sort.sortAlarm(alarmsForList, getActivity());
    }


}
