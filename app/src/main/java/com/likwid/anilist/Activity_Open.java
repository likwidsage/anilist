package com.likwid.anilist;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import java.io.File;
import java.util.ArrayList;


public class Activity_Open extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner sDropList;
    Button bSubmit;
    private ArrayList<Element> sorted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.likwid.anilist.R.layout.activity_open);

        sDropList = (Spinner) findViewById(com.likwid.anilist.R.id.sDropList);
        bSubmit = (Button) findViewById(com.likwid.anilist.R.id.bSubmit);

        Adapter_SQL database = new Adapter_SQL(getApplication());
        database.open();
        ArrayList<Element> elements = database.getAll();
        Sort_Elements sort = new Sort_Elements();
        sorted = sort.sortFolder(elements);
        sorted.add(0, new Element(0, "Main List", false, "blue", 0, false, 0, "0/"));
        ArrayList<String> list = new ArrayList<>();

        for (Element item: sorted){
            String prefix = "";
            int level = database.getLevel(item.id);
            for (int i = 0; i < level; i++){
                prefix = prefix + "\t\t";
            }
            list.add(prefix + item.title);
        }
        database.close();

        Adapter_spinner sAdapter = new Adapter_spinner(getApplication(), list, "#000000", "#00FFFFFF");
        sDropList.setAdapter(sAdapter);
        sDropList.setOnItemSelectedListener(this);


        bSubmit.setText("Import to Main List");
        bSubmit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case com.likwid.anilist.R.id.bSubmit:
                int pos = sDropList.getSelectedItemPosition();
                int selected = sorted.get(pos).id;
                ImportText it =new ImportText(getApplication(), selected);
                String filePath = getIntent().getData().getEncodedPath();
                String listAsString = it.ReadAni(new File(filePath));
                if (it.importText(listAsString)){
                    finish();
                }
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        bSubmit.setText("Import to '" + sorted.get(pos).title + "'");
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
