package com.likwid.anilist;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class Activity_QuickShare extends Activity{
	
	int selected;
    ExportList export = new ExportList(this);
    ArrayList<Integer> selection;

	public Activity_QuickShare() {}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        selection = getIntent().getIntegerArrayListExtra("selections");

        File zipFile = new File(MainActivity.EXPORT_FOLDER, "send" + MainActivity.EXTENSION);
        try {
            FileOutputStream fos = new FileOutputStream(zipFile);
            if (export.CreateAni(selection, fos)) {
                Toast.makeText(this, zipFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this, "Couldn't create a file.", Toast.LENGTH_SHORT).show();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        finish();
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
	
	
}