package com.likwid.anilist;

import android.animation.Animator;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Fragment;
import android.support.v4.view.VelocityTrackerCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewAnimationUtils;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;

import java.io.File;
import java.util.ArrayList;

public class Frag_MainList extends Fragment implements OnClickListener,
        OnLongClickListener, View.OnTouchListener {

    Bitmap bmpWrite, bmpAdd, bmpMan, bmpAddAlarm, bmpAddCam, bmpAddGal, bmpAddNote,
    bmpManExport, bmpManImport, bmpManRename, bmpManMove;

    public final static String FRAG_NAME = "frag_main";

    //Button does nothing
    private final int DO_NOTHING = 0;

    //sets button to do main_add actions
    private final int ADD_CAM = 1;
    private final int ADD_GAL = 2;
    private final int ADD_NOTE = 3;
    private final int ADD_ALARM = 4;

    //Set button to do main_manage actions
    private final int MAN_IMPORT = 5;
    private final int MAN_EXPORT = 6;
    private final int MAN_RENAME = 7;
    private final int MAN_MOVE = 8;
    private int button1, button2, button3, button4;

    //Set the 4 button set to main_add/main_manage
    private final int BUTTON_MODE_ADD = 9;
    private final int BUTTON_MODE_MAN = 10;
    private final int BUTTON_WRITE = 11;

    //Start as Add mode
    private int BUTTON_MODE = BUTTON_WRITE;
    private boolean expandedButton;

    private Boolean tutorialShown = false;
    private final String HELP_OPTION = "help_option";

    private VelocityTracker mVelocityTracker = null;

    static final String TIME = "Time";
    static final String ALPHA = "Alphabetical";
    static final String COLORS = "Colors";
    static final String USER = "Manual";
    static String sortMethod = TIME;

    static final String header = MainActivity.HEADER;
    static final String starter = MainActivity.STARTER;
    static final String footer = MainActivity.FOOTER;
    static final String seperator = MainActivity.SEPERATOR;

    static int dragStart, dragEnd;

    static String ARG_SECTION_NUMBER = "fragment_section";

    Adapter_SQL database;
    Adapter_List adapter;
    MainActivity toSlide;

    Animation downOut50, downIn50, upOut50, upIn50;
    ArrayList<Element> list = new ArrayList<>();
    Button bMovingAlert;
    ImageView ivMainBG, ivFloatButton1, ivFloatButton2, ivFloatButton3,
            ivFloatButton4, ivEmpty;
    ArrayList<Integer> moveID;

    LinearLayout llMovingAlert;
    RelativeLayout MainBG;
    String fullTitle = "";
    Sort_Elements sort = new Sort_Elements();
    TextView tvButton1, tvButton2, tvButton3, tvButton4;
    AdView ad;
    private DisplayMetrics dm = new DisplayMetrics();

    ListView lv;
    private Boolean showLabel = true;

    public Frag_MainList() {
    }

    public static Fragment newInstance(int sectionNumber) {
        Frag_MainList fragment = new Frag_MainList();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_mainlist, container, false);

        lv = (ListView) view.findViewById(R.id.lvMain);
        ivFloatButton1 = (ImageView) view.findViewById(R.id.ivFloatingButton1);
        ivFloatButton2 = (ImageView) view.findViewById(R.id.ivFloatingButton2);
        ivFloatButton3 = (ImageView) view.findViewById(R.id.ivFloatingButton3);
        ivFloatButton4 = (ImageView) view.findViewById(R.id.ivFloatingButton4);

        tvButton1 = (TextView) view.findViewById(R.id.tvButton1);
        tvButton2 = (TextView) view.findViewById(R.id.tvButton2);
        tvButton3 = (TextView) view.findViewById(R.id.tvButton3);
        tvButton4 = (TextView) view.findViewById(R.id.tvButton4);

        ad = (AdView) view.findViewById(R.id.ad);
        MainBG = (RelativeLayout) view.findViewById(R.id.rlMainLO);
        ivEmpty = (ImageView) view.findViewById(R.id.ivEmpty);
        bMovingAlert = (Button) view.findViewById(R.id.bMainMovingAlert);
        Button bMoveCancel = (Button) view.findViewById(R.id.bMainMoveCancel);
        llMovingAlert = (LinearLayout) view.findViewById(R.id.llMainMovingAlert);
        ivMainBG = (ImageView) view.findViewById(R.id.ivMainBG);

        ivFloatButton1.setOnClickListener(this);
        ivFloatButton2.setOnClickListener(this);
        ivFloatButton3.setOnClickListener(this);
        ivFloatButton4.setOnClickListener(this);

        ivFloatButton1.setOnLongClickListener(this);

        bMovingAlert.setOnClickListener(this);
        bMoveCancel.setOnClickListener(this);

        ivFloatButton1.setOnTouchListener(this);
        lv.setOnTouchListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("MainList Lifecycle", "onActivityCreated");

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        toSlide = (MainActivity) getActivity();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        database = new Adapter_SQL(getActivity());

        downIn50 = AnimationUtils.loadAnimation(getActivity(), R.anim.drag_exit_down);
        downOut50 = AnimationUtils.loadAnimation(getActivity(), R.anim.drag_slide_down);
        upIn50 = AnimationUtils.loadAnimation(getActivity(), R.anim.drag_exit_up);
        upOut50 = AnimationUtils.loadAnimation(getActivity(), R.anim.drag_slide_up);

        SetList();
        adapter = new Adapter_List(getActivity(), list);
        lv.setAdapter(adapter);

        CollapseButtons();
    }

    @Override
    public void onResume() {
        super.onResume();
        //onResume so that themes apply after pause
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        showLabel = prefs.getBoolean(getString(R.string.prefShowLabel), true);
        setMenu();
        setSelectionEffects();
        refresh();
    }

    public void switchSort(){
        switch (sortMethod) {
            case TIME:
                sortMethod = ALPHA;
                break;
            case ALPHA:
                sortMethod = COLORS;
                break;
            case COLORS:
                sortMethod = USER;
                break;
            case USER:
                sortMethod = TIME;
                break;
        }
    }

    public void SetList() {
        // Don't change set the global list to something new. It breaks notifyDataSetChanged
        // Only Modify it with clear, add, remove

        database.open();
        ArrayList<Element> newList = database.getChildren(toSlide.getViewId());
        database.close();

        //sort
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        sortMethod = prefs.getString(getString(R.string.prefSort), "time");
        sort = new Sort_Elements();

        switch (sortMethod) {
            case ALPHA:
                newList = new ArrayList<>(sort.sortAlphabetically(newList));
                break;
            case COLORS:
                newList = new ArrayList<>(sort.sortColor(newList));
                break;
            case USER:
                newList = new ArrayList<>(sort.sortPriorities(newList));
                break;
            default:
                sortMethod = TIME;
                break;
        }

        list.clear();
        for(Element single : newList){
            list.add(single);
        }
    }

    public void refresh() {
        //SetViews is dependent on list size which is set in SetList so SetViews should come after
        SetList();
        setSubTitle();
        setViews();
        adapter.notifyDataSetChanged();
    }

    public void setBG(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Boolean BgIsImage = pref.getBoolean(getString(R.string.prefBgIsImage), false);
        Boolean ColorBG = pref.getBoolean(getString(R.string.prefColorBG), false);

        if (BgIsImage && toSlide.hasThemes()) {
            File file = new File(getActivity().getFilesDir() + "/bg.jpg");

            if (file.exists()) {
                Drawable image = Drawable.createFromPath(file.getPath());
                ivMainBG.setImageDrawable(image);
            }else{
                ivMainBG.setImageResource(R.drawable.main_bg);
            }
        }else{
            ivMainBG.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        if (ColorBG){
            int color = pref.getInt(getString(R.string.prefColorInt), R.color.cBlue);
            ivMainBG.setImageResource(color);
        }else{
            ivMainBG.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public void setViews() {
        setBG();
        if (list.size() > 0) {
            //Items exist
            ivEmpty.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.VISIBLE);
        }

        if (toSlide.getMovingMode()) {
            llMovingAlert.setVisibility(View.VISIBLE);
        } else {
            llMovingAlert.setVisibility(View.GONE);
        }
    }

    public void moveUp() {
        if (toSlide.getViewId() > 0) {
            database.open();
            String path = database.getPath(toSlide.getViewId());

            String[] paths = path.split("/");
            int id = Integer.parseInt(paths[paths.length - 2]);
            toSlide.setViewId(id);

            if (!toSlide.getMovingMode()) toSlide.clearSelection();
            database.close();
            refresh();
        }
    }

    public void setSubTitle() {
        String viewPath;
        fullTitle = "";
        if (toSlide.getViewId() > 0) {
            database.open();
            viewPath = database.getPath(toSlide.getViewId());
            String[] paths = viewPath.split("/"); //Needs to stay "/" because that's how it's stored
            for (String path : paths) {
                int id = Integer.parseInt(path);
                if (id > 0){
                    fullTitle = fullTitle + database.getName(id) + " > ";
                }
            }
            database.close();
        }
    }

    public void goIntoItem(int id) {
        if (!toSlide.getMovingMode()) toSlide.clearSelection();
        toSlide.setViewId(id);
        refresh();
    }

    public void dropProcess() {
        Animation dropDown = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down_in100);
        Animation dropUp = AnimationUtils.loadAnimation(getActivity(), R.anim.drop_slide_up);
        if (dragStart != dragEnd) {
            if (dragStart > dragEnd) {
                //move up
                for (int i = dragEnd - lv.getFirstVisiblePosition() + 1; i < lv.getChildCount(); i++) {

                    lv.getChildAt(i).startAnimation(dropDown);
                }
            } else if (dragStart < dragEnd) {
                //move down
                for (int i = dragEnd - 1 - lv.getFirstVisiblePosition(); i > -1; i--) {
                    lv.getChildAt(i).startAnimation(dropUp);
                }
            }

            Element transfer = list.get(dragStart);
            this.list.remove(dragStart);
            this.list.add(dragEnd, transfer);
            database.open();
            for (int i = 0; i < list.size(); i++) {
                database.setPriority(list.get(i).id, i);
            }
            database.close();
            refresh();
            lv.smoothScrollToPosition(dragEnd);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(HELP_OPTION, tutorialShown);
        super.onSaveInstanceState(outState);
    }

    public void setMenu() {
        tvButton1.setVisibility(View.GONE);
        if (showLabel) tvButton1.setVisibility(View.VISIBLE);
        SetUserColor();
        SetThemePack();
    }

    private void SetUserColor() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Boolean colorMenu = pref.getBoolean(getString(R.string.prefColorMenu), true);

        int intColor = 0, color;
        if (colorMenu) {
            intColor = pref.getInt(getString(R.string.prefColorInt), R.color.cBlue);
            color = getResources().getColor(intColor);
        }else{
            color = Color.GRAY;
        }

        //Set icons and action bar color
        ShapeDrawable iconBG = new ShapeDrawable(new OvalShape());
        iconBG.setIntrinsicWidth(200);
        iconBG.setIntrinsicHeight(200);
        iconBG.getPaint().setColor(color);
        ivFloatButton1.setBackground(iconBG);
        ivFloatButton2.setBackground(iconBG);
        ivFloatButton3.setBackground(iconBG);
        ivFloatButton4.setBackground(iconBG);

        // Set statusbar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int barColor = Color.parseColor("#333333");
            switch (intColor){
                case R.color.cBlue:
                    barColor = getResources().getColor(R.color.dBlue);
                    break;
                case R.color.cRed:
                    barColor = getResources().getColor(R.color.dRed);
                    break;
                case R.color.cPink:
                    barColor = getResources().getColor(R.color.dPink);
                    break;
                case R.color.cGreen:
                    barColor = getResources().getColor(R.color.dGreen);
                    break;
                case R.color.cPurple:
                    barColor = getResources().getColor(R.color.dPurple);
                    break;
                case R.color.cOrange:
                    barColor = getResources().getColor(R.color.dOrange);
                    break;
                case R.color.cLime:
                    barColor = getResources().getColor(R.color.dLime);
                    break;
                case R.color.cYellow:
                    barColor = getResources().getColor(R.color.dYellow);
                    break;
                case R.color.cSky:
                    barColor = getResources().getColor(R.color.dSky);
                    break;
                case R.color.cSlate:
                    barColor = getResources().getColor(R.color.dSlate);
                    break;
            }
            getActivity().getWindow().setStatusBarColor(barColor);
        }

        int trans = pref.getInt(getString(R.string.pref_menu_alpha), 255);
    }

    private void SetThemePack(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String packageName = prefs.getString(getString(R.string.prefTheme), getActivity().getPackageName());

        try {
            Resources res = getActivity().getPackageManager().getResourcesForApplication(packageName);
            int resourceImg;

            resourceImg = res.getIdentifier("main_write", "drawable", packageName);
            if (resourceImg != 0) bmpWrite = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpWrite = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.main_write);

            resourceImg = res.getIdentifier("main_add", "drawable", packageName);
            if (resourceImg != 0) bmpAdd = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpAdd = BitmapFactory.decodeResource(getResources(), R.drawable.main_add);

            resourceImg = res.getIdentifier("add_alarm", "drawable", packageName);
            if (resourceImg != 0) bmpAddAlarm = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpAddAlarm = BitmapFactory.decodeResource(getResources(), R.drawable.add_alarm);

            resourceImg = res.getIdentifier("add_camera", "drawable", packageName);
            if (resourceImg != 0) bmpAddCam = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpAddCam = BitmapFactory.decodeResource(getResources(), R.drawable.add_camera);

            resourceImg = res.getIdentifier("add_gallery", "drawable", packageName);
            if (resourceImg != 0) bmpAddGal = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpAddGal = BitmapFactory.decodeResource(getResources(), R.drawable.add_gallery);

            resourceImg = res.getIdentifier("add_note", "drawable", packageName);
            if (resourceImg != 0) bmpAddNote = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpAddNote = BitmapFactory.decodeResource(getResources(), R.drawable.add_note);

            resourceImg = res.getIdentifier("main_manage", "drawable", packageName);
            if (resourceImg != 0) bmpMan = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpMan = BitmapFactory.decodeResource(getResources(), R.drawable.main_manage);

            resourceImg = res.getIdentifier("manage_import", "drawable", packageName);
            if (resourceImg != 0) bmpManImport = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpManImport = BitmapFactory.decodeResource(getResources(), R.drawable.manage_import);

            resourceImg = res.getIdentifier("manage_export", "drawable", packageName);
            if (resourceImg != 0) bmpManExport = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpManExport = BitmapFactory.decodeResource(getResources(), R.drawable.manage_export);

            resourceImg = res.getIdentifier("manage_move", "drawable", packageName);
            if (resourceImg != 0) bmpManMove = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpManMove = BitmapFactory.decodeResource(getResources(), R.drawable.manage_move);

            resourceImg = res.getIdentifier("manage_rename", "drawable", packageName);
            if (resourceImg != 0) bmpManRename = BitmapFactory.decodeResource(res, resourceImg);
            if (resourceImg == 0) bmpManRename = BitmapFactory.decodeResource(getResources(), R.drawable.manage_rename);

            //Get opacity from package
            String alphaString;
            int alpha = 255;

            resourceImg = res.getIdentifier("opacity", "string", packageName);
            if (resourceImg != 0) {
                alphaString = (String) res.getText(resourceImg);
                alpha = Integer.parseInt(alphaString);
            }

            ivFloatButton1.getBackground().setAlpha(alpha);
            ivFloatButton2.getBackground().setAlpha(alpha);
            ivFloatButton3.getBackground().setAlpha(alpha);
            ivFloatButton4.getBackground().setAlpha(alpha);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

            bmpWrite = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.main_write);
            bmpAdd = BitmapFactory.decodeResource(getResources(), R.drawable.main_add);
            bmpAddAlarm = BitmapFactory.decodeResource(getResources(), R.drawable.add_alarm);
            bmpAddCam = BitmapFactory.decodeResource(getResources(), R.drawable.add_camera);
            bmpAddGal = BitmapFactory.decodeResource(getResources(), R.drawable.add_gallery);
            bmpAddNote = BitmapFactory.decodeResource(getResources(), R.drawable.add_note);
            bmpMan = BitmapFactory.decodeResource(getResources(), R.drawable.main_manage);
            bmpManImport = BitmapFactory.decodeResource(getResources(), R.drawable.manage_import);
            bmpManExport = BitmapFactory.decodeResource(getResources(), R.drawable.manage_export);
            bmpManMove = BitmapFactory.decodeResource(getResources(), R.drawable.manage_move);
            bmpManRename = BitmapFactory.decodeResource(getResources(), R.drawable.manage_rename);
        }
    }

    //Setting onlySetIcons to true will not switch the mode, but will set icons.
    //Setting it to false will switch the mode AND set icons.
    private void SwitchSetMode(Boolean onlySetIcons){
        if (BUTTON_MODE != BUTTON_WRITE) {
            if (!onlySetIcons) {
                Animation button1Anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button1_swtich);
                ivFloatButton1.startAnimation(button1Anim);

                //Switch button mode if onlySetIcons is false

                if (BUTTON_MODE == BUTTON_MODE_ADD) {
                    BUTTON_MODE = BUTTON_MODE_MAN;
                } else if (BUTTON_MODE == BUTTON_MODE_MAN) {
                    BUTTON_MODE = BUTTON_MODE_ADD;
                }
            }

            //set icons to new button mode
            if (BUTTON_MODE == BUTTON_MODE_ADD) {
                if (expandedButton) {
                    ivFloatButton1.setImageBitmap(bmpAddAlarm);
                    button1 = ADD_ALARM;
                    tvButton1.setText("Alarm");
                } else {
                    ivFloatButton1.setImageBitmap(bmpAdd);
                    button1 = DO_NOTHING;
                    tvButton1.setText("Add");
                }
                ivFloatButton2.setImageBitmap(bmpAddCam);
                button2 = ADD_CAM;
                tvButton2.setText("Capture");
                ivFloatButton3.setImageBitmap(bmpAddGal);
                button3 = ADD_GAL;
                tvButton3.setText("Picture");
                ivFloatButton4.setImageBitmap(bmpAddNote);
                button4 = ADD_NOTE;
                tvButton4.setText("Note");
            }else
            if (BUTTON_MODE == BUTTON_MODE_MAN) {
                if (expandedButton) {
                    ivFloatButton1.setImageBitmap(bmpManExport);
                    button1 = MAN_EXPORT;
                    tvButton1.setText("Export");
                } else {
                    ivFloatButton1.setImageBitmap(bmpMan);
                    button1 = DO_NOTHING;
                    tvButton1.setText("Manage");
                }
                ivFloatButton2.setImageBitmap(bmpManImport);
                button2 = MAN_IMPORT;
                tvButton2.setText("Import");
                ivFloatButton3.setImageBitmap(bmpManRename);
                button3 = MAN_RENAME;
                tvButton3.setText("Rename");
                ivFloatButton4.setImageBitmap(bmpManMove);
                button4 = MAN_MOVE;
                tvButton4.setText("Move");
            }
        }
    }

    private void CollapseButtons(){
        //If buttons are expanded
        if (expandedButton) {
            button1 = DO_NOTHING;
            expandedButton = false;

            Animation button2Anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button2_hide);
            Animation button3Anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button3_hide);
            Animation button4Anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button4_hide);

            ivFloatButton2.startAnimation(button2Anim);
            if (tvButton2.getVisibility() == View.VISIBLE) tvButton2.startAnimation(button2Anim);
            ivFloatButton3.startAnimation(button3Anim);
            if (tvButton3.getVisibility() == View.VISIBLE) tvButton3.startAnimation(button3Anim);
            ivFloatButton4.startAnimation(button4Anim);
            if (tvButton4.getVisibility() == View.VISIBLE) tvButton4.startAnimation(button4Anim);

            ivFloatButton2.setVisibility(View.GONE);
            tvButton2.setVisibility(View.GONE);
            ivFloatButton3.setVisibility(View.GONE);
            tvButton3.setVisibility(View.GONE);
            ivFloatButton4.setVisibility(View.GONE);
            tvButton4.setVisibility(View.GONE);

            if (BUTTON_MODE == BUTTON_MODE_ADD) {
                ivFloatButton1.setImageBitmap(bmpAdd);
                tvButton1.setText("Add");
            }

            if (BUTTON_MODE == BUTTON_MODE_MAN) {
                ivFloatButton1.setImageBitmap(bmpMan);
                tvButton1.setText("Manage");
            }
        }
    }

    private void ExpandButtons(){
        //If buttons aren't expanded
        if (!expandedButton && BUTTON_MODE != BUTTON_WRITE) {
            expandedButton = true;
            if (BUTTON_MODE == BUTTON_MODE_ADD) {
                button1 = ADD_ALARM;
                ivFloatButton1.setImageBitmap(bmpAddAlarm);
                tvButton1.setText("Alarm");
            }

            if (BUTTON_MODE == BUTTON_MODE_MAN) {
                ivFloatButton1.setImageBitmap(bmpManExport);
                button1 = MAN_EXPORT;
                tvButton1.setText("Export");
            }

            Animation button2Anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button2_show);
            Animation button3Anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button3_show);
            Animation button4Anim = AnimationUtils.loadAnimation(getActivity(), R.anim.button4_show);

            ivFloatButton2.startAnimation(button2Anim);
            if (tvButton2.getVisibility() == View.VISIBLE) tvButton2.startAnimation(button2Anim);
            ivFloatButton3.startAnimation(button3Anim);
            if (tvButton3.getVisibility() == View.VISIBLE) tvButton3.startAnimation(button3Anim);
            ivFloatButton4.startAnimation(button4Anim);
            if (tvButton4.getVisibility() == View.VISIBLE) tvButton4.startAnimation(button4Anim);

            ivFloatButton2.setVisibility(View.VISIBLE);
            ivFloatButton3.setVisibility(View.VISIBLE);
            ivFloatButton4.setVisibility(View.VISIBLE);

            if (showLabel) {
                tvButton2.setVisibility(View.VISIBLE);
                tvButton3.setVisibility(View.VISIBLE);
                tvButton4.setVisibility(View.VISIBLE);
            }
        }
    }

    //calls function of each button defined by the button function
    private void ButtonPressed(int button){
        menuItems type = (menuItems) getActivity();
        switch (button) {
            case 1:
                if (button1 == ADD_ALARM){
                    if (toSlide.getSelection().size() > 0)
                        type.addAlarm(toSlide.getSelection().get(0));
                }else if (button1 == MAN_EXPORT) {
                    type.exportItem();
                }else if (button1 == BUTTON_WRITE){
                    //adding to list
                    Dial_Add add = Dial_Add.newInstance();
                    add.show(getFragmentManager().beginTransaction(), "add_frame");

                    InputMethodManager inputMethodManager=(InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.toggleSoftInputFromWindow(llMovingAlert.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);

                }else if (button1 == DO_NOTHING) {
                    ExpandButtons();
                }
                break;
            case 2:
                if (button2 == ADD_CAM){
                    type.addCamera();
                }else if (button2 == MAN_IMPORT){
                    type.importItem();
                }
                break;
            case 3:
                if (button3 == ADD_GAL){
                    type.addGallery();
                }else if (button3 == MAN_RENAME){
                    type.renameItem();
                }
                break;
            case 4:
                if (button4 == ADD_NOTE){
                    type.addNote();
                }else if (button4 == MAN_MOVE){
                    InitMoveItem(toSlide.getSelection(), -1);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (v.getId() != R.id.ivFloatingButton1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Animator tapAnim;
                tapAnim = ViewAnimationUtils.createCircularReveal(v,
                        v.getWidth() / 2, v.getHeight() / 2, 0, v.getWidth());
                tapAnim.setDuration(ViewConfiguration.getDoubleTapTimeout());
                tapAnim.start();
            } else {
                Animation tapped = AnimationUtils.loadAnimation(getActivity(), R.anim.tapped);
                v.startAnimation(tapped);
            }
        }

        switch (v.getId()) {
            case R.id.ivFloatingButton1:
                ButtonPressed(1);
                break;
            case R.id.ivFloatingButton2:
                ButtonPressed(2);
                break;
            case R.id.ivFloatingButton3:
                ButtonPressed(3);
                break;
            case R.id.ivFloatingButton4:
                ButtonPressed(4);
                break;
            case R.id.bMainMovingAlert:
                InitMoveItem(moveID, toSlide.getViewId());
                break;
            case R.id.bMainMoveCancel:
                endMoving();
                break;
        }
    }

    public void setSelectionEffects(){
        Animation hideButton = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);

        if (toSlide.getSelection().size() <= 0) {

            if (ivFloatButton2.getVisibility() == View.VISIBLE) {
                ivFloatButton2.startAnimation(hideButton);
                ivFloatButton2.setVisibility(View.GONE);
                tvButton2.setVisibility(View.GONE);
            }

            if (ivFloatButton3.getVisibility() == View.VISIBLE) {
                ivFloatButton3.startAnimation(hideButton);
                ivFloatButton3.setVisibility(View.GONE);
                tvButton3.setVisibility(View.GONE);
            }

            if (ivFloatButton4.getVisibility() == View.VISIBLE) {
                ivFloatButton4.startAnimation(hideButton);
                ivFloatButton4.setVisibility(View.GONE);
                tvButton4.setVisibility(View.GONE);
            }

            expandedButton = false;
            button2 = DO_NOTHING;

            BUTTON_MODE = BUTTON_WRITE;
        }else if (toSlide.getSelection().size() == 1){
            BUTTON_MODE = BUTTON_MODE_ADD;
        }else{
            BUTTON_MODE = BUTTON_MODE_MAN;
        }

        //Set icon image according to what mode is set to
        if (BUTTON_MODE == BUTTON_MODE_ADD) {
            ivFloatButton1.setImageBitmap(bmpAdd);
            button1 = DO_NOTHING;
            tvButton1.setText("Add");
        }else if (BUTTON_MODE == BUTTON_MODE_MAN) {
            ivFloatButton1.setImageBitmap(bmpMan);
            button1 = DO_NOTHING;
            tvButton1.setText("Manage");
        }else if (BUTTON_MODE == BUTTON_WRITE) {
            ivFloatButton1.setImageBitmap(bmpWrite);
            button1 = BUTTON_WRITE;
            tvButton1.setText("Create");
        }

        SwitchSetMode(true);

        //Set Action Bar icon to show if clipboard has
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip()) {
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            String clipText = item.getText().toString();
            if (clipText.startsWith(header) && clipText.endsWith(footer)
                    && clipText.contains(starter) && clipText.contains(seperator)) {
            }
        }
        setMenu();
    }

    public void toolTip(String title, String text) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View helpView = li.inflate(R.layout.diag_help, null);
        TextView helpText = (TextView) helpView.findViewById(R.id.tvHelpText);
        Button bDismiss = (Button) helpView.findViewById(R.id.bDiagHelpDismis);
        helpText.setText(text);

        final Dialog diag = new Dialog(getActivity());
        diag.setTitle(title);
        diag.setContentView(helpView);
        diag.setCanceledOnTouchOutside(true);
        bDismiss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                diag.dismiss();
            }
        });

        diag.show();
    }

    @Override
    public boolean onLongClick(View v) {
        String tipText;
        String title = "What does this do?";
        switch (v.getId()) {
            case R.id.ivFloatingButton1:
                tipText = "When no items are selected, tap to add an item to the current list\n\nIf items are selected, you can perform the following gestures on the button:\n" +
                        "- Swipe left to change categories.\n- If 1 icon is showing, tap/swipe up to expand the current category.\n- If 4 icons are showing, swipe down to collapse the menu.";
                toolTip(title, tipText);
                break;
        }
        return true;
    }

    //Set Elements PID to the given PID | PID of -1 indicates initiation, otherwise moves
    public void InitMoveItem(final ArrayList<Integer> ids, final int pid) {
        if (pid > -1) {
            //If pid is given, change the parent
            database.open();
            int errorCode = 0;
            for (Integer anId : ids) {
                //Check for illegal moves for all ids
                String idPath = database.getPath(anId) + anId;
                String pidPath = (pid > 0 ? database.getPath(pid) + pid : "0/");

                if (pidPath.contains(idPath)) {
                    //Attempting to move into self
                    errorCode = 1;
                    break;
                } else if (database.getParent(anId) == pid) {
                    //Old Parent = New Parent
                    errorCode = 2;
                    break;
                }
            }

            if (errorCode == 0) {
                //If no illegal moves
                AlertDialog.Builder db = new Builder(getActivity());
                db.setTitle("Move item?");
                String name = database.getName(pid);
                //show confirmation based on the amount of selected items
                if (ids.size() == 1) {
                    db.setMessage("Would you like to move '" + database.getName(ids.get(0)) + "' to '" + name + "'?");
                }else if(ids.size() > 1){
                    db.setMessage("Would you like to move " + ids.size() + " items to '" + name + "'?");
                }
                db.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        database.open();
                        for (Integer anId : ids) {
                            database.changeParent(anId, pid);
                        }
                        database.close();
                        toSlide.clearSelection();
                        refresh();
                    }
                });
                db.setNegativeButton("No", null);
                db.show();
            } else if (errorCode == 1) {
                toolTip("Illegal Operation", "Can NOT move an item into itself.");
            } else if (errorCode == 2) {
                toolTip("Move Cancelled", "Item already exists in this directory");
            }
            database.close();
            toSlide.SetMovingMode(false);
            endMoving();
        } else { //Initiate the move
            moveID = ids;
            toSlide.SetMovingMode(true);
            Animation showAlert = AnimationUtils.loadAnimation(getActivity(), R.anim.showpanel);
            showAlert.setFillAfter(false);
            llMovingAlert.startAnimation(showAlert);
            llMovingAlert.setVisibility(View.VISIBLE);
            bMovingAlert.setVisibility(View.VISIBLE);
            database.open();
            if (ids.size() == 1){
                bMovingAlert.setText("Tap to move " + ids.size() + " item here.");
            }else if (ids.size() > 1){
                bMovingAlert.setText("Tap to move " + ids.size() + " items here.");
            }
            database.close();
            ivFloatButton1.setVisibility(View.INVISIBLE);
            ivFloatButton2.setVisibility(View.INVISIBLE);
            ivFloatButton3.setVisibility(View.INVISIBLE);
            ivFloatButton4.setVisibility(View.INVISIBLE);
        }
    }

    public void endMoving() {
        toSlide.SetMovingMode(false);
        Animation hideAlert = AnimationUtils.loadAnimation(getActivity(), R.anim.hide_panel);
        llMovingAlert.startAnimation(hideAlert);
        llMovingAlert.setVisibility(View.GONE);
        bMovingAlert.setVisibility(View.GONE);
        ivFloatButton1.setVisibility(View.VISIBLE);
        ivFloatButton2.setVisibility(View.VISIBLE);
        ivFloatButton3.setVisibility(View.VISIBLE);
        ivFloatButton4.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int index = event.getActionIndex();
        int pointerId = event.getPointerId(index);
        int action = event.getActionMasked();

        if (view.getId() == R.id.ivFloatingButton1){
            //If it's the floating button
            switch (action){
                case MotionEvent.ACTION_DOWN:
                    if (mVelocityTracker == null) {
                        mVelocityTracker = VelocityTracker.obtain();
                    }else{
                        mVelocityTracker.clear();
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    mVelocityTracker.addMovement(event);
                    mVelocityTracker.computeCurrentVelocity(1000);
                    break;
                case MotionEvent.ACTION_UP:
                    Float velX = VelocityTrackerCompat.getXVelocity(mVelocityTracker, pointerId);
                    Float velY = VelocityTrackerCompat.getYVelocity(mVelocityTracker, pointerId);

                    if (Math.abs(velY) > Math.abs(velX)) {
                        //If moved upwards, than ExpandButtons
                        if (velY < -200f) {
                            ExpandButtons();
                        } else
                        //If moved downwards, than CollapseButtons
                        if (velY > 200f) {
                            CollapseButtons();
                        }
                    }else{
                        if (velX < -1000f){
                            SwitchSetMode(false);
                        }
                    }
                    break;
            }

        }else{
            //If its' not the floating button
            switch (action){
                case MotionEvent.ACTION_DOWN:
                    if (mVelocityTracker == null && dm.widthPixels - event.getX() < 50) {
                        mVelocityTracker = VelocityTracker.obtain();
                    }else if (mVelocityTracker != null){
                        mVelocityTracker = null;
                    }
                case MotionEvent.ACTION_MOVE:
                    if (mVelocityTracker != null) {
                        mVelocityTracker.addMovement(event);
                        mVelocityTracker.computeCurrentVelocity(1000);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (mVelocityTracker != null && mVelocityTracker.getXVelocity() < -1000f) {
                        toSlide.spClosePanel(true);
                        mVelocityTracker = null;
                    }
                    break;
            }
        }
        return false;
    }

    interface menuItems {
        void addNote();
        void addCamera();
        void addGallery();
        void addAlarm(int selection);
        void importItem();
        void exportItem();
        void renameItem();
        void viewPhotos(int id);
        void viewAll();
        void viewNotes(int id);
    }

}
