package com.likwid.anilist;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

public class Adapter_Calendar extends BaseAdapter {
    private final ShapeDrawable iconBG;
    private final ShapeDrawable shadeBG;
    private final ShapeDrawable selectedIconBG;
    int startDay = 0;
    Context mContext;
    View row;
    Calendar mCal = Calendar.getInstance();
    Calendar selectedCal;
    ImageView ivBlue, ivRed, ivPink, ivPurple, ivOrange, ivGreen, ivLime, ivYellow, ivSky, ivSlate;
    Adapter_SQL database;
    TextView tvCalAdapterNum;
    Frag_Calendar_View.FragmentCalendar fc;
    private int adapterDate;
    SharedPreferences prefs;
    Long time;

    public Adapter_Calendar(Context c, Long timeInMillis){
        mContext = c;
        time = timeInMillis;
        mCal.setTimeInMillis(time);
        fc = (Frag_Calendar_View.FragmentCalendar) mContext;
        selectedCal = fc.getDate();
        prefs = PreferenceManager.getDefaultSharedPreferences(c);

        int color = prefs.getInt(mContext.getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
        int colorUsed = mContext.getResources().getColor(color);
        float r = 0f;
        float[] rf = {r,r,r,r,r,r,r,r};
        Shape calBgShape = new RoundRectShape(rf, null, rf);

        iconBG = new ShapeDrawable(calBgShape);
        iconBG.setIntrinsicWidth(200);
        iconBG.setIntrinsicHeight(200);
        iconBG.getPaint().setColor(colorUsed);

        shadeBG = new ShapeDrawable(calBgShape);
        shadeBG.setIntrinsicWidth(200);
        shadeBG.setIntrinsicHeight(200);
        shadeBG.getPaint().setColor(Color.parseColor("#33000000"));

        selectedIconBG = new ShapeDrawable(calBgShape);
        selectedIconBG.setIntrinsicWidth(200);
        selectedIconBG.setIntrinsicHeight(200);
        selectedIconBG.getPaint().setColor(Color.parseColor("#999999"));
    }

    @Override
    public int getCount() {
        return mCal.getActualMaximum(Calendar.DAY_OF_MONTH)+startDay;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        row = view;
        if (row == null){
            LayoutInflater inflate = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflate.inflate(com.likwid.anilist.R.layout.adapter_calendar_item, viewGroup, false);
        }

        Calendar tCal = Calendar.getInstance();
        tCal.setTime(mCal.getTime());
        tCal.set(Calendar.DAY_OF_MONTH, 1);
        startDay = tCal.get(Calendar.DAY_OF_WEEK)-1;

        if (row != null){
            tvCalAdapterNum = (TextView) row.findViewById(com.likwid.anilist.R.id.tvCalAdapterNum);
            RelativeLayout llCalAdapter = (RelativeLayout) row.findViewById(com.likwid.anilist.R.id.llCalAdapter);

            ivBlue = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCircleblue);
            ivRed = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCirclered);
            ivPink = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCirclepink);
            ivPurple = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCirclepurple);
            ivOrange = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCircleorange);
            ivGreen = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCirclegreen);
            ivLime = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCirclelime);
            ivYellow = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCircleyellow);
            ivSky = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCirclesky);
            ivSlate = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCircleSlate);

            adapterDate = i-startDay+1;

            int month = fc.getDate().get(Calendar.MONTH) + 1;
            String tagSuffix = "" + fc.getDate().get(Calendar.YEAR) + month + adapterDate;

            ivBlue.setTag("blue"+tagSuffix);
            ivRed.setTag("red"+tagSuffix);
            ivPink.setTag("pink"+tagSuffix);
            ivPurple.setTag("purple"+tagSuffix);
            ivOrange.setTag("orange"+tagSuffix);
            ivGreen.setTag("green"+tagSuffix);
            ivLime.setTag("lime"+tagSuffix);
            ivYellow.setTag("yellow"+tagSuffix);
            ivSky.setTag("sky"+tagSuffix);
            ivSlate.setTag("slate"+tagSuffix);

            Calendar adapterCal = Calendar.getInstance();
            adapterCal.setTime(fc.getDate().getTime());
            adapterCal.set(Calendar.DAY_OF_MONTH, adapterDate);
            adapterCal.set(Calendar.HOUR_OF_DAY, 23);
            adapterCal.set(Calendar.MINUTE, 59);
            adapterCal.set(Calendar.SECOND, 59);
            Boolean datePassed = false;
            if (adapterCal.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()){
                datePassed = true;
            }

            if (adapterDate > 0) {
                tvCalAdapterNum.setText(adapterDate+"");
                llCalAdapter.setTag(adapterDate);
                llCalAdapter.setVisibility(View.VISIBLE);

                if (datePassed){
                    llCalAdapter.setAlpha(.25f);
                }else{
                    llCalAdapter.setAlpha(1f);
                }

            }else{
                tvCalAdapterNum.setText("");
                llCalAdapter.setVisibility(View.INVISIBLE);
            }

            //set Backgrounds
            int color = prefs.getInt(mContext.getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);


            tvCalAdapterNum.setBackground(shadeBG);
            if(selectedCal.get(Calendar.DAY_OF_MONTH) == adapterDate
                    && selectedCal.get(Calendar.MONTH) == mCal.get(Calendar.MONTH)
                    && selectedCal.get(Calendar.YEAR) == mCal.get(Calendar.YEAR)){
                llCalAdapter.setBackground(selectedIconBG);
            }else{
                llCalAdapter.setBackground(iconBG);
            }



            //set today's date text color
            if (adapterCal.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR)
            && adapterCal.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)){
                tvCalAdapterNum.setTextColor(Color.WHITE);
            }else{
                tvCalAdapterNum.setTextColor(Color.BLACK);
            }
        }

        //when the last day is done, call CalAdaptDone
        if (adapterDate == fc.getDate().getActualMaximum(Calendar.DAY_OF_MONTH)){
            CalAdaptFinished caf = (CalAdaptFinished) mContext;
            caf.CalAdaptDone();
        }

        return row;
    }

    interface CalAdaptFinished{
        void CalAdaptDone();
    }
}