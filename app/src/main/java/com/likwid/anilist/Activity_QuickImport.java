package com.likwid.anilist;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;

public class Activity_QuickImport extends Activity{

	static final File folder = MainActivity.EXPORT_FOLDER;
	Adapter_SQL database;
	String importText;
	int selected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		database = new Adapter_SQL(getApplication());
        selected = getIntent().getIntExtra("selected", 0);
		
		//Detect clipboard
		ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		if (clipboard.hasPrimaryClip()){
			ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
			this.importText = item.getText().toString();
            new ImportText(getApplication(), selected).importText(importText);
            finish();
		}else{
			Toast.makeText(getApplication(), "Couldn't find any thing to copy in.\nPlease copy the text first.", Toast.LENGTH_SHORT).show();
			finish();
		}
	}
}
