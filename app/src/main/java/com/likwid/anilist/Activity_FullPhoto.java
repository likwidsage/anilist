package com.likwid.anilist;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import java.io.File;

public class Activity_FullPhoto extends FragmentActivity{
	
	static final File folder = MainActivity.PHOTOS_FOLDER;
	String name;
	RelativeLayout layout;
	ImageTouchView ivFullPhoto;

	@Override
	protected void onCreate(Bundle arg0) {
        if (getActionBar() != null) {
			getActionBar().hide();
		}
		super.onCreate(arg0);
		name = getIntent().getExtras().getString("fileName");
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(com.likwid.anilist.R.layout.activity_full_photo);
		
		ivFullPhoto = (ImageTouchView) findViewById(com.likwid.anilist.R.id.ivFullPhoto);
        Uri picUri = Uri.fromFile(new File(folder, name));
        ivFullPhoto.setImageURI(picUri);
	}
	
	@Override
	public Intent getParentActivityIntent() {
		finish();
		return super.getParentActivityIntent();
	}
}
