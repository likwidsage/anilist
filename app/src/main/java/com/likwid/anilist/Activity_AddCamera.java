package com.likwid.anilist;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Activity_AddCamera extends FragmentActivity {

	final File folder = MainActivity.PHOTOS_FOLDER;
	private static final int RESULT_CAMERA = 0;
	Intent mIntent;
	int selected;
	Adapter_SQL database;
	Element parentInfo;
	Uri photoUri;
	File photoFile;
	Boolean takingPic = false;
	Intent takepic;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		database = new Adapter_SQL(this);
		if (arg0 != null) {
			takingPic = arg0.getBoolean("takingPic");
			photoUri = Uri.parse(arg0.getString("photoUri"));
			selected = arg0.getInt("selected");

			
			File photo1 = new File(photoUri.getPath());
			long photoSize = photo1.length();
			if (photoSize > 0){
				String photoName = photoUri.getLastPathSegment();
				database.open();
				database.addPhoto(photoName, selected);
				database.close();
				Toast.makeText(getApplication(), photoName + "  saved.",
						Toast.LENGTH_SHORT).show();
				finish();
			}
		}

		mIntent = getIntent();
		selected = mIntent.getExtras().getInt("selected", 0);

		if (selected > 0) {
			if (!folder.exists()) {
				folder.mkdir();
			}
			database.open();
			parentInfo = database.getSingle(selected);
			database.close();
			if (!takingPic) {
				takePicture();
			} else {
				finish();
			}
		} else {
			Toast.makeText(getApplication(), "Select item to main_add picture to.",
					Toast.LENGTH_LONG).show();
			finish();
		}
	}

	private void takePicture() {
		takingPic = true;
		takepic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takepic.resolveActivity(getPackageManager()) != null) {
			photoFile = null;
			try {
				photoFile = MakeFileName();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (photoFile != null) {
				NoMedia();
				photoUri = Uri.fromFile(photoFile);
				takepic.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
				startActivityForResult(takepic, RESULT_CAMERA);

			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == RESULT_CAMERA) {
			if (resultCode == RESULT_OK) {
				picToDatabase(photoUri);
			} else if (resultCode == RESULT_CANCELED) {
				takingPic = false;
				finish();
			}
		}
	}

	private void picToDatabase(Uri photoUri) {
		String photoName = photoUri.getLastPathSegment();
		if (photoFile.length() > 0) {
			database.open();
			database.addPhoto(photoName, selected);
			database.close();
			Toast.makeText(getApplication(), photoName + "  saved.",
					Toast.LENGTH_SHORT).show();
			finish();
		} else {
			photoFile.delete();
		}
	}

	private File MakeFileName() throws IOException {
		database.open();
		int picNum = database.getCount() + 1;
		database.close();

		String StringNum = String.format("%05d", picNum);

        return File.createTempFile("elementPic" + StringNum, ".jpg", folder);
	}

	private void NoMedia() {
		// Create . nomedia
		File nomedia = new File(folder, ".nomedia");
		if (!nomedia.exists()) {
			try {
				FileOutputStream nomediaOut = new FileOutputStream(nomedia);
				nomediaOut.flush();
				nomediaOut.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean("takingPic", takingPic);
		outState.putString("photoUri", photoUri.toString());
		outState.putInt("selected", selected);
		super.onSaveInstanceState(outState);
	}
}
