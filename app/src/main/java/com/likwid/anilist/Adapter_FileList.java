package com.likwid.anilist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_FileList extends BaseAdapter{
	
	private Context ourContext;
	LayoutInflater inflate;
	ArrayList<String> values;
	int id;
	String text;
	
	public Adapter_FileList(Context c, ArrayList<String> list){
		ourContext = c;
		values = list;

	}

	@Override
	public View getView(int pos, View convert, ViewGroup parent) {
		View spinner = convert;
		if (spinner == null){
			inflate = (LayoutInflater) ourContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			spinner = inflate.inflate(com.likwid.anilist.R.layout.adapter_spinner_item, parent, false);
		}
		
		TextView spinnerText = (TextView) spinner.findViewById(com.likwid.anilist.R.id.SpinnerText);
		
		String[] split = values.get(pos).split("/");
		String file = split[split.length-1];
		String[] filename = file.split("\\.");
		
		
		
		spinnerText.setText(filename[0]);
		
		return spinner; 
	}

	@Override
	public int getCount() { return values.size(); }
	@Override
	public Object getItem(int pos) { return pos; }
	@Override
	public long getItemId(int pos) {return pos; }
}