package com.likwid.anilist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class Activity_OpenNote extends Activity {

	TextView note;
	int noteID;
	ArrayList<Element> noteList;
	Adapter_SQL database;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.likwid.anilist.R.layout.activity_open_note);
		noteID = getIntent().getExtras().getInt("noteID");
		database = new Adapter_SQL(this);
		note = (TextView) findViewById(com.likwid.anilist.R.id.tvOpenNote);
				
		database.open();
		noteList = database.getSingleNote(noteID);
		database.close();
		
		note.setText(noteList.get(0).title);
	}
	
	@Override
	public Intent getParentActivityIntent() {
		finish();
		return super.getParentActivityIntent();
	}

}
