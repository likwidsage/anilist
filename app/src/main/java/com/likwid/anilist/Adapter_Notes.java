package com.likwid.anilist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_Notes extends BaseAdapter{
	
	ArrayList<Element> noteList;
	Context ourContext;
	TextView tvNoteText;

	
	Adapter_Notes(Context c, ArrayList<Element> notelist){
		noteList = notelist;
		ourContext = c;
		
	}

	@Override
	public View getView(int pos, View convert, ViewGroup parent) {
		View row = convert;
		if (row == null){
			LayoutInflater inflate = (LayoutInflater) ourContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflate.inflate(com.likwid.anilist.R.layout.adapter_notes_row, parent, false);
		}
		tvNoteText = (TextView) row.findViewById(com.likwid.anilist.R.id.tvNoteRowText);
		tvNoteText.setText(noteList.get(pos).title);
		
		return row;
	}
	
	@Override
	public int getCount() {	return noteList.size(); }
	@Override
	public Object getItem(int pos) { return pos; }
	@Override
	public long getItemId(int pos) { return pos; }
}
