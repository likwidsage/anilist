package com.likwid.anilist;

import android.content.Context;
import android.view.DragEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

public class DragListener implements View.OnDragListener{
	
	Context mContext;
	listManage LM;
	
	public DragListener(Context c){
		mContext = c;
		LM = (listManage) c;
	}
	
	@Override
	public boolean onDrag(View v, DragEvent event) {
		ListView lv = (ListView) v.getParent().getParent();
		if (event.getAction() == DragEvent.ACTION_DRAG_STARTED){
			return true;
		}
		if (event.getAction() == DragEvent.ACTION_DRAG_ENTERED) {
			int startpos = Frag_MainList.dragStart;
			
			if (startpos > lv.getPositionForView(v)) { // Drag up
				v.startAnimation(AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.drag_slide_down));
			} else if ((startpos < lv.getPositionForView(v))) { // Drag down
				v.startAnimation(AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.drag_slide_up));
			}
		}
		if (event.getAction() == DragEvent.ACTION_DRAG_EXITED) {
			int startpos = Frag_MainList.dragStart;
			if (startpos > lv.getPositionForView(v)) { // Drag up
				v.startAnimation(AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.drag_exit_down));
			} else if ((startpos < lv.getPositionForView(v))) { // Drag down
				v.startAnimation(AnimationUtils.loadAnimation(mContext, com.likwid.anilist.R.anim.drag_exit_up));
			}
			
		}
		if (event.getAction() == DragEvent.ACTION_DROP) {
			int startpos = Frag_MainList.dragStart;
			int endpos = lv.getPositionForView(v);
			v.setVisibility(View.VISIBLE);
			for (int i = 0; i < lv.getChildCount(); i++) {
				lv.getChildAt(i).startAnimation(
						AnimationUtils.loadAnimation(mContext,
								com.likwid.anilist.R.anim.no_anim));
			}
			v.startAnimation(AnimationUtils.loadAnimation(
					mContext, com.likwid.anilist.R.anim.row_drop));
			Frag_MainList.dragStart = startpos;
			Frag_MainList.dragEnd = endpos;
			LM.dropProcess();
		}
		return true;
	}
	
	interface listManage{
		void dropProcess();
	}
}
