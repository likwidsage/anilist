package com.likwid.anilist;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class Frag_List_All extends Fragment {
    static String ARG_SECTION_NUMBER = "section";
    private Adapter_SQL database;
    private Adapter_List_All listAdapter;
    private ListView listView;
    ArrayList<Element> list = new ArrayList<>();
    LinearLayout llListAllMain;

    public Frag_List_All() {}

    public static Fragment newInstance(int sectionNumber) {
        Fragment fragment = new Frag_List_All();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(com.likwid.anilist.R.layout.frag_list_all, container, false);
        database = new Adapter_SQL(getActivity());
        database.open();
        list = database.getAll();
        database.close();

        listView = (ListView) view.findViewById(com.likwid.anilist.R.id.lvList);
        llListAllMain = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.fraglistall);

        list = new Sort_Elements().sortFolder(list);
        listAdapter = new Adapter_List_All(getActivity(), list);
        listView.setAdapter(listAdapter);

        return view;
    }

}
