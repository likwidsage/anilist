package com.likwid.anilist;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import static android.widget.AdapterView.OnItemClickListener;

public class Frag_Calendar_View extends Fragment implements OnItemClickListener{

    public final static String FRAG_NAME = "CalView";

    File folder = MainActivity.EXPORT_FOLDER;
    GridView gvMainCalendar;
    Adapter_SQL database;
    Adapter_Calendar calAdapter;
    MainActivity toSlide;
    SharedPreferences prefs;
    Long timeForAdapter;

    private ArrayList<Element> alarm;
    private RelativeLayout rlMainCalBG;
    public LinearLayout llAllDays;
    FragmentCalendar fc;


    final String[] CALCOL = {CalendarContract.Events._ID,
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.RRULE,
            CalendarContract.Events.RDATE
    };
    private String email;

    public Frag_Calendar_View() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(com.likwid.anilist.R.layout.frag_calendar_view, container, false);
        rlMainCalBG = (RelativeLayout) view.findViewById(com.likwid.anilist.R.id.rlCalMainBG);
        gvMainCalendar = (GridView) view.findViewById(com.likwid.anilist.R.id.gvMainCalendar);
        llAllDays = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.llAllDays);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        email = prefs.getString(getString(com.likwid.anilist.R.string.prefEmail), "");
        fc = (FragmentCalendar) getActivity();
        database = new Adapter_SQL(getActivity());
        toSlide = (MainActivity) getActivity();

        database.open();
        alarm = database.getAllAlarms();
        database.close();
        timeForAdapter = fc.getDate().getTimeInMillis();

        calAdapter = new Adapter_Calendar(getActivity(), timeForAdapter);

        gvMainCalendar.setAdapter(calAdapter);
        gvMainCalendar.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs;
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int ColorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
        Boolean colorBG = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);

        if (colorBG){
            rlMainCalBG.setBackgroundColor(getResources().getColor(ColorPrefInt));
        }else{
            rlMainCalBG.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        int colorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
        llAllDays.setBackgroundColor(getResources().getColor(colorPrefInt));
    }

    public void CalAdaptDone() {
        database.open();
        alarm = database.getAllAlarms();
        database.close();

        int numOfDays = fc.getDate().getActualMaximum(Calendar.DAY_OF_MONTH);
        Integer[] days = new Integer[numOfDays];
        for(int i = 0; i < numOfDays; i++){
            days[i] = i+1;
        }

        SetBlipDisplay setBlipDisplay = new SetBlipDisplay();
        if(setBlipDisplay.getStatus() != AsyncTask.Status.RUNNING) {
            setBlipDisplay.execute(days);
        }
    }


    protected class SetBlipDisplay extends AsyncTask<Integer, String, Integer>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Integer doInBackground(Integer... days) {
            int EventsNotFound = 0;
            ArrayList<String> blipsToShow = new ArrayList<String>();
            blipsToShow.clear();
            for (Element anAlarm : alarm) {
                Cursor c = getActivity().getContentResolver().query(CalendarContract.Events.CONTENT_URI,
                                CALCOL,
                                CalendarContract.Events._ID + "=" + anAlarm.alarm,
                                null,
                                null
                );


                //Check to see if results came and set starting and ending date, if any.
                if (c.moveToFirst()){
                    String time = c.getString(c.getColumnIndex(CalendarContract.Events.DTSTART));
                    String rule = c.getString(c.getColumnIndex(CalendarContract.Events.RRULE));
                    Calendar eventStart = Calendar.getInstance();
                    eventStart.setTimeInMillis(Long.parseLong(time));
                    String dateTag;

                    Log.d("Blips Rule", rule);

                    //Show items with end dates
                    if (rule.contains("UNTIL=")){
                        int index = rule.lastIndexOf("UNTIL=");
                        String dateString = rule.substring(index+6, index+14);           //yyyyMMdd
                        int endYear = Integer.parseInt(dateString.substring(0,4));
                        int endMonth = Integer.parseInt(dateString.substring(4, 6));
                        int endDay = Integer.parseInt(dateString.substring(6,8));
                        Calendar eventEnd = Calendar.getInstance();
                        eventEnd.set(endYear, endMonth-1, endDay);

                        //Get a list of the days between the start and end date
                        int numberOrDays = (eventEnd.get(Calendar.DAY_OF_YEAR) - eventStart.get(Calendar.DAY_OF_YEAR)) + 1;

                        for (int i = 0; i < numberOrDays; i++){
                            //Create calendar and adjust it to set tag and check dates for specific days
                            Calendar addEventCal = Calendar.getInstance();
                            addEventCal.setTimeInMillis(eventStart.getTimeInMillis());
                            addEventCal.set(Calendar.DAY_OF_YEAR, eventStart.get(Calendar.DAY_OF_YEAR) + i);
                            dateTag = anAlarm.color
                                    + addEventCal.get(Calendar.YEAR)
                                    + (addEventCal.get(Calendar.MONTH)+1)
                                    + addEventCal.get(Calendar.DAY_OF_MONTH);

                            //Check if days specified
                            if (rule.contains("FREQ=DAILY;BYDAY=")) {
                                String ruleDays = rule.substring(17);
                                if ((ruleDays.contains("SU") && addEventCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                                        || (ruleDays.contains("MO") && addEventCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
                                        || (ruleDays.contains("TU") && addEventCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
                                        || (ruleDays.contains("WE") && addEventCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
                                        || (ruleDays.contains("TH") && addEventCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
                                        || (ruleDays.contains("FR") && addEventCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
                                        || (ruleDays.contains("SA") && addEventCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                                        ) {
                                    blipsToShow.add(dateTag);
                                }
                            }else if(rule.contains("FREQ=MONTHLY")) {
                                //monthly with ending time
                                if (eventStart.get(Calendar.DAY_OF_MONTH) == addEventCal.get(Calendar.DAY_OF_MONTH)) {
                                    blipsToShow.add(dateTag);
                                }
                            }else if(rule.contains("FREQ=YEARLY")) {
                                //yearly with ending time
                                if (eventStart.get(Calendar.DAY_OF_YEAR) == addEventCal.get(Calendar.DAY_OF_YEAR)) {
                                    blipsToShow.add(dateTag);
                                }
                            }else{
                                //else days NOT specified
                                blipsToShow.add(dateTag);
                            }
                        }
                    }else{
                        //Show items that run forever starting from the event start time to
                        // the end of the month

                        //Get the amount of days in the current month
                        int daysInMonth = fc.getDate().getMaximum(Calendar.DAY_OF_MONTH);

                        //if current or future month shown
                        // if (year > eventYear) || (year == eventYear && month >= eventMonth)
                        if (fc.getDate().get(Calendar.YEAR) > eventStart.get(Calendar.YEAR)
                                || (fc.getDate().get(Calendar.YEAR) == eventStart.get(Calendar.YEAR)
                                    && fc.getDate().get(Calendar.MONTH) >= eventStart.get(Calendar.MONTH))
                        ){
                            //Set to higher number to bypass in case of no matches
                            int day = 100;
                            //If future month, set starting date to start of month
                            if (fc.getDate().get(Calendar.MONTH) > eventStart.get(Calendar.MONTH)
                                || fc.getDate().get(Calendar.YEAR) > eventStart.get(Calendar.YEAR)
                            ){
                                day = 1;
                            }else if(fc.getDate().get(Calendar.MONTH) == eventStart.get(Calendar.MONTH)
                                    && fc.getDate().get(Calendar.YEAR) == eventStart.get(Calendar.YEAR)){
                                //if current month show from beginning of the month
                                day = eventStart.get(Calendar.DAY_OF_MONTH);
                            }

                            //run from event starting day to end of the month
                            while (day <= daysInMonth){
                                //If days are specified

                                dateTag = anAlarm.color
                                        + fc.getDate().get(Calendar.YEAR)
                                        + (fc.getDate().get(Calendar.MONTH) + 1)
                                        + day;

                                //Create a calendar to check
                                Calendar ruleCheckCal = Calendar.getInstance();
                                ruleCheckCal.set(fc.getDate().get(Calendar.YEAR), fc.getDate().get(Calendar.MONTH), day);
                                if(rule.contains("FREQ=DAILY;BYDAY=")){
                                    //
                                    String ruleDays = rule.substring(17);
                                    if( (ruleCheckCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && ruleDays.contains("SU"))
                                            || (ruleCheckCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY && ruleDays.contains("MO"))
                                            || (ruleCheckCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY && ruleDays.contains("TU"))
                                            || (ruleCheckCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY && ruleDays.contains("WE"))
                                            || (ruleCheckCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY && ruleDays.contains("TH"))
                                            || (ruleCheckCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && ruleDays.contains("FR"))
                                            || (ruleCheckCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && ruleDays.contains("SA"))
                                    ){
                                        //Add blip for all from starting day to end of the month
                                        if (!blipsToShow.contains(dateTag)) {
                                            blipsToShow.add(dateTag);
                                        }
                                    }
                                }else if(rule.contains("FREQ=MONTHLY")){
                                    //Monthly without ending time
                                    if (eventStart.get(Calendar.DAY_OF_MONTH) == ruleCheckCal.get(Calendar.DAY_OF_MONTH)) {
                                        blipsToShow.add(dateTag);
                                    }
                                }else if(rule.contains("FREQ=YEARLY")) {
                                    //yearly with ending time
                                    if (eventStart.get(Calendar.DAY_OF_YEAR) == ruleCheckCal.get(Calendar.DAY_OF_YEAR)) {
                                        blipsToShow.add(dateTag);
                                    }
                                }else {
                                //If days are NOT specified
                                    dateTag = anAlarm.color
                                            + fc.getDate().get(Calendar.YEAR)
                                            + (fc.getDate().get(Calendar.MONTH) + 1)
                                            + day;

                                    //Add blip for all from starting day to end of the month
                                    if (!blipsToShow.contains(dateTag)) {
                                        blipsToShow.add(dateTag);
                                    }
                                }
                                day++;
                            }


                        }
                    }

                    for (String IdTag: blipsToShow){
                        publishProgress(IdTag);
                    }

                    fc.setBlipsShown(blipsToShow);
                }else{
                    //Cursor didn't find any matching events. Report it.
                    EventsNotFound++;
                    database.open();
                    database.alarmDone(anAlarm.id);
                    database.close();
                }
            }
            return EventsNotFound;
        }

        @Override
        protected void onProgressUpdate(String... tag) {
            if (tag[0].length() > 2) {
                ImageView ivColor = (ImageView) gvMainCalendar.findViewWithTag(tag[0]);
                if (ivColor != null) {
                    ivColor.setVisibility(View.VISIBLE);
                }
            }else{
                ImageView ivRed = (ImageView) gvMainCalendar.findViewWithTag("red" + tag[0]);
                ImageView ivBlue = (ImageView) gvMainCalendar.findViewWithTag("blue" + tag[0]);
                ImageView ivPurple = (ImageView) gvMainCalendar.findViewWithTag("purple" + tag[0]);
                ImageView ivPink = (ImageView) gvMainCalendar.findViewWithTag("pink" + tag[0]);
                ImageView ivOrange = (ImageView) gvMainCalendar.findViewWithTag("orange" + tag[0]);
                ImageView ivGreen = (ImageView) gvMainCalendar.findViewWithTag("green" + tag[0]);
                ImageView ivLime = (ImageView) gvMainCalendar.findViewWithTag("lime" + tag[0]);
                ImageView ivYellow = (ImageView) gvMainCalendar.findViewWithTag("yellow" + tag[0]);
                ImageView ivSky = (ImageView) gvMainCalendar.findViewWithTag("sky" + tag[0]);
                ImageView ivSlate = (ImageView) gvMainCalendar.findViewWithTag("slate" + tag[0]);

                if (ivBlue != null) ivBlue.setVisibility(View.GONE);
                if (ivGreen != null) ivGreen.setVisibility(View.GONE);
                if (ivLime != null) ivLime.setVisibility(View.GONE);
                if (ivOrange != null) ivOrange.setVisibility(View.GONE);
                if (ivPink != null) ivPink.setVisibility(View.GONE);
                if (ivPurple != null) ivPurple.setVisibility(View.GONE);
                if (ivRed != null) ivRed.setVisibility(View.GONE);
                if (ivSky != null) ivSky.setVisibility(View.GONE);
                if (ivSky != null) ivSlate.setVisibility(View.GONE);
                if (ivYellow != null) ivYellow.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onPostExecute(Integer notFoundCount) {
            super.onPostExecute(notFoundCount);
            if (notFoundCount >= 1) {
                toolTip("Events not found",
                        "Some events seem to be misplaced."
                                + "\nPossible reasons:"
                                + "\n1) The task was deleted from another app "
                                + "\n2) A new app that maintains the calendar was installed");
                toSlide.refreshList();
            }
        }
    }

    public void toolTip(String title, String text) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View helpView = li.inflate(com.likwid.anilist.R.layout.diag_help, null);
        TextView helpText = (TextView) helpView.findViewById(com.likwid.anilist.R.id.tvHelpText);
        Button bDismiss = (Button) helpView.findViewById(com.likwid.anilist.R.id.bDiagHelpDismis);
        helpText.setText(text);

        final Dialog diag = new Dialog(getActivity());
        diag.setTitle(title);
        diag.setContentView(helpView);
        diag.setCanceledOnTouchOutside(true);
        bDismiss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                diag.dismiss();
            }
        });

        diag.show();
    }

    public void refreshCalView(){
        timeForAdapter = fc.getDate().getTimeInMillis();
        calAdapter = new Adapter_Calendar(getActivity(), timeForAdapter);
        gvMainCalendar.setAdapter(calAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        int dayClicked = (Integer) view.getTag();
        if (fc.getDate().get(Calendar.DAY_OF_MONTH) != dayClicked){
            fc.setDay(dayClicked);
            refreshSelectionDisplay();
        }else{
            toSlide.addAlarm(0);
        }
    }

    public void refreshSelectionDisplay(){
        int color = prefs.getInt(getActivity().getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
        int colorUsed = getResources().getColor(color);
        float r = 0f;
        float[] rf = {r,r,r,r,r,r,r,r};
        Shape calBgShape = new RoundRectShape(rf, null, rf);

        //All tiles
        ShapeDrawable iconBG = new ShapeDrawable(calBgShape);
        iconBG.setIntrinsicWidth(200);
        iconBG.setIntrinsicHeight(200);
        iconBG.getPaint().setColor(colorUsed);

        ShapeDrawable shadeBG = new ShapeDrawable(calBgShape);
        shadeBG.setIntrinsicWidth(200);
        shadeBG.setIntrinsicHeight(200);
        shadeBG.getPaint().setColor(Color.parseColor("#33000000"));

        for (int j = 0; j < gvMainCalendar.getChildCount(); j++){
            gvMainCalendar.getChildAt(j).setBackground(iconBG);
            View num = gvMainCalendar.getChildAt(j).findViewById(com.likwid.anilist.R.id.tvCalAdapterNum);
            num.setBackground(shadeBG);
        }

        //Selected tile
        ShapeDrawable SelectedIconBG = new ShapeDrawable(calBgShape);
        SelectedIconBG.setIntrinsicWidth(200);
        SelectedIconBG.setIntrinsicHeight(200);
        SelectedIconBG.getPaint().setColor(Color.parseColor("#999999"));
        gvMainCalendar.findViewWithTag(fc.getDate().get(Calendar.DAY_OF_MONTH)).setBackground(SelectedIconBG);
    }

    interface FragmentCalendar{
        void resetDate();
        void setMonth(int M);
        void setYear(int Y);
        void setDay(int d);
        void setBlipsShown(ArrayList<String> blipsShown);
        Calendar getDate();
        void refreshCalendar();
    }

}
