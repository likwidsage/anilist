package com.likwid.anilist;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Adapter_CalList extends BaseAdapter implements View.OnClickListener {

    private Context ourContext;

    ArrayList<Element> values;
    int selected;
    TextView tvCalListItem, tvCalListStartTime, tvCalListEndTime, tvCalListDates;
    MainActivity toSlide;
    ArrayList<Integer> selections;
    RelativeLayout rlCalListBG;
    static int slctColor;
    private ImageView ivCalListIconBG;
    private TextView tvCalListIconLetter;

    public Adapter_CalList(Context c, ArrayList<Element> list) {
        ourContext = c;
        values = list;
        toSlide = (MainActivity) ourContext;
        selections = toSlide.getSelection();
    }

    private Long durationtoLongInMillis(String PTxHxMxS){
        Log.d("DurationToMils", PTxHxMxS);
        Long returnLong;

        //PTxHxMxS becomes xHxMxS
        String xHxMxS = PTxHxMxS.replaceFirst("P","");
        xHxMxS = xHxMxS.replace("T","");
        String passThru = xHxMxS;

        int Hour=0, Minute=0, Second=0;

        if (passThru.contains("H")) {
            Hour = Integer.parseInt(passThru.split("H")[0]);
            //xHxMxS becomes xMxS
            passThru = passThru.split("H")[1];
        }


        if (passThru.contains("M")) {
            Minute = Integer.parseInt(passThru.split("M")[0]);
            //becomes xS
            passThru = passThru.split("M")[1];
        }

        if (passThru.contains("S")) {
            Second = Integer.parseInt(passThru.split("S")[0]);
        }

        Log.d("DurationToMils", Hour+":"+Minute+":"+Second);
        returnLong = (Hour * 60L*60L*1000L) + (Minute * 60L*1000L) + (Second * 1000L);

        return returnLong;
    }

    @Override
    public View getView(int pos, View row, ViewGroup parent) {
        if (row == null) {
            LayoutInflater inflate = (LayoutInflater) ourContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflate.inflate(com.likwid.anilist.R.layout.adapter_cal_list_row, parent, false);
        }

        if (row != null) {
            tvCalListItem = (TextView) row.findViewById(com.likwid.anilist.R.id.tvCalListItem);
            tvCalListStartTime = (TextView) row.findViewById(com.likwid.anilist.R.id.tvCalListStartTime);
            tvCalListEndTime = (TextView) row.findViewById(com.likwid.anilist.R.id.tvCalListEndTime);
            tvCalListDates = (TextView) row.findViewById(com.likwid.anilist.R.id.tvCalListDates);
            rlCalListBG = (RelativeLayout) row.findViewById(com.likwid.anilist.R.id.rlCalListBG);
            ivCalListIconBG = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivCalListRowIconBg);
            tvCalListIconLetter = (TextView) row.findViewById(com.likwid.anilist.R.id.tvCalListRowIconLetter);

            Calendar tCal = Calendar.getInstance();
            tCal.setTimeInMillis(values.get(pos).alarm);
            tvCalListItem.setText(values.get(pos).title);

            final String[] CALCOL = {CalendarContract.Events._ID,
                    CalendarContract.Events.TITLE,
                    CalendarContract.Events.DTSTART,
                    CalendarContract.Events.RRULE,
                    CalendarContract.Events.DURATION
            };

            Cursor c = ourContext.getContentResolver().query(CalendarContract.Events.CONTENT_URI,
                    CALCOL,
                    CalendarContract.Events._ID + "=" + values.get(pos).alarm,
                    null,
                    null
            );
            c.moveToFirst();
            if (!c.isAfterLast()) {
                String time = c.getString(c.getColumnIndex(CalendarContract.Events.DTSTART));

                //Duration => PTxHxMxS
                String duration = c.getString(c.getColumnIndex(CalendarContract.Events.DURATION));
                String rule = c.getString(c.getColumnIndex(CalendarContract.Events.RRULE));
                Long durationLong = durationtoLongInMillis(duration);
                Long endtime = durationLong + Long.parseLong(time);

                Calendar startTime = Calendar.getInstance();
                startTime.setTimeInMillis(Long.parseLong(time));

                Calendar endTime = Calendar.getInstance();
                endTime.setTimeInMillis(endtime);

                //Set start and end TIME of event
                tvCalListStartTime.setText("Start: "+new SimpleDateFormat("hh:mm a").format(startTime.getTime()));
                tvCalListEndTime.setText("End: "+new SimpleDateFormat("hh:mm a").format(endTime.getTime()));

                //set start and end DATE of event
                String startingDate = new SimpleDateFormat("MM/dd/yyyy").format(startTime.getTime());
                String endingDate = "";
                if (rule.contains("UNTIL=")) {
                    int index = rule.lastIndexOf("UNTIL=");
                    String dateString = rule.substring(index + 6, index + 14);           //yyyyMMdd
                    int endYear = Integer.parseInt(dateString.substring(0, 4));
                    int endMonth = Integer.parseInt(dateString.substring(4, 6));
                    int endDay = Integer.parseInt(dateString.substring(6, 8));

                    endingDate = endMonth+"/"+endDay+"/"+endYear;
                }else{
                    endingDate = "∞";
                }
                tvCalListDates.setText(startingDate + " - " + endingDate);
            }
            c.close();

            //If Completed
            if (values.get(pos).completed) {
                tvCalListItem.setAlpha(.25f);
                tvCalListItem.setPaintFlags(tvCalListItem.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                tvCalListItem.setPaintFlags(tvCalListItem.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                tvCalListItem.setAlpha(1f);
            }

            //set selected color and icon color
            if (values.get(pos).color.equals("blue")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cBlue);
            } else if (values.get(pos).color.equals("red")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cRed);
            } else if (values.get(pos).color.equals("purple")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cPurple);
            } else if (values.get(pos).color.equals("pink")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cPink);
            } else if (values.get(pos).color.equals("orange")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cOrange);
            } else if (values.get(pos).color.equals("green")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cGreen);
            } else if (values.get(pos).color.equals("lime")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cLime);
            } else if (values.get(pos).color.equals("yellow")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cYellow);
            } else if (values.get(pos).color.equals("sky")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cSky);
            } else if (values.get(pos).color.equals("slate")) {
                slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cSlate);
            }

            float r = 0;
            float s = 0;
            float[] outer = {s,s,s,s,s,s,s,s};
            RectF inset = new RectF();
            float[] inner = {r,r,r,r,r,r,r,r};

            ShapeDrawable iconBG = new ShapeDrawable(new RoundRectShape(outer, null, inner));
            iconBG.setIntrinsicWidth(100);
            iconBG.setIntrinsicHeight(100);
            iconBG.getPaint().setColor(slctColor);


            ivCalListIconBG.setImageDrawable(iconBG);

            tvCalListIconLetter.setText(values.get(pos).title.substring(0,1).toUpperCase());

            row.setTag(values.get(pos).id);
            row.setOnClickListener(this);
        }


        return row;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int pos) {
        return new Element(values.get(pos).id,
                values.get(pos).title,
                values.get(pos).completed,
                values.get(pos).color,
                values.get(pos).priority,
                values.get(pos).marked,
                values.get(pos).alarm,
                values.get(pos).folder
                );
    }

    @Override
    public long getItemId(int pos) {
        return values.get(pos).id;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case com.likwid.anilist.R.id.rlCalListBG:

                Integer id = (Integer) view.getTag();
                Adapter_SQL database = new Adapter_SQL(ourContext);
                database.open();
                String IDpath = database.getPath(id) + id + "/";
                database.close();
                String[] paths = IDpath.split("/");
                for (String path : paths) {
                    int pathid = Integer.parseInt(path);
                    if (pathid > 0) {
                        toSlide.goInto(pathid);
                    }
                }

                break;
        }
    }
}