package com.likwid.anilist;

import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Widget_Add extends Activity implements View.OnClickListener {

    private Button bWAdd;
    private EditText etWAdd;
    private Adapter_SQL database;
    private ImageView ivBlue, ivGreen, ivLime, ivOrange, ivPink, ivPurple, ivRed, ivSky, ivSlate, ivYellow;
    private LinearLayout llBlue, llGreen, llLime, llOrange, llPink, llPurple, llRed, llSky, llSlate, llYellow;
    private String color = "blue";
    private int listID = 0;
    private boolean addToList = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.likwid.anilist.R.layout.widget_add);

        bWAdd = (Button) findViewById(com.likwid.anilist.R.id.bEnter);
        etWAdd = (EditText) findViewById(com.likwid.anilist.R.id.etWAdd);

        ivBlue = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckBlue);
        ivGreen = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckGreen);
        ivLime = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckLime);
        ivOrange = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckOrange);
        ivPink = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckPink);
        ivPurple = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckPurple);
        ivRed = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckRed);
        ivSky = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckSky);
        ivSlate = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckSlate);
        ivYellow = (ImageView) findViewById(com.likwid.anilist.R.id.ivCheckYellow);

        llBlue = (LinearLayout) findViewById(com.likwid.anilist.R.id.llBlue);
        llGreen = (LinearLayout) findViewById(com.likwid.anilist.R.id.llGreen);
        llLime = (LinearLayout) findViewById(com.likwid.anilist.R.id.llLime);
        llOrange = (LinearLayout) findViewById(com.likwid.anilist.R.id.llOrange);
        llPink = (LinearLayout) findViewById(com.likwid.anilist.R.id.llPink);
        llPurple = (LinearLayout) findViewById(com.likwid.anilist.R.id.llPurple);
        llSky = (LinearLayout) findViewById(com.likwid.anilist.R.id.llSky);
        llRed = (LinearLayout) findViewById(com.likwid.anilist.R.id.llRed);
        llSlate = (LinearLayout) findViewById(com.likwid.anilist.R.id.llSlate);
        llYellow = (LinearLayout) findViewById(com.likwid.anilist.R.id.llYellow);

        bWAdd.setOnClickListener(this);
        llBlue.setOnClickListener(this);
        llGreen.setOnClickListener(this);
        llLime.setOnClickListener(this);
        llOrange.setOnClickListener(this);
        llPink.setOnClickListener(this);
        llPurple.setOnClickListener(this);
        llSky.setOnClickListener(this);
        llSlate.setOnClickListener(this);
        llRed.setOnClickListener(this);
        llYellow.setOnClickListener(this);

        updateColor(color);

        Intent intent = getIntent();
        addToList = intent.getBooleanExtra(WidgetProvider.ADD_TO_LIST, false);

        int wID = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        listID  = 0;

        if (addToList) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            listID = prefs.getInt(wID + WidgetProvider.ID_FOR_WIDGET, 0);
        }

        database = new Adapter_SQL(getApplication());
        database.open();
        String parentName;
        if (listID > 0 && database.itemExists(listID)) {
            //Check if ID is greater then 0 and if it exists
            Adapter_SQL database = new Adapter_SQL(this);
            database.open();
            parentName = database.getName(listID);
            if (parentName.length() > 10) parentName = parentName.substring(0, 10) + "...";
            database.close();
            bWAdd.setText("Add to '" + parentName + "'");
        }else if (listID <= 0) {
            //do nothing if ID is the main list which is 0
        }else{
            //Id is greater then 0 but doesn't exist
            AlertDialog.Builder notExist = new AlertDialog.Builder(this);
            notExist.setMessage("This item no longer exists. ")
                    .setTitle("Item not found").show();
            listID = 0;
        }
        database.close();


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case com.likwid.anilist.R.id.bEnter:
                String itemName = etWAdd.getText().toString();
                if (itemName.contains(MainActivity.HEADER) ||
                        itemName.contains(MainActivity.STARTER) ||
                        itemName.contains(MainActivity.SEPERATOR) ||
                        itemName.contains(MainActivity.FOOTER))
                {
                    Toast.makeText(getApplicationContext(), "Text can not contain '"
                            + MainActivity.HEADER + "', '"
                            + MainActivity.STARTER + "', '"
                            + MainActivity.SEPERATOR + "' or '"
                            + MainActivity.FOOTER + "'", Toast.LENGTH_SHORT).show();
                }else{
                    insertTasks(itemName);
                }
                break;
            case com.likwid.anilist.R.id.llBlue:
                color = updateColor("blue");
                break;
            case com.likwid.anilist.R.id.llGreen:
                color = updateColor("green");
                break;
            case com.likwid.anilist.R.id.llLime:
                color = updateColor("lime");
                break;
            case com.likwid.anilist.R.id.llPink:
                color = updateColor("pink");
                break;
            case com.likwid.anilist.R.id.llOrange:
                color = updateColor("orange");
                break;
            case com.likwid.anilist.R.id.llRed:
                color = updateColor("red");
                break;
            case com.likwid.anilist.R.id.llSky:
                color = updateColor("sky");
                break;
            case com.likwid.anilist.R.id.llSlate:
                color = updateColor("slate");
                break;
            case com.likwid.anilist.R.id.llYellow:
                color = updateColor("yellow");
                break;
            case com.likwid.anilist.R.id.llPurple:
                color = updateColor("purple");
                break;
        }
    }

    private void insertTasks(String text){
        int tasksCreated = 0;
        database = new Adapter_SQL(getApplicationContext());
        database.open();

        String[] tasks = text.trim().split("\n");
        for (String task : tasks){
            if ( database.createElement(task.trim(), color, 0, database.getPath(listID)) ) {
                tasksCreated++;
            }
        }
        database.close();
        Toast.makeText(getApplicationContext(), "Added "+tasksCreated+" tasks!", Toast.LENGTH_SHORT).show();

        AppWidgetManager awm = AppWidgetManager.getInstance(this);
        int[] ids = awm.getAppWidgetIds(new ComponentName(this, WidgetProvider.class));
        awm.notifyAppWidgetViewDataChanged(ids, com.likwid.anilist.R.id.wList);
        etWAdd.setText("");
        finish();
    }

    //Blue Green Lime Orange Pink Purple Red Sky Slate Yellow;
    private String updateColor(String color){
        ivBlue.setVisibility(View.GONE);
        ivGreen.setVisibility(View.GONE);
        ivLime.setVisibility(View.GONE);
        ivOrange.setVisibility(View.GONE);
        ivPink.setVisibility(View.GONE);
        ivPurple.setVisibility(View.GONE);
        ivRed.setVisibility(View.GONE);
        ivSky.setVisibility(View.GONE);
        ivSlate.setVisibility(View.GONE);
        ivYellow.setVisibility(View.GONE);

        switch (color) {
            case "blue":
                ivBlue.setVisibility(View.VISIBLE);
                break;
            case "green":
                ivGreen.setVisibility(View.VISIBLE);
                break;
            case "lime":
                ivLime.setVisibility(View.VISIBLE);
                break;
            case "orange":
                ivOrange.setVisibility(View.VISIBLE);
                break;
            case "pink":
                ivPink.setVisibility(View.VISIBLE);
                break;
            case "purple":
                ivPurple.setVisibility(View.VISIBLE);
                break;
            case "red":
                ivRed.setVisibility(View.VISIBLE);
                break;
            case "sky":
                ivSky.setVisibility(View.VISIBLE);
                break;
            case "slate":
                ivSlate.setVisibility(View.VISIBLE);
                break;
            case "yellow":
                ivYellow.setVisibility(View.VISIBLE);
                break;
        }

        return color;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(com.likwid.anilist.R.anim.fade_in, com.likwid.anilist.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
