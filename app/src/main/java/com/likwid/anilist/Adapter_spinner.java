package com.likwid.anilist;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_spinner extends BaseAdapter{
	
	private Context ourContext;
	LayoutInflater inflate;
	ArrayList<String> values;
	String text, bg;
	
	public Adapter_spinner(Context c, ArrayList<String> list, String colorHex, String bgHex){
		ourContext = c;
		values = list;
		text = colorHex;
		bg = bgHex;
	}

	@Override
	public View getView(int pos, View convert, ViewGroup parent) {
		View spinner = convert;
		if (spinner == null){
			inflate = (LayoutInflater) ourContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			spinner = inflate.inflate(com.likwid.anilist.R.layout.adapter_spinner_item, parent, false);
		}
		
		TextView spinnerText = (TextView) spinner.findViewById(com.likwid.anilist.R.id.SpinnerText);
		LinearLayout layout = (LinearLayout) spinner.findViewById(com.likwid.anilist.R.id.SpinnerBG);
		
		spinnerText.setText(values.get(pos));
		spinnerText.setTextColor(Color.parseColor(text));
		layout.setBackgroundColor(Color.parseColor(bg));
		
		return spinner; 
	}

	@Override
	public int getCount() { return values.size(); }
	@Override
	public Object getItem(int pos) { return pos; }
	@Override
	public long getItemId(int pos) {return pos; }
}