package com.likwid.anilist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Adapter_Photos extends BaseAdapter {
	
	static final int filesize = 125;
	static final File folder = MainActivity.PHOTOS_FOLDER;
	ArrayList<Element> PhotoList;
	Context ourContext;
	ImageView ivPhotoItem;
	ExifInterface exin;

	Adapter_Photos(Context c, ArrayList<Element> list) {
		PhotoList = list;
		ourContext = c;
	}

	@Override
	public View getView(int pos, View convert, ViewGroup parent) {
		View row = convert;
		if (row == null) {
			LayoutInflater inflate = (LayoutInflater) ourContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflate.inflate(com.likwid.anilist.R.layout.adapter_photo_item, parent, false);
		}
		ivPhotoItem = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivPhotoItem);
		
		try {
			exin = new ExifInterface(folder + "/"	+ PhotoList.get(pos).title);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Bitmap fullpic = decodeFile(new File(folder + "/"	+ PhotoList.get(pos).title), filesize);
		int rotation = exin.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
		Matrix matrix = new Matrix();
		switch(rotation) {
	    case ExifInterface.ORIENTATION_ROTATE_90:
	    	matrix.postRotate(90);
	        break;
	    case ExifInterface.ORIENTATION_ROTATE_180:
	    	matrix.postRotate(180);
	        break;
	    case ExifInterface.ORIENTATION_ROTATE_270:
	    	matrix.postRotate(270);
	        break;
		}
		
		Bitmap pic = Bitmap.createBitmap(fullpic, 0, 0, fullpic.getWidth(), fullpic.getHeight(), matrix, true);
		
		ivPhotoItem.setImageBitmap(pic);
		return row;
	}

	private Bitmap decodeFile(File f, int size){
	    try {
	        //Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f),null,o);

	        //The new size we want to scale to

            //Find the correct scale value. It should be the power of 2.
	        int scale=1;
	        while(o.outWidth/scale/2>= size && o.outHeight/scale/2>= size)
	            scale*=2;

	        //Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
	    return null;
	}
	
	@Override
	public int getCount() {
		return PhotoList.size();
	}

	@Override
	public Object getItem(int pos) {
		return pos;
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}
}
