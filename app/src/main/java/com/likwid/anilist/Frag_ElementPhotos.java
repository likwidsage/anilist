package com.likwid.anilist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class Frag_ElementPhotos extends Fragment implements OnItemClickListener, OnItemLongClickListener{

	static final File folder = MainActivity.PHOTOS_FOLDER;
	static final String PHOTO_ID = "photo_id";
	int photoID;
	ArrayList<Element> photoList;
	GridView gvPhotos;
	Adapter_Photos adapter;
	Adapter_SQL database;
	TextView noGrid;
    TextView tvTitle;
	LinearLayout MainBG;

	static String ARG_SECTION_NUMBER = "section_arg";

	public Frag_ElementPhotos(){}

	public static Fragment newInstance(int sectionNumber) {
		Fragment fragment = new Frag_ElementPhotos();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		View view  = inflater.inflate(com.likwid.anilist.R.layout.frag_element_photos, container, false);
		gvPhotos = (GridView) view.findViewById(com.likwid.anilist.R.id.gv_Photos);
		tvTitle = (TextView) view.findViewById(com.likwid.anilist.R.id.tvPhotoTitle);
		noGrid = (TextView) view.findViewById(com.likwid.anilist.R.id.tvPhotosNoGrid);
		MainBG = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.loPhoto_Main);

		gvPhotos.setOnItemClickListener(this);
		gvPhotos.setOnItemLongClickListener(this);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
        database = new Adapter_SQL(getActivity());

        photoID = getArguments().getInt(PHOTO_ID);
        setGrid(photoID);
	}
	
	public void setGrid(int id){
		database.open();
		photoList = database.getPhotos(id);
		tvTitle.setText("Photos of '"+ database.getName(id) + "'");
		database.close();
		
		if (photoList.size() > 0){
			noGrid.setVisibility(View.GONE);
		}else{
			noGrid.setVisibility(View.VISIBLE);
		}
        adapter = new Adapter_Photos(getActivity(), photoList);
        gvPhotos.setAdapter(adapter);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int colorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
		Boolean ColorBG = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);
		
		tvTitle.setBackgroundColor(getResources().getColor(colorPrefInt));
		
		if (ColorBG){
			MainBG.setBackgroundColor(getResources().getColor(colorPrefInt));
		}else{
			MainBG.setBackgroundColor(Color.WHITE);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		Toast.makeText(getActivity(), ""+photoList.get(pos).title, Toast.LENGTH_SHORT).show();
		Intent intent = new Intent("com.likwid.orgalist.ACTIVITY_FULLPHOTO");
		intent.putExtra("fileName", photoList.get(pos).title);
		
		startActivity(intent);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos,	long arg3) {
		final int id = photoList.get(pos).id;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		final String name = photoList.get(pos).title;
		builder.setTitle("Delete '" + name + "' ?");
		//Delete
		builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				File photo = new File(folder, name);
				if (photo.exists()){
					photo.delete();

					database.open();
					database.deleteItem(id);
					database.close();
					setGrid(photoID);
				}else{
					Toast.makeText(getActivity(), "Couldn't locate file", Toast.LENGTH_SHORT).show();
				}
			}
		});
		builder.setNegativeButton("Cancel", null);
		builder.create();
		builder.show();
		return true;
	}
	

}
