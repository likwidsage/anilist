package com.likwid.anilist;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class Adapter_SQL {

	private static final String DATABASE_NAME = "Elements";
	private static final String TABLE_NAME = "Elements";
	private static final String ROW_ID = "_id";
	private static final String ROW_TITLE = "title";
	private static final String ROW_COMPLETED = "completed";
	private static final String ROW_TYPE = "type";
	private static final String ROW_ALARM = "alarms";
	private static final String ROW_FOLDER = "path";
	private static final String ROW_COLOR = "color";
	private static final String ROW_PRIORITY = "priority";
	private static final String ROW_DELETE = "mark_delete";

	private static final String TYPE_PHOTO = "photo";
	private static final String TYPE_NOTE = "note";
	private static final String TYPE_ITEM = "element";

	private static final int DATABASE_VERSION = 5;

    private final Context ourContext;
	private SQLiteDatabase ourDatabase;

	private static class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + TABLE_NAME + " (" 
					+ ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ ROW_TITLE	+ " TEXT NOT NULL, " 
					+ ROW_COMPLETED + " INTEGER,"
					+ ROW_ALARM + " INTEGER, "
					+ ROW_TYPE + " TEXT, "
					+ ROW_FOLDER + " TEXT, "
					+ ROW_COLOR + " TEXT, "
					+ ROW_PRIORITY + " INTEGER, "
                    + ROW_DELETE + " INTEGER "
					+ ");"
            );
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d("onUpgrade", oldVersion + " => " + newVersion);
			switch (oldVersion){
                case 3:
                case 4:
                    db.execSQL("ALTER TABLE " + TABLE_NAME  + " ADD COLUMN " + ROW_DELETE + " INTEGER DEFAULT 0");
                    break;

            }
		}
	}

	public Adapter_SQL(Context c) {
		ourContext = c;
	}

	public void open() {
        DbHelper ourHelper = new DbHelper(ourContext);
		ourDatabase = ourHelper.getWritableDatabase();
	}

	public void close() {
		ourDatabase.close();
	}

	private void updateWidget(){
		AppWidgetManager awm = AppWidgetManager.getInstance(ourContext);
		int[] ids = awm.getAppWidgetIds(new ComponentName(ourContext, WidgetProvider.class));
		awm.notifyAppWidgetViewDataChanged(ids, com.likwid.anilist.R.id.wList);
	}

	// Adds Element to database, checks to see if last item is created/////////////////////////////////
	public Boolean createElement(String name, String color, int priority, String folder) {
        int lastNum = getCount();
        if (name.contains(MainActivity.HEADER)
                || name.contains(MainActivity.STARTER)
                || name.contains(MainActivity.SEPERATOR)
                || name.contains(MainActivity.FOOTER))
		{
            Toast.makeText(ourContext, "Text can not contain '"
                    + MainActivity.HEADER + "', '"
                    + MainActivity.STARTER + "', '"
                    + MainActivity.SEPERATOR + "' or '"
                    + MainActivity.FOOTER + "'", Toast.LENGTH_SHORT).show();
            return false;
        }else if(name.length() <= 0){
				Toast.makeText(ourContext, "Please enter a name.", Toast.LENGTH_SHORT).show();
				return false;
		}else{
			ContentValues cv = new ContentValues();
			cv.put(ROW_TITLE, name);
			cv.put(ROW_TYPE, TYPE_ITEM);
			cv.put(ROW_FOLDER, folder);
			cv.put(ROW_COLOR, color);
			cv.put(ROW_PRIORITY, priority);
			cv.put(ROW_DELETE, 0);

			ourDatabase.insert(TABLE_NAME, null, cv);

			//if entry added, return true.
			if (getCount() == lastNum + 1) {
				updateWidget();
				return true;
			} else {
				Toast.makeText(ourContext, "A serious error has occured.\nItem was not created.", Toast.LENGTH_SHORT).show();
				return false;
			}
		}
	}

    public void renameElement(String name, int id) {
        ContentValues cv = new ContentValues();
        cv.put(ROW_TITLE, name);
        ourDatabase.update(TABLE_NAME, cv, ROW_ID + "=" + id, null);
    }

    public ArrayList<Element> getAll(){
        ArrayList<Element> allChildren = new ArrayList<Element>();
        Cursor c = ourDatabase.query(TABLE_NAME, null, ROW_TYPE + "='" + TYPE_ITEM + "'", null, null, null, null);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
			Boolean marked = (c.getInt(c.getColumnIndex(ROW_DELETE)) == 1);
            allChildren.add(new Element(
                    c.getInt(c.getColumnIndex(ROW_ID)),
                    c.getString(c.getColumnIndex(ROW_TITLE)),
                    c.getInt(c.getColumnIndex(ROW_COMPLETED)) == 1,
                    c.getString(c.getColumnIndex(ROW_COLOR)),
                    c.getInt(c.getColumnIndex(ROW_PRIORITY)),
                    marked,
                    c.getInt(c.getColumnIndex(ROW_ALARM)),
                    c.getString(c.getColumnIndex(ROW_FOLDER))
            ));
        }
        c.close();
        return allChildren;
    }
	
	public ArrayList<Element> getAllChildren(int parent, String path){
		ArrayList<Element> allChildren = new ArrayList<Element>();
        path = path + parent + "/";
		Cursor c = ourDatabase.query(TABLE_NAME, null, ROW_FOLDER + " LIKE '" + path + "%'", null, null, null, null);
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
			Boolean marked = (c.getInt(c.getColumnIndex(ROW_DELETE)) == 1);
			allChildren.add(new Element(
					c.getInt(c.getColumnIndex(ROW_ID)), 
					c.getString(c.getColumnIndex(ROW_TITLE)), 
					c.getInt(c.getColumnIndex(ROW_COMPLETED)) == 1,
					c.getString(c.getColumnIndex(ROW_COLOR)),
					c.getInt(c.getColumnIndex(ROW_PRIORITY)),
					marked,
					c.getInt(c.getColumnIndex(ROW_ALARM)),
					c.getString(c.getColumnIndex(ROW_FOLDER))
					));
		}
        c.close();
		return allChildren;
	}
	
	public String getType(int id){
		String[] columns = { ROW_ID, ROW_TYPE };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID +"="+ id, null, null, null, null);
		c.moveToFirst();
		String type = c.getString(c.getColumnIndex(ROW_TYPE));
        c.close();
        return type;
	}
	
	public int getLevel(int id){
        if (id <= 0){
            return 0;
        }
		String[] columns = { ROW_ID, ROW_FOLDER };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID +"="+ id, null, null, null, null);
        String[] count = {};
        if (!c.isAfterLast()){
            c.moveToFirst();
            String path = c.getString(c.getColumnIndex(ROW_FOLDER));
            count = path.split("/");
            c.close();
        }

        if (count.length > 0){
            return count.length;
        }else{
            return -1;
        }

	}

    //Returns a string containing the full path to the item (including itself)
	public String getPath(int id){
        if (id <= 0) return "0/";
        String path = null;
		String[] columns = { ROW_ID, ROW_FOLDER };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID +"="+ id, null, null, null, null);
		if (!c.isAfterLast()){
            c.moveToFirst();
            path = c.getString(c.getColumnIndex(ROW_FOLDER));
        }
        c.close();
		return path+id+"/";
	}

	// Gets single element i ///////////////////////
	public Element getSingle(int i) {
		Element single = null;
		Cursor c = ourDatabase.query(TABLE_NAME, null, ROW_ID +"="+ i + " AND " + ROW_TYPE + "='"+TYPE_ITEM+"'", null, null,	null, null);
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			Boolean marked = (c.getInt(c.getColumnIndex(ROW_DELETE)) == 1);
			single = new Element(
					c.getInt(c.getColumnIndex(ROW_ID)), 
					c.getString(c.getColumnIndex(ROW_TITLE)), 
					c.getInt(c.getColumnIndex(ROW_COMPLETED)) == 1,
					c.getString(c.getColumnIndex(ROW_COLOR)),
					c.getInt(c.getColumnIndex(ROW_PRIORITY)),
					marked,
					c.getInt(c.getColumnIndex(ROW_ALARM)),
					c.getString(c.getColumnIndex(ROW_FOLDER))
					);
		}
		c.close();
		return single;
	}

    public Boolean getStatus(int id){
        String[] columns = { ROW_ID, ROW_COMPLETED};
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID +"="+ id, null, null,	null, null);
        c.moveToFirst();
        int status = c.getInt(c.getColumnIndex(ROW_COMPLETED));

        return (status > 0);
    }

    // Checks to see if ID exists
    public Boolean itemExists(int id){
        String[] columns = { ROW_ID };
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID +"="+ id + " AND " + ROW_TYPE + "='"+TYPE_ITEM+"'", null, null,	null, null);
        c.moveToFirst();
        if (c.isAfterLast()){
            return false;
        }else{
            return true;
        }
    }
	
	// Gets name of single element i ///////////////////////
		public String getName(int i) {
            if (i <= 0) return "main list";
			String single = null;
			String[] columns = { ROW_ID, ROW_TITLE};
			Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + i + " AND " + ROW_TYPE + "='" + TYPE_ITEM + "'", null, null, null, null);
			c.moveToFirst();
            if (!c.isAfterLast())
			    single = c.getString(c.getColumnIndex(ROW_TITLE));
			c.close();
			return single;
		}

	// Changes completion status of i //////////////////
	public void switchComplete(int i) {
		String[] columns = { ROW_COMPLETED, ROW_TITLE };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + i,	null, null, null, null);
		c.moveToFirst();
		ContentValues cv = new ContentValues();
		if (c.getInt(c.getColumnIndex(ROW_COMPLETED)) == 1) {
			cv.put(ROW_COMPLETED, 0);
		} else {
			cv.put(ROW_COMPLETED, 1);
		}
		c.close();
		ourDatabase.update(TABLE_NAME, cv, ROW_ID + "=" + i, null);

        AppWidgetManager awm = AppWidgetManager.getInstance(ourContext);
        int[] ids = awm.getAppWidgetIds(new ComponentName(ourContext, WidgetProvider.class));
        awm.notifyAppWidgetViewDataChanged(ids, com.likwid.anilist.R.id.wList);
	}
	
	// Changes priority of i //////////////////
	public void setPriority(int id, int priority) {

		String[] columns = { ROW_PRIORITY };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + id,	null, null, null, null);
		c.moveToFirst();
		ContentValues cv = new ContentValues();
		cv.put(ROW_PRIORITY, priority);
		c.close();
		ourDatabase.update(TABLE_NAME, cv, ROW_ID + "=" + id, null);
	}
	
	//Changes color of element
	public void changeColor(int id, String color){
		String[] columns = { ROW_COLOR };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + id,	null, null, null, null);
		c.moveToFirst();
		ContentValues cv = new ContentValues();
		cv.put(ROW_COLOR, color);
		c.close();
		ourDatabase.update(TABLE_NAME, cv, ROW_ID + "=" + id, null);
	}
	
	//gets parent of ID
	public int getParent(int id){
			String[] columns = { ROW_FOLDER };
			Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + id,	null, null, null, null);
			c.moveToFirst();
			String folder = c.getString(c.getColumnIndex(ROW_FOLDER));
            c.close();
            return folderToId(folder);
	}

    private Integer folderToId(String folder){
        String[] pid = folder.split("/");
        return Integer.parseInt(pid[pid.length - 1]);
    }
	
	//Changes parent of ID | 0/1/2/3/4/5 => move *3* from 2 to *1* => 0/1/3/4/5
	public void changeParent(int id, int newPid){
        String[] columns = { ROW_FOLDER};
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + id,	null, null, null, null);
        c.moveToFirst();
        ContentValues cv = new ContentValues();
        c.close();

        //get all items that contain "0/1/2/3/" [ie its children]
        ArrayList<Element> children = getAllChildren(id, getPath(id));

        //Use "0/1/2/3/" as the starting point in the childrens' path
        int startingPoint = getPath(id).length();

        //Create the new path to assign to all the children ie "0/1/3/"
        String newParentPath = "0/";
        if (newPid > 0) {
            newParentPath = getPath(newPid) + id + "/";
        }

        //For each child, replace the old path and main_add the newly created one
        //ie: from "0/1/2/3/4/5" replace "0/1/2/3/" with "0/1/3/"
        for (Element aChildren : children) {
            String newPath = newParentPath + aChildren.folder.substring(startingPoint);
            cv.put(ROW_FOLDER, newPath);
            ourDatabase.update(TABLE_NAME, cv, ROW_ID + "=" + aChildren.id, null);
        }

        //Then change the item itself
        cv.put(ROW_FOLDER, getPath(newPid));
        ourDatabase.update(TABLE_NAME, cv, ROW_ID + "=" + id, null);
	}

	// Deletes a single Element RECURSIVELY and its' alarm////////////
	public void deleteElement(int i) {
		int c = i;
		Element single = getSingle(i);
		while (getChildOf(i) > 0) {
			if (getChildOf(c) > 0) {
				c = getChildOf(c);
			} else {
                //Sync the deletion of the elements
                Element deleteCalOf = getSingle(c);

                if (deleteCalOf != null) {
                    Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, deleteCalOf.alarm);
                    ourContext.getContentResolver().delete(deleteUri, null, null);
                }

                ourDatabase.delete(TABLE_NAME, ROW_ID + "=" + c, null);
				c = i;
			}
		}

        Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, single.alarm);
        ourContext.getContentResolver().delete(deleteUri, null, null);
		ourDatabase.delete(TABLE_NAME, ROW_ID + "=" + i, null);
		updateWidget();
	}

    // Returns number of children Elements
    public int getChildCount(int i) {
        String[] columns = { ROW_FOLDER };
        String[] args = { TYPE_ITEM, getPath(i) };
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_TYPE + "=? AND " + ROW_FOLDER + " LIKE ?", args, null, null, null);

        int count = c.getCount();
        c.close();
        return count;
    }

    // Returns number of children
    public int getCompletedChildCount(int i) {
        String[] columns = { ROW_FOLDER };
        String[] args = { "1", getPath(i) };
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_COMPLETED + "=? AND " + ROW_FOLDER + " LIKE ?", args, null, null, null);
        int count = c.getCount();
        c.close();
        return count;
    }

	// Returns ID of Element with a pid of i (Used for recursive delete)
	public int getChildOf(int i) {
		int child = 0;

		String[] columns = { ROW_ID, ROW_FOLDER };
        String path = getPath(i);
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_FOLDER + " LIKE '" + path + "'", null, null, null, null);

		if (c.getCount() > 0) {
			c.moveToFirst();
			child = c.getInt(0);
		}
		c.close();
		return child;
	}

	// Returns ALL Direct elements of i ///////////////////
	public ArrayList<Element> getChildren(int i) {
		ArrayList<Element> childrenList = new ArrayList<Element>();
        String path = getPath(i);
        Log.d("getChildren", i +"  "+ path);

		Cursor c = ourDatabase.query(TABLE_NAME, null, ROW_FOLDER + " LIKE '" + path + "' AND " + ROW_TYPE + "= 'element' ", null, null, null, null);
        Log.d("getChildrenCount", c.getCount()+"");

		if (c.getCount() > 0) {
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				Boolean marked = (c.getInt(c.getColumnIndex(ROW_DELETE)) == 1);
				childrenList.add(new Element(
						c.getInt(c.getColumnIndex(ROW_ID)), 
						c.getString(c.getColumnIndex(ROW_TITLE)), 
						c.getInt(c.getColumnIndex(ROW_COMPLETED)) == 1,
						c.getString(c.getColumnIndex(ROW_COLOR)),
						c.getInt(c.getColumnIndex(ROW_PRIORITY)),
						marked,
						c.getInt(c.getColumnIndex(ROW_ALARM)),
						c.getString(c.getColumnIndex(ROW_FOLDER))
						));
			}
		}
		c.close();
		return childrenList;
	}
	
	public int getCount(){
		String[] columns = { ROW_ID };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, null, null, null, null, null);
		int count = c.getCount();
		c.close();
		return count;
	}
	
	public Element getLast(){
		Element result;
		Cursor c = ourDatabase.query(TABLE_NAME, null, null, null, null, null, null);
		c.moveToLast();
		Boolean marked = (c.getInt(c.getColumnIndex(ROW_DELETE)) == 1);
		result = new Element(
				c.getInt(c.getColumnIndex(ROW_ID)), 
				c.getString(c.getColumnIndex(ROW_TITLE)), 
				c.getInt(c.getColumnIndex(ROW_COMPLETED)) == 1,
				c.getString(c.getColumnIndex(ROW_COLOR)),
				c.getInt(c.getColumnIndex(ROW_PRIORITY)),
				marked,
				c.getInt(c.getColumnIndex(ROW_ALARM)),
				c.getString(c.getColumnIndex(ROW_FOLDER))
				);
		c.close();
		return result;
	}

	// Create a note [Title = text] //////////////////////////////
	public void addNote(String name, int pid) {
		ContentValues cv = new ContentValues();
		cv.put(ROW_TITLE, name);
		cv.put(ROW_TYPE, TYPE_NOTE);
		if (pid <= 0){ 
			cv.put(ROW_FOLDER, "0/");		        //id: 		1  		2		3
		}else if (pid > 0){					        //order:	1st		sub		sub-sub
			String path = getPath(pid);             //path:		0/  	0/1/	0/1/2/
			cv.put(ROW_FOLDER, path);
		}

        Toast.makeText(ourContext, "Note added.", Toast.LENGTH_LONG).show();

		ourDatabase.insert(TABLE_NAME, null, cv);
	}

    public boolean hasNotes(int i) {
        Boolean hasNotes = false;

        String[] columns = { ROW_ID };
        String[] args = {TYPE_NOTE, getPath(i) };
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_TYPE + "=? AND " + ROW_FOLDER + " LIKE ?", args, null, null, null);

        if (c.getCount() > 0) {
            hasNotes = true;
        }
        c.close();
        return hasNotes;
    }

	// Returns notes with a parent ID if i /////////////////////////
	public ArrayList<Element> getChildNotes(int i) {
		ArrayList<Element> notes = new ArrayList<Element>();

		String[] columns = { ROW_ID, ROW_TITLE, ROW_COMPLETED, ROW_TYPE, ROW_COLOR, ROW_PRIORITY, ROW_DELETE, ROW_ALARM, ROW_FOLDER};
		String[] args = {TYPE_NOTE, getPath(i) };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_TYPE + "=? AND " + ROW_FOLDER + " LIKE ?", args, null, null, null);

		if (c.getCount() > 0) {
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				notes.add(new Element(
						c.getInt(c.getColumnIndex(ROW_ID)),
						c.getString(c.getColumnIndex(ROW_TITLE)),
						c.getInt(c.getColumnIndex(ROW_COMPLETED))==1,
						c.getString(c.getColumnIndex(ROW_COLOR)),
						c.getInt(c.getColumnIndex(ROW_PRIORITY)),
                        c.getInt(c.getColumnIndex(ROW_DELETE)) == 1,
						c.getInt(c.getColumnIndex(ROW_ALARM)),
						c.getString(c.getColumnIndex(ROW_FOLDER))
						));
			}
		}
		c.close();
		return notes;
	}

	// Deletes a single item with a ID of i. Used for deleting photos and notes from DB
	public void deleteItem(int i) {
        String[] columns = {ROW_ID, ROW_TYPE};
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID+"="+i, null, null, null, null);

		ourDatabase.delete(TABLE_NAME, ROW_ID + "=" + i, null);
		Toast.makeText(ourContext, "Deleted.",	Toast.LENGTH_SHORT).show();
	}


	// Returns single note with ID of i /////////////////////////
	public ArrayList<Element> getSingleNote(int i) {
		ArrayList<Element> notes = new ArrayList<Element>();

		String[] columns = { ROW_ID, ROW_TITLE, ROW_COMPLETED, ROW_TYPE, ROW_COLOR, ROW_PRIORITY, ROW_DELETE };
		String[] args = { TYPE_NOTE, ""+i };
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_TYPE + "=? AND "
				+ ROW_ID + "=?", args, null, null, null);

		if (c.getCount() > 0) {
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				Boolean marked = (c.getInt(c.getColumnIndex(ROW_DELETE)) == 1);
				notes.add(new Element(
						c.getInt(c.getColumnIndex(ROW_ID)), 
						c.getString(c.getColumnIndex(ROW_TITLE)), 
						c.getInt(c.getColumnIndex(ROW_COMPLETED))==1,
						c.getString(c.getColumnIndex(ROW_COLOR)),
						c.getInt(c.getColumnIndex(ROW_PRIORITY)),
						marked,
						c.getInt(c.getColumnIndex(ROW_ALARM)),
						c.getString(c.getColumnIndex(ROW_FOLDER))
						));
			}
		}
		c.close();
		return notes;
	}
	
	/////////// Photos /////////////////////////////////////////
	
	//Adds a photo to element pid [Title = filename]
	public void addPhoto(String name, int pid) {
		ContentValues cv = new ContentValues();
		cv.put(ROW_TITLE, name);
		cv.put(ROW_FOLDER, getPath(pid));
		cv.put(ROW_TYPE, TYPE_PHOTO);
		ourDatabase.insert(TABLE_NAME, null, cv);
	}

	public Boolean hasPhotos(int id) {
		Boolean hasPhoto = false;

		String[] columns = {ROW_ID};
		String[] args = {TYPE_PHOTO, getPath(id)};
		Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_TYPE + "=? AND " + ROW_FOLDER + "=?", args, null, null, null);

		if (c.getCount() > 0) {
			hasPhoto = true;
		}
        c.close();
        return hasPhoto;
	}
	
	// Returns photos under a specific id /////////////////////////
    public ArrayList<Element> getPhotos(int i) {
        ArrayList<Element> photos = new ArrayList<Element>();

        String[] columns = { ROW_ID, ROW_TITLE, ROW_COMPLETED, ROW_TYPE, ROW_COLOR, ROW_PRIORITY, ROW_DELETE, ROW_ALARM, ROW_FOLDER };
        String[] args = { TYPE_PHOTO, getPath(i) };
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_TYPE + "=? AND " + ROW_FOLDER + "=?", args, null, null, null);

        if (c.getCount() > 0) {
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                photos.add(new Element(
                        c.getInt(c.getColumnIndex(ROW_ID)),
                        c.getString(c.getColumnIndex(ROW_TITLE)),
                        c.getInt(c.getColumnIndex(ROW_COMPLETED))==1,
                        c.getString(c.getColumnIndex(ROW_COLOR)),
                        c.getInt(c.getColumnIndex(ROW_PRIORITY)),
                        c.getInt(c.getColumnIndex(ROW_DELETE))==1,
						c.getInt(c.getColumnIndex(ROW_ALARM)),
						c.getString(c.getColumnIndex(ROW_FOLDER))
                        ));
            }
        }
        c.close();
        return photos;
    }

    ///////////Alarm ////////////////////////////////////////////
    public void addAlarm(int id, Long alarmId){
        ContentValues cv = new ContentValues();
        cv.put(ROW_ALARM, alarmId);
        ourDatabase.update(TABLE_NAME, cv, ROW_ID+"="+id, null);
    }

    public void alarmDone(int id){
        ContentValues cv = new ContentValues();
        cv.putNull(ROW_ALARM);
        ourDatabase.update(TABLE_NAME, cv, ROW_ID+"="+id, null);
    }

    public ArrayList<Element> getAllAlarms(){
        ArrayList<Element> alarms = new ArrayList<Element>();
        alarms.clear();
        Cursor c = ourDatabase.query(TABLE_NAME, null, ROW_ALARM+" > 0", null, null, null, null);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            alarms.add(new Element(
                    c.getInt(c.getColumnIndex(ROW_ID)),
                    c.getString(c.getColumnIndex(ROW_TITLE)),
                    c.getInt(c.getColumnIndex(ROW_COMPLETED))==1,
                    c.getString(c.getColumnIndex(ROW_COLOR)),
                    c.getInt(c.getColumnIndex(ROW_PRIORITY)),
                    c.getInt(c.getColumnIndex(ROW_DELETE)) == 1,
                    c.getInt(c.getColumnIndex(ROW_ALARM)),
                    c.getString(c.getColumnIndex(ROW_FOLDER))
            ));
        }

        c.close();
        return alarms;
    }

    // Changes completion status of i //////////////////
    public void ToggleDeletion(int i) {

        String[] columns = { ROW_DELETE, ROW_TITLE };
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + i,	null, null, null, null);
        c.moveToFirst();
        ContentValues cv = new ContentValues();
        if (c.getInt(c.getColumnIndex(ROW_DELETE)) == 1) {
            cv.put(ROW_DELETE, 0);
        } else {
            cv.put(ROW_DELETE, 1);
        }
        c.close();
        ourDatabase.update(TABLE_NAME, cv, ROW_ID + "=" + i, null);

        AppWidgetManager awm = AppWidgetManager.getInstance(ourContext);
        int[] ids = awm.getAppWidgetIds(new ComponentName(ourContext, WidgetProvider.class));
        awm.notifyAppWidgetViewDataChanged(ids, com.likwid.anilist.R.id.wList);
    }

    public Boolean isMarked(int id) {
        String[] columns = {ROW_DELETE};
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_ID + "=" + id, null, null, null, null);
        if (c.isAfterLast()) {
            return null;
        }else{
            c.moveToFirst();
            int idMarked = c.getInt(c.getColumnIndex(ROW_DELETE));
            return (idMarked == 1);
        }
    }

    public ArrayList<Integer> getMarked(){
        ArrayList<Integer> returned = new ArrayList<>();
        String[] columns = {ROW_ID, ROW_DELETE};
        Cursor c = ourDatabase.query(TABLE_NAME, columns, ROW_DELETE + "=1", null, null, null, null);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            returned.add(c.getInt(c.getColumnIndex(ROW_ID)));
        }
        return returned;
    }
}
