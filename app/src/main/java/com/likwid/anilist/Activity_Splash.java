package com.likwid.anilist;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;

public class Activity_Splash extends Activity {

    private ProgressBar mSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        if (getActionBar() != null)
            getActionBar().hide();
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        TextView tvTitle = (TextView) findViewById(R.id.tvSplashTitle);
        ImageView ivSplashBG = (ImageView) findViewById(R.id.ivSplashBG);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Allura.otf");

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean BgIsImage = pref.getBoolean(getString(R.string.prefBgIsImage), false);
        Boolean ColorBG = pref.getBoolean(getString(R.string.prefColorBG), false);
        int color = pref.getInt(getString(R.string.prefColorInt), R.color.cBlue);
        int barColor = getResources().getColor(color);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            switch (color){
                case R.color.cBlue:
                    barColor = getResources().getColor(R.color.dBlue);
                    break;
                case R.color.cRed:
                    barColor = getResources().getColor(R.color.dRed);
                    break;
                case R.color.cPink:
                    barColor = getResources().getColor(R.color.dPink);
                    break;
                case R.color.cGreen:
                    barColor = getResources().getColor(R.color.dGreen);
                    break;
                case R.color.cPurple:
                    barColor = getResources().getColor(R.color.dPurple);
                    break;
                case R.color.cOrange:
                    barColor = getResources().getColor(R.color.dOrange);
                    break;
                case R.color.cLime:
                    barColor = getResources().getColor(R.color.dOrange);
                    break;
                case R.color.cYellow:
                    barColor = getResources().getColor(R.color.dYellow);
                    break;
                case R.color.cSky:
                    barColor = getResources().getColor(R.color.dSky);
                    break;
                case R.color.cSlate:
                    barColor = getResources().getColor(R.color.dSlate);
                    break;
            }
            getWindow().setStatusBarColor(barColor);
        }

        if (BgIsImage) {
            File file = new File(getFilesDir() + "/bg.jpg");

            if (file.exists()) {
                Drawable image = Drawable.createFromPath(file.getPath());
                ivSplashBG.setImageDrawable(image);
            }else{
                ivSplashBG.setImageResource(R.drawable.main_bg);
            }
        }else{
            ivSplashBG.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        if (ColorBG){
            ivSplashBG.setImageResource(color);
        }else{
            ivSplashBG.setBackgroundColor(Color.TRANSPARENT);
        }

        tvTitle.setTypeface(font);
        Thread thread = new Thread(runable);
        thread.start();
    }

    public Runnable runable = new Runnable() {
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            startActivity(new Intent(Activity_Splash.this, MainActivity.class));
            finish();


        }
    };
}
