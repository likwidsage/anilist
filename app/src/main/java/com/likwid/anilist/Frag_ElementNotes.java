package com.likwid.anilist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Frag_ElementNotes extends Fragment implements OnItemClickListener, OnItemLongClickListener{

    static final String NOTE_ID = "note_id";
	Adapter_SQL database;
	ListView lvNoteList;
	ArrayList<Element> noteList;
    Element parentInfo;
	int NoteID;
	TextView noteTitle, noNote;
	View view;
	Adapter_Notes adapter;
	LinearLayout loMain;
	MainActivity toSlide;

	static String ARG_SECTION_NUMBER = "section_arg";

    public Frag_ElementNotes(){}

	public static Fragment newInstance(int sectionNumber) {
		Fragment fragment = new Frag_ElementNotes();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		view = inflater.inflate(com.likwid.anilist.R.layout.frag_element_notes, container, false);
		lvNoteList = (ListView) view.findViewById(com.likwid.anilist.R.id.lvNoteList);
		noteTitle = (TextView) view.findViewById(com.likwid.anilist.R.id.tvNoteTitle);
		noNote = (TextView) view.findViewById(com.likwid.anilist.R.id.tvNoteNone);
		loMain = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.lo_NotesMain);
		
		lvNoteList.setOnItemClickListener(this);
		lvNoteList.setOnItemLongClickListener(this);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		database = new Adapter_SQL(getActivity());
		
		NoteID = getArguments().getInt(NOTE_ID);
		setList(NoteID);
	}
	
	public void setList(int id){
		database.open();
		noteList = database.getChildNotes(id);
		parentInfo = database.getSingle(id);		
		database.close();
		
		if (noteList.size() > 0 && NoteID > 0){
            noNote.setVisibility(View.GONE);
			noteTitle.setText(" Note(s) for " + parentInfo.title+":");
		}else if (noteList.size() <= 0 && NoteID > 0){
			noNote.setVisibility(View.VISIBLE);
		}
		adapter = new Adapter_Notes(getActivity(), noteList);
		lvNoteList.setAdapter(adapter);

        SharedPreferences prefs = getActivity().getSharedPreferences("com.likwid.orgalist_preferences", Context.MODE_PRIVATE);
        int colorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);
		
		Boolean ColorBG = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);
		
		noteTitle.setBackgroundColor(getResources().getColor(colorPrefInt));
		
		if (ColorBG){
			loMain.setBackgroundColor(getResources().getColor(colorPrefInt));
		}else{
			loMain.setBackgroundColor(Color.BLACK);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		Intent intent = new Intent ("com.likwid.orgalist.ACTIVITY_OPENNOTE");
		intent.putExtra("noteID", noteList.get(pos).id);
		startActivity(intent);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos,	long arg3) {
		final int id = noteList.get(pos).id;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		String name = (noteList.get(pos).title.length() > 20 ? noteList.get(pos).title.substring(0, 17)+"..." : noteList.get(pos).title);
		builder.setTitle("Delete '" + name + "' ?");
		//Delete
		builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				database.open();
				database.deleteItem(id);
				database.close();
				setList(NoteID);
                Frag_MainList main = (Frag_MainList) getActivity().getFragmentManager().findFragmentByTag("main");
                main.refresh();
			}
		});
		builder.setNegativeButton("Cancel", null);
		builder.create();
		builder.show();
		return true;
	}
	
	


}
