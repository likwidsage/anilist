package com.likwid.anilist;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter_List_All extends BaseAdapter implements OnClickListener{

    private Context ourContext;
    ArrayList<Element> values;
    ImageView ivTinyAlarm;
    int selected;
    Adapter_SQL database;
    TextView title, iconLetter;
    RelativeLayout rlSelect;
    LinearLayout llInfo;
    View row;
    TextView rowText, tvSub;
    MainActivity toSlide;
    Animation selectRow;
    ArrayList<Integer> selections;

    int ColorPrefInt;

    DisplayMetrics dm = new DisplayMetrics();

    static int slctColor;
    int minLevel;

    public Adapter_List_All(Context c, ArrayList<Element> list){
        dm = c.getResources().getDisplayMetrics();
        ourContext = c;
        values = list;
        database = new Adapter_SQL(ourContext);
        toSlide = (MainActivity) ourContext;
        selections = toSlide.getSelection();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ourContext);

        minLevel = 0;
        if (list.size() > 0) {
            database.open();
            minLevel = database.getLevel(list.get(0).id);
            for (Element element : list) {
                if (database.getLevel(element.id) < minLevel) {
                    minLevel = database.getLevel(element.id);
                }
            }
            database.close();
        }

        //read preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(ourContext);
        ColorPrefInt = prefs.getInt(ourContext.getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);

        selectRow = AnimationUtils.loadAnimation(ourContext, com.likwid.anilist.R.anim.list_select);
    }

    @Override
    public View getView(int pos, View convert, ViewGroup parent) {
        row = convert;
        if (row == null) {
            LayoutInflater inflate = (LayoutInflater) ourContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflate.inflate(com.likwid.anilist.R.layout.adapter_main_row, parent, false);
        }

        llInfo = (LinearLayout) row.findViewById(com.likwid.anilist.R.id.llInfo);
        RelativeLayout rlMainRow = (RelativeLayout) row.findViewById(com.likwid.anilist.R.id.rlMainRow);
        rlSelect = (RelativeLayout) row.findViewById(com.likwid.anilist.R.id.rlMainSelect);
        rowText = (TextView) row.findViewById(com.likwid.anilist.R.id.tvItemText);
        iconLetter = (TextView) row.findViewById(com.likwid.anilist.R.id.tvRow_IconLetter);
        ivTinyAlarm = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyAlarm);
        ImageView ivTinyNote = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyNote);
        ImageView ivTinyPic = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyPic);
        ImageView ivTinyUrl = (ImageView) row.findViewById(com.likwid.anilist.R.id.ivMainListTinyURL);

        tvSub = (TextView) row.findViewById(com.likwid.anilist.R.id.tvMainRowSub);

        database.open();

        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) rlMainRow.getLayoutParams();
        int m = database.getLevel(values.get(pos).id) - minLevel;
        m = m * 25;

        param.setMargins(m, param.topMargin, param.rightMargin, param.bottomMargin);
        rlMainRow.setLayoutParams(param);

        ivTinyAlarm.setVisibility(View.GONE);
        ivTinyPic.setVisibility(View.GONE);
        ivTinyNote.setVisibility(View.GONE);

        database.close();

        if (values.get(pos).title.contains("http://")) {
            ivTinyUrl.setVisibility(View.VISIBLE);
            String title = values.get(pos).title;
            int startPos = title.indexOf("http://");
            int endPos = title.substring(startPos).indexOf(" ");

            if (endPos == -1) {
                ivTinyUrl.setTag(title.substring(startPos));
            } else {
                ivTinyUrl.setTag(title.substring(startPos, endPos));
            }

        } else {
            ivTinyUrl.setVisibility(View.GONE);
        }

        String iconText = values.get(pos).title.substring(0, 1).toUpperCase();
        iconLetter.setText(iconText);

        rowText.setTextColor(Color.parseColor("#ffffff"));
        rowText.setText(values.get(pos).title);
        //If Completed
        if (values.get(pos).completed) {
            rowText.setAlpha(.25f);
            tvSub.setAlpha(.25f);
            rowText.setPaintFlags(rowText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            rowText.setPaintFlags(rowText.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            iconLetter.setVisibility(View.VISIBLE);
            rowText.setAlpha(1f);
            tvSub.setAlpha(1f);
        }

        //set selected color and icon color
        if (values.get(pos).color.equals("blue")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cBlue);
        } else if (values.get(pos).color.equals("red")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cRed);
        } else if (values.get(pos).color.equals("purple")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cPurple);
        } else if (values.get(pos).color.equals("pink")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cPink);
        } else if (values.get(pos).color.equals("orange")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cOrange);
        } else if (values.get(pos).color.equals("green")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cGreen);
        } else if (values.get(pos).color.equals("lime")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cLime);
        } else if (values.get(pos).color.equals("yellow")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cYellow);
        } else if (values.get(pos).color.equals("sky")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cSky);
        } else if (values.get(pos).color.equals("slate")) {
            slctColor = ourContext.getResources().getColor(com.likwid.anilist.R.color.cSlate);
        }

        ShapeDrawable iconBG =  new ShapeDrawable(new OvalShape());
        iconBG.setIntrinsicWidth(200);
        iconBG.setIntrinsicHeight(200);
        iconBG.getPaint().setColor(slctColor);

        iconLetter.setBackground(iconBG);
        tvSub.setVisibility(View.GONE);

        rlMainRow.setTag(values.get(pos).id);
        rlMainRow.setOnClickListener(this);

        return row;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int pos) {
        return new Element(values.get(pos).id,
                values.get(pos).title,
                values.get(pos).completed,
                values.get(pos).color,
                values.get(pos).priority,
                values.get(pos).marked,
                values.get(pos).alarm,
                values.get(pos).folder
                );
    }

    @Override
    public long getItemId(int pos) {
        return values.get(pos).id;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case com.likwid.anilist.R.id.rlMainRow:
                MainActivity toSlide = (MainActivity) ourContext;
                int id = (Integer) v.getTag();
                toSlide.goInto(id);
                break;
        }
    }

}

