package com.likwid.anilist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class Frag_Import extends Fragment implements OnClickListener {

    static final int openFile = 1;
    Button bOpen, bImport;
    EditText etText;
    TextView tvTitle, tvProgress;
    Adapter_SQL database;
    MainActivity toSlide;
    LinearLayout llMain, llProgress;
    int selected;
    private boolean imported = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(com.likwid.anilist.R.layout.frag_import, container, false);

        tvTitle = (TextView) view.findViewById(com.likwid.anilist.R.id.tvImportTitle);
        etText = (EditText) view.findViewById(com.likwid.anilist.R.id.etImportList);
        bOpen = (Button) view.findViewById(com.likwid.anilist.R.id.bImportFile);
        bImport = (Button) view.findViewById(com.likwid.anilist.R.id.bImportDo);
        llMain = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.llImportMain);
        llProgress = (LinearLayout) view.findViewById(com.likwid.anilist.R.id.llProgress);
        tvProgress = (TextView) view.findViewById(com.likwid.anilist.R.id.tvProgress);

        bOpen.setOnClickListener(this);
        bImport.setOnClickListener(this);

        toSlide = (MainActivity) getActivity();

        bImport.setAlpha(.5f);
        bImport.setClickable(false);
        imported=false;

        etText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ImportText it = new ImportText(getActivity(), selected);
                if (it.TextIsGood(etText.getText().toString())){
                    bImport.setAlpha(1f);
                    bImport.setClickable(true);
                }
            }
        });

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        database = new Adapter_SQL(getActivity());
        toSlide = (MainActivity) getActivity();

        String nameOfParent = "main list";

        if (toSlide.getSelection().size() == 1) {
            database.open();
            nameOfParent = database.getName(toSlide.getSelection().get(0));
            selected = toSlide.getSelection().get(0);
            database.close();
        } else if (toSlide.getViewId() > 0) {
            database.open();
            nameOfParent = database.getName(toSlide.getViewId());
            database.close();
        }

        if (Build.VERSION.SDK_INT >= 19) {
            tvTitle.setText("Importing to '" + nameOfParent + "'");
        } else {
            tvTitle.setText("Importing to '" + nameOfParent + "'\nPlace file(s) in: " + MainActivity.EXPORT_FOLDER.toString());
        }

        SharedPreferences prefs = getActivity().getSharedPreferences("com.likwid.orgalist_preferences", Context.MODE_PRIVATE);
        int colorPrefInt = prefs.getInt(getString(com.likwid.anilist.R.string.prefColorInt), com.likwid.anilist.R.color.cBlue);

        Boolean ColorBG = prefs.getBoolean(getString(com.likwid.anilist.R.string.prefColorBG), false);

        tvTitle.setBackgroundColor(getResources().getColor(colorPrefInt));

        if (ColorBG) {
            llMain.setBackgroundColor(getResources().getColor(colorPrefInt));
        }

        bImport.setBackgroundColor(getResources().getColor(colorPrefInt));
        bOpen.setBackgroundColor(getResources().getColor(colorPrefInt));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (com.likwid.anilist.R.id.bImportFile):

                if (Build.VERSION.SDK_INT >= 19) {
                    tvTitle.setVisibility(View.GONE);

                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    startActivityForResult(intent, openFile);
                } else {

                    File[] files = MainActivity.EXPORT_FOLDER.listFiles();
                    ArrayList<String> textFiles = new ArrayList<>();
                    if (files != null) {
                        for (File file : files) {
                            textFiles.add(file.toString());
                        }
                        LayoutInflater li = LayoutInflater.from(getActivity());
                        View dialogView = li.inflate(com.likwid.anilist.R.layout.dial_file_list, null);

                        final Spinner sFile = (Spinner) dialogView
                                .findViewById(com.likwid.anilist.R.id.sFileList);
                        final Context mContext = getActivity();

                        Adapter_FileList adapter = new Adapter_FileList(mContext, textFiles);
                        sFile.setAdapter(adapter);

                        AlertDialog.Builder db = new AlertDialog.Builder(
                                getActivity());
                        db.setView(dialogView);

                        final ArrayList<String> finalList = textFiles;
                        db.setPositiveButton("Select",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        int pos = sFile.getSelectedItemPosition();
                                        if (pos > -1) {
                                            String pickedFile = finalList.get(pos);
                                            File fileToRead = new File(pickedFile);
                                            ImportText it = new ImportText(mContext, selected);
                                            it.ReadAni(fileToRead);
                                        }
                                    }
                                }
                        );
                        db.setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                    }
                                }
                        );
                        db.setTitle("Choose a file to import.");
                        AlertDialog prompt = db.create();
                        prompt.show();

                    } else {
                        Toast.makeText(getActivity(), "No files found",
                                Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            case (com.likwid.anilist.R.id.bImportDo):
                if (etText.getText().length() > 0 && !imported) {
                    ImportText it = new ImportText(getActivity(), selected);
                    Log.d ("ImportText Frag_Import", etText.getText().toString());
                    it.importText(etText.getText().toString());
                    imported = true;
                }
                toSlide.refreshList();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == openFile && resultCode == Activity.RESULT_OK) {
            Uri uri = intent.getData();
            Cursor cursor = getActivity().getContentResolver().query(uri, null,
                    null, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                ImportText it = new ImportText(getActivity(), selected);
                String list = it.ReadAni(uri);
                if (list != null){
                    etText.setText(list);
                }

            }
            if (cursor != null) {
                cursor.close();
            }
        }
    }



}

