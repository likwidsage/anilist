package com.likwid.anilist;

import android.os.Bundle;
import android.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by likwid on 3/22/15.
 */
public class Dial_Add extends DialogFragment implements View.OnClickListener, TextView.OnEditorActionListener {
    private String itemColor = "blue";

    LinearLayout llBlue, llRed, llPink, llGreen, llPurple, llOrange, llLime, llYellow, llSky, llSlate;
    ImageView checkBlue, checkRed, checkPink, checkGreen, checkPurple, checkOrange, checkLime,
            checkYellow, checkSky, checkSlate;
    EditText etName;

    static Dial_Add newInstance(){
        return new Dial_Add();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(com.likwid.anilist.R.layout.dial_add, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        llBlue = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llBlue);
        llRed = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llRed);
        llPink = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llPink);
        llGreen = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llGreen);
        llPurple = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llPurple);
        llOrange = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llOrange);
        llLime = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llLime);
        llYellow = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llYellow);
        llSky = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llSky);
        llSlate = (LinearLayout) inflate.findViewById(com.likwid.anilist.R.id.llSlate);

        checkBlue = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckBlue);
        checkRed = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckRed);
        checkPink = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckPink);
        checkGreen = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckGreen);
        checkPurple = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckPurple);
        checkOrange = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckOrange);
        checkLime = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckLime);
        checkYellow = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckYellow);
        checkSky = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckSky);
        checkSlate = (ImageView) inflate.findViewById(com.likwid.anilist.R.id.ivCheckSlate);

        llBlue.setOnClickListener(this);
        llRed.setOnClickListener(this);
        llPink.setOnClickListener(this);
        llGreen.setOnClickListener(this);
        llPurple.setOnClickListener(this);
        llOrange.setOnClickListener(this);
        llLime.setOnClickListener(this);
        llYellow.setOnClickListener(this);
        llSky.setOnClickListener(this);
        llSlate.setOnClickListener(this);

        etName = (EditText) inflate.findViewById(com.likwid.anilist.R.id.etName);
        etName.setOnEditorActionListener(this);
        etName.setImeOptions(EditorInfo.IME_ACTION_GO);

        return inflate;
    }

    @Override
    public void onClick(View v) {
        checkBlue.setVisibility(View.INVISIBLE);
        checkRed.setVisibility(View.INVISIBLE);
        checkPink.setVisibility(View.INVISIBLE);
        checkGreen.setVisibility(View.INVISIBLE);
        checkPurple.setVisibility(View.INVISIBLE);
        checkOrange.setVisibility(View.INVISIBLE);
        checkLime.setVisibility(View.INVISIBLE);
        checkYellow.setVisibility(View.INVISIBLE);
        checkSky.setVisibility(View.INVISIBLE);
        checkSlate.setVisibility(View.INVISIBLE);
        switch (v.getId()){
            case com.likwid.anilist.R.id.llBlue:
                itemColor = "blue";
                checkBlue.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llRed:
                itemColor = "red";
                checkRed.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llPink:
                itemColor = "pink";
                checkPink.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llGreen:
                itemColor = "green";
                checkGreen.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llPurple:
                itemColor = "purple";
                checkPurple.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llOrange:
                itemColor = "orange";
                checkOrange.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llLime:
                itemColor = "lime";
                checkLime.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llYellow:
                itemColor = "yellow";
                checkYellow.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llSky:
                itemColor = "sky";
                checkSky.setVisibility(View.VISIBLE);
                break;
            case com.likwid.anilist.R.id.llSlate:
                itemColor = "slate";
                checkSlate.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        String name = etName.getText().toString().trim();
        MainActivity toSlide = (MainActivity) getActivity();
        int viewId = toSlide.getViewId();

        Adapter_SQL database = new Adapter_SQL(getActivity());
        database.open();
        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN){
            if (database.getLevel(toSlide.getViewId()) < toSlide.getFreeSubLimit() || toSlide.hasSubs()) {
                if (database.createElement(name, itemColor, 0, database.getPath(viewId))) {
                    etName.setText("");
                    Toast.makeText(getActivity(), name + " added to " + database.getName(viewId), Toast.LENGTH_SHORT).show();
                    toSlide.refreshList();
                }
            }else{
                toSlide.showItemToBuy(MainActivity.UNLSUBS);
            }
        }
        database.close();
        return true;
    }
}
